// headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "SphereMesh.h"


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile = NULL;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry-point function
int main(int argc, const char * argv[])
{
    // code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // window
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can not create log file.\nExitting now. ");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program is started successfully :)\n");

	NSRect win_rect;
   	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    
    	// create simple window
   	window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | 
						 NSWindowStyleMaskClosable | 
						 NSWindowStyleMaskMiniaturizable | 						 						 NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
    [window setTitle:@"macOS Ortho Triangle Window"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    	// code
	fprintf(gpFile, "Program is Terminated Successfully :)");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

- (void)dealloc
{
    // code
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
	@private
	CVDisplayLinkRef displayLink;

	GLuint gVertexShaderObjectPerVertex;
	GLuint gFragmentShaderObjectPerVertex;
	GLuint gShaderProgramObjectPerVertex;

	GLuint gVertexShaderObjectPerFragment;
	GLuint gFragmentShaderObjectPerFragment;
	GLuint gShaderProgramObjectPerFragment;

	GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

	GLuint L_KeyPressed_uniform;
	GLuint V_KeyPressed_uniform;
	GLuint F_KeyPressed_uniform;

	GLuint La0_uniform;
	GLuint Ld0_uniform;
	GLuint Ls0_uniform;
	GLuint light0_position_uniform;

	GLuint La1_uniform;
	GLuint Ld1_uniform;
	GLuint Ls1_uniform;
	GLuint light1_position_uniform;

	GLuint La2_uniform;
	GLuint Ld2_uniform;
	GLuint Ls2_uniform;
	GLuint light2_position_uniform;

	GLuint Ka_uniform;
	GLuint Kd_uniform;
	GLuint Ks_uniform;
	GLuint material_shininess_uniform;

	GLfloat angle;
	vmath::mat4 perspectiveProjectionMatrix;
	
	bool light;
	bool bIsFKeyPressed;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
    angle = 0.0f;

    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
	NSOpenGLPixelFormatAttribute attrs[] = 
	{
		NSOpenGLPFAOpenGLProfile, 
		NSOpenGLProfileVersion4_1Core,
		NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
		NSOpenGLPFANoRecovery, 
		NSOpenGLPFAAccelerated,
		NSOpenGLPFAColorSize, 24,
		NSOpenGLPFADepthSize, 24,
		NSOpenGLPFAAlphaSize, 8,
		NSOpenGLPFADoubleBuffer,
		0
	};
	
	NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
	
	if(pixelFormat == nil)
	{
		fprintf(gpFile, "No Valid OGL Pixel Format is available. Exitting.. :(");
		[self release];
		[NSApp terminate:self];
	}

	NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
	[self setPixelFormat:pixelFormat];
	[self setOpenGLContext:glContext];
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	return(kCVReturnSuccess); 
}

-(void) prepareOpenGL
{
	light = false;

	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version	: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	[[self openGLContext]makeCurrentContext];
	
	GLint swapInt = 1;
	[[self openGLContext]setValues: &swapInt forParameter:NSOpenGLCPSwapInterval];
	
   gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
    
    // provide source code to shader
    const GLchar *vertexShaderSourceCodePerVertex =
 	 	"#version 410 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_lighting_enabled;"			\
		"uniform vec3 u_La[3];"						\
		"uniform vec3 u_Ld[3];"						\
		"uniform vec3 u_Ls[3];"						\
		"uniform vec4 u_light_position[3];"			\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"out vec3 phong_ads_color;"					\
		"void main(void)"							\
		"{"											\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"					\
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"	\
		"int i = 0;"																			\
		"vec3 light_direction;"																	\
		"float tn_dot_ld;"																		\
		"vec3 ambient;"																			\
		"vec3 diffuse;"																			\
		"vec3 reflection_vector;"																\
		"vec3 viewer_vector;"																	\
		"vec3 specular;"																		\
		"vec3 phong_ads[3];"																	\
		"for(i = 0; i < 3; i++)"																\
		"{"																						\
		"light_direction = normalize(vec3(u_light_position[i]) - eye_coordinates.xyz);"			\
		"tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);"						\
		"ambient = u_La[i] * u_Ka;"																\
		"diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"													\
		"reflection_vector = reflect(-light_direction, transformed_normals);"					\
		"viewer_vector = normalize(-eye_coordinates.xyz);"										\
		"specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"\
		"phong_ads[i] = ambient + diffuse + specular;"											\
		"}"																						\
		"phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];"							\
		"}"																						\
		"else"																					\
		"{"																						\
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"												\
		"}"																						\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"		\
		"}";

  glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar **)&vertexShaderSourceCodePerVertex, NULL);
    
    // compile shader
    glCompileShader(gVertexShaderObjectPerVertex);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** FRAGMENT SHADER ***
    // re-initialize
    iInfoLogLength = 0;
    iShaderCompiledStatus = 0;
    szInfoLog = NULL;
    
    // create shader
    gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
    
    // provide source code to shader
    const GLchar *fragmentShaderSourceCodePerVertex =
 		"#version 410 core"												\
		"\n"															\
		"in vec3 phong_ads_color;"					\
		"out vec4 FragColor;"						\
		"void main(void)"							\
		"{"											\
		"FragColor = vec4(phong_ads_color, 1.0);"	\
		"}";
   glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);
    
    // compile shader
    glCompileShader(gFragmentShaderObjectPerVertex);
    glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** SHADER PROGRAM ***
    // create
    gShaderProgramObjectPerVertex = glCreateProgram();
    
    glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
    glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
    
    // pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObjectPerVertex, VDG_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObjectPerVertex, VDG_ATTRIBUTE_NORMAL, "vNormal");
   
    // link shader
    glLinkProgram(gShaderProgramObjectPerVertex);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength>0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_lighting_enabled");
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_vertex_enabled");
	F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_fragment_enabled");

	La0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[0]");
	Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[0]");
	Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[0]");

	light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[0]");

	La1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[1]");
	Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[1]");
	Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[1]");

	light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[1]");

	La2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[2]");
	Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[2]");
	Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[2]");

	light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[2]");

	Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_material_shininess");


   gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
    
    // provide source code to shader
    const GLchar *vertexShaderSourceCodePerFragment =
 	 	"#version 410 core"							\
		"\n"										\
			"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_lighting_enabled;"			\
		"uniform vec4 u_light_position[3];"			\
		"out vec3 transformed_normals;"				\
		"out vec3 light_direction[3];"				\
		"out vec3 viewer_vector;"					\
		"void main(void)"							\
		"{"											\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"					\
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"					\
		"int i;"																				\
		"for(i = 0;i<3;i++)"																	\
		"{"																						\
		"light_direction[i] = u_light_position[i].xyz - eye_coordinates.xyz;"					\
		"}"																						\
		"viewer_vector = -eye_coordinates.xyz;"													\
		"}"																						\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"		\
		"}";

  glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertexShaderSourceCodePerFragment, NULL);
    
    // compile shader
    glCompileShader(gVertexShaderObjectPerFragment);
    iInfoLogLength = 0;
    iShaderCompiledStatus = 0;
    szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** FRAGMENT SHADER ***
    // re-initialize
    iInfoLogLength = 0;
    iShaderCompiledStatus = 0;
    szInfoLog = NULL;
    
    // create shader
    gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);
    
    // provide source code to shader
    const GLchar *fragmentShaderSourceCodePerFragment =
 		"#version 410 core"												\
		"\n"															\
		"in vec3 phong_ads_color;"					\
		"in vec3 transformed_normals;"				\
		"in vec3 light_direction[3];"				\
		"in vec3 viewer_vector;"					\
		"out vec4 FragColor;"						\
		"uniform int u_lighting_enabled;"			\
		"uniform vec3 u_La[3];"						\
		"uniform vec3 u_Ld[3];"						\
		"uniform vec3 u_Ls[3];"						\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"void main(void)"												\
		"{"																\
		"if(u_lighting_enabled == 1)"									\
		"{"																\
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"			\
		"vec3 normalized_light_direction[3];"											\
		"int i;"																		\
		"vec3 ambient;"													\
		"float tn_dot_ld;"		\
		"vec3 diffuse;"									\
		"vec3 reflection_vector;"														\
		"vec3 specular;"																\
		"vec3 phong_ads[3];"															\
		"for(i=0;i<3;i++)"																\
		"{"																				\
		"normalized_light_direction[i] = normalize(light_direction[i]);"				\
		"}"																				\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"						\
		"for(i=0;i<3;i++)"																\
		"{"																				\
		"ambient = u_La[i] * u_Ka;"														\
		"tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction[i]), 0.0);"		\
		"vec3 diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"									\
		"vec3 reflection_vector = reflect(-normalized_light_direction[i], normalized_transformed_normals);"	\
		"vec3 specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
		"phong_ads[i] = ambient + diffuse + specular;"									\
		"}"																				\
		"vec3 phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];"			\
		"FragColor = vec4(phong_ads_color, 1.0);"										\
		"}"																				\
		"else"																			\
		"{"																				\
		"FragColor = vec4(1.0,1.0,1.0,1.0);"											\
		"}"																				\
		"}";

   glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);
    
    // compile shader
    glCompileShader(gFragmentShaderObjectPerFragment);
    glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** SHADER PROGRAM ***
    // create
    gShaderProgramObjectPerFragment = glCreateProgram();
    
    glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
    glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
    
    // pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(gShaderProgramObjectPerFragment, VDG_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObjectPerFragment, VDG_ATTRIBUTE_NORMAL, "vNormal");
   
    // link shader
    glLinkProgram(gShaderProgramObjectPerFragment);
    iShaderProgramLinkStatus = 0;
    glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength>0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lighting_enabled");
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_vertex_enabled");
	F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_fragment_enabled");

	La0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[0]");
	Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[0]");
	Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[0]");

	light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[0]");

	La1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[1]");
	Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[1]");
	Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[1]");

	light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[1]");

	La2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[2]");
	Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[2]");
	Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[2]");

	light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[2]");

	Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_material_shininess");


	makeSphere(2.0 ,60, 60);
 
	
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
 // glEnable(GL_CULL_FACE);
 // glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
 //	glShadeModel(GL_SMOOTH);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}

-(void) reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if(height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	perspectiveProjectionMatrix = vmath::perspective(45.0f,((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
    	// code
	[self drawView];
}

-(void) drawView
{

	GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat light0Diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
	GLfloat light0Specular[] = { 1.0f, 0.0f, 0.0f ,0.0f };
	GLfloat light0Position[] = { 000.0f, 100.0f, 100.0f, 1.0f };

	GLfloat light1Diffuse[] = { 0.0f, 1.0f, 0.0f, 0.0f };
	GLfloat light1Specular[] = { 0.0f, 1.0f, 0.0f ,0.0f };
	GLfloat light1Position[] = { 100.0f, 000.0f, 100.0f, 1.0f };

	GLfloat light2Diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
	GLfloat light2Specular[] = { 0.0f, 0.0f, 1.0f ,0.0f };
	GLfloat light2Position[] = { 100.0f, 100.0f, 000.0f, 1.0f };

	GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat material_shininess = 50.0f;

	angle += 0.01f;
	if (angle >= 2*3.145)
	{
		angle = 0.0f;
	}
	light0Position[1] = 100.0f * cos(angle);
	light0Position[2] = 100.0f * sin(angle);

	light1Position[0] = 100.0f * cos(angle);
	light1Position[2] = 100.0f * sin(angle);

	light2Position[0] = 100.0f * cos(angle);
	light2Position[1] = 100.0f * sin(angle);


	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	if (light == true)
	{
		if (bIsFKeyPressed)
		{
			glUseProgram(gShaderProgramObjectPerFragment);
		}
		else
		{
			glUseProgram(gShaderProgramObjectPerVertex);
		}

		glUniform1i(L_KeyPressed_uniform, 1);
		
		glUniform3fv(La0_uniform, 1, lightAmbient);
		glUniform3fv(Ld0_uniform, 1, light0Diffuse);
		glUniform3fv(Ls0_uniform, 1, light0Specular);
		glUniform4fv(light0_position_uniform, 1, light0Position);

		glUniform3fv(La1_uniform, 1, lightAmbient);
		glUniform3fv(Ld1_uniform, 1, light1Diffuse);
		glUniform3fv(Ls1_uniform, 1, light1Specular);
		glUniform4fv(light1_position_uniform, 1, light1Position);

		glUniform3fv(La2_uniform, 1, lightAmbient);
		glUniform3fv(Ld2_uniform, 1, light2Diffuse);
		glUniform3fv(Ls2_uniform, 1, light2Specular);
		glUniform4fv(light2_position_uniform, 1, light2Position);

		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	vmath::mat4 modelMatrix = vmath::mat4::identity();
	vmath::mat4 viewMatrix = vmath::mat4::identity();
    
    modelMatrix = vmath::translate(0.0f, 0.0f, -8.0f);

    
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1,GL_FALSE, perspectiveProjectionMatrix);

   	draw();

    glUseProgram(0);	

	
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 'q':
        case 'Q':
            [ self release];
            [NSApp terminate:self];
            break;
        case 'f':
        case 'F':
        	model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_model_matrix");
			view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_view_matrix");
			projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projection_matrix");

			L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lighting_enabled");
			V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_vertex_enabled");
			F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_fragment_enabled");

			La0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[0]");
			Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[0]");
			Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[0]");

			light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[0]");

			La1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[1]");
			Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[1]");
			Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[1]");

			light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[1]");

			La2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[2]");
			Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[2]");
			Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[2]");

			light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[2]");

			Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ka");
			Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Kd");
			Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ks");
			material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_material_shininess");

        	bIsFKeyPressed = true;
        	break;
        case 'v':
        case 'V':
			model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_model_matrix");
			view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_view_matrix");
			projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_projection_matrix");

			L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_lighting_enabled");
			V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_vertex_enabled");
			F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_fragment_enabled");

			La0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[0]");
			Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[0]");
			Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[0]");

			light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[0]");

			La1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[1]");
			Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[1]");
			Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[1]");

			light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[1]");

			La2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[2]");
			Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[2]");
			Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[2]");

			light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[2]");

			Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ka");
			Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Kd");
			Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ks");
			material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_material_shininess");

        
        	bIsFKeyPressed = false;
        	break; 
        case 27:
            [[self window]toggleFullScreen:self]; // repainting occurs automatically
            break;
        case 'l':
        case 'L':
        	light = !light;
        	break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) dealloc
{
	// code    

    glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	glDeleteShader(gVertexShaderObjectPerVertex);
	gVertexShaderObjectPerVertex = 0;

	glDeleteShader(gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;

	glDeleteProgram(gShaderProgramObjectPerVertex);
	gShaderProgramObjectPerVertex = 0;

	glDeleteShader(gVertexShaderObjectPerFragment);
	gVertexShaderObjectPerFragment = 0;

	glDeleteShader(gFragmentShaderObjectPerFragment);
	gFragmentShaderObjectPerFragment = 0;

	glDeleteProgram(gShaderProgramObjectPerFragment);
	gShaderProgramObjectPerFragment = 0;


	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}
