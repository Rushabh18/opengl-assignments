// headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "SphereMesh.h"


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *, void *);

FILE *gpFile = NULL;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry-point function
int main(int argc, const char * argv[])
{
    // code
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];

    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    
    [pPool release];
    
    return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // code
    // window
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can not create log file.\nExitting now. ");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program is started successfully :)\n");

	NSRect win_rect;
   	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    
    	// create simple window
   	window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled | 
						 NSWindowStyleMaskClosable | 
						 NSWindowStyleMaskMiniaturizable | 						 						 NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
    [window setTitle:@"macOS Ortho Triangle Window"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    	// code
	fprintf(gpFile, "Program is Terminated Successfully :)");
	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
    // code
    [NSApp terminate:self];
}

- (void)dealloc
{
    // code
    [glView release];
    
    [window release];
    
    [super dealloc];
}
@end

@implementation GLView
{
	@private
	CVDisplayLinkRef displayLink;

	GLuint vertexShaderObject;
	GLuint fragmentShaderObject;
	GLuint shaderProgramObject;

	GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

	GLuint L_KeyPressed_uniform;

	GLuint La_uniform;
	GLuint Ld_uniform;
	GLuint Ls_uniform;
	GLuint light_position_uniform;

	GLuint Ka_uniform;
	GLuint Kd_uniform;
	GLuint Ks_uniform;
	GLuint material_shininess_uniform;

	vmath::mat4 perspectiveProjectionMatrix;
	
	bool light;
	bool gbXPressed;
	bool gbYPressed;
	bool gbZPressed;

	float gAngle;

	GLfloat width;
	GLfloat height;
}

-(id)initWithFrame:(NSRect)frame;
{
    // code
	gbXPressed = true;
	gbYPressed = false;
	gbZPressed = false;

	gAngle = 0.0f;

    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
	NSOpenGLPixelFormatAttribute attrs[] = 
	{
		NSOpenGLPFAOpenGLProfile, 
		NSOpenGLProfileVersion4_1Core,
		NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
		NSOpenGLPFANoRecovery, 
		NSOpenGLPFAAccelerated,
		NSOpenGLPFAColorSize, 24,
		NSOpenGLPFADepthSize, 24,
		NSOpenGLPFAAlphaSize, 8,
		NSOpenGLPFADoubleBuffer,
		0
	};
	
	NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attrs] autorelease];
	
	if(pixelFormat == nil)
	{
		fprintf(gpFile, "No Valid OGL Pixel Format is available. Exitting.. :(");
		[self release];
		[NSApp terminate:self];
	}

	NSOpenGLContext *glContext = [[[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:nil] autorelease];
	[self setPixelFormat:pixelFormat];
	[self setOpenGLContext:glContext];
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	return(kCVReturnSuccess); 
}

-(void) prepareOpenGL
{
	light = false;

	fprintf(gpFile, "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version	: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	[[self openGLContext]makeCurrentContext];
	
	GLint swapInt = 1;
	[[self openGLContext]setValues: &swapInt forParameter:NSOpenGLCPSwapInterval];
	
   vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    
    // provide source code to shader
    const GLchar *vertexShaderSourceCode =
 	 	"#version 410 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform mediump int u_lighting_enabled;"	\
		"uniform vec4 u_light_position;"			\
		"out vec3 transformed_normals;"				\
		"out vec3 light_direction;"					\
		"out vec3 viewer_vector;"					\
		"void main(void)"							\
		"{"											\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"				\
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"				\
		"light_direction = u_light_position.xyz - eye_coordinates.xyz;"					\
		"viewer_vector = -eye_coordinates.xyz;"												\
		"}"																					\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"	\
		"}";
  glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(vertexShaderObject);
    GLint iInfoLogLength = 0;
    GLint iShaderCompiledStatus = 0;
    char *szInfoLog = NULL;
    glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** FRAGMENT SHADER ***
    // re-initialize
    iInfoLogLength = 0;
    iShaderCompiledStatus = 0;
    szInfoLog = NULL;
    
    // create shader
    fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    
    // provide source code to shader
    const GLchar *fragmentShaderSourceCode =
 		"#version 410 core"												\
		"\n"															\
		"in vec3 transformed_normals;"				\
		"in vec3 light_direction;"					\
		"in vec3 viewer_vector;"					\
		"out vec4 FragColor;"						\
		"uniform vec3 u_La;"						\
		"uniform vec3 u_Ld;"						\
		"uniform vec3 u_Ls;"						\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"uniform int u_lighting_enabled;"			\
		"void main(void)"							\
		"{"											\
		"vec3 phong_ads_color;"						\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"					\
		"vec3 normalize_light_direction = normalize(light_direction);"							\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"								\
		"vec3 ambient = u_La * u_Ka;"															\
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalize_light_direction), 0.0);"				\
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"												\
		"vec3 reflection_vector = reflect(-normalize_light_direction, normalized_transformed_normals);"				\
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
		"phong_ads_color = ambient + diffuse + specular;"										\
		"}"																						\
		"else"																					\
		"{"																						\
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"												\
		"}"																						\
		"FragColor = vec4(phong_ads_color, 1.0);"												\
		"}";
   glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
    
    // compile shader
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
    if (iShaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    // *** SHADER PROGRAM ***
    // create
    shaderProgramObject = glCreateProgram();
    
    // attach vertex shader to shader program
    glAttachShader(shaderProgramObject, vertexShaderObject);
    
    // attach fragment shader to shader program
    glAttachShader(shaderProgramObject, fragmentShaderObject);
    
    // pre-link binding of shader program object with vertex shader position attribute
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
   
    // link shader
    glLinkProgram(shaderProgramObject);
    GLint iShaderProgramLinkStatus = 0;
    glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
    if (iShaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength>0)
        {
            szInfoLog = (char *)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
	model_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");

	La_uniform = glGetUniformLocation(shaderProgramObject, "u_La");
	Ld_uniform = glGetUniformLocation(shaderProgramObject, "u_Ld");
	Ls_uniform = glGetUniformLocation(shaderProgramObject, "u_Ls");

	light_position_uniform = glGetUniformLocation(shaderProgramObject, "u_light_position");

	Ka_uniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
	Kd_uniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
	Ks_uniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");

	makeSphere(2.0 ,60, 60);
 
	
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
 // glEnable(GL_CULL_FACE);
 // glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
 //	glShadeModel(GL_SMOOTH);

	glClearColor(0.05f, 0.05f, 0.05f, 0.05f);

	perspectiveProjectionMatrix = vmath::mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}

-(void) reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
	NSRect rect = [self bounds];

	width = rect.size.width;
	height = rect.size.height;

	if(height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	perspectiveProjectionMatrix = vmath::perspective(45.0f,((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void) viewPort:(GLint) viewPortWidth height:(GLint) viewPortHeight viewPortX:(GLint) x viewPortY:(GLint) y
{
	glViewport(x, y, (GLsizei)viewPortWidth, (GLsizei)viewPortHeight);
	
	perspectiveProjectionMatrix = vmath::perspective(45.0f,((GLfloat)viewPortWidth / (GLfloat)viewPortHeight), 0.1f, 100.0f);
}

- (void)drawRect:(NSRect)dirtyRect
{
    	// code
	[self drawView];
}

-(void) drawView
{

	GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f ,1.0f };
	GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f };

	GLfloat materialAmbient[24][4] = {
		{ 0.0215f, 0.1745f, 0.0215f, 1.0f },
		{ 0.135f , 0.225f , 0.1575f, 1.0f },
		{ 0.05375f , 0.05f , 0.06625f, 1.0f },
		{ 0.25f , 0.20725f , 0.20725f, 1.0f },
		{ 0.1745f , 0.01175f , 0.01175f, 1.0f },
		{ 0.1f , 0.18725f , 0.1745f, 1.0f },

		{ 0.329412f , 0.223529f , 0.027451f, 1.0f },
		{ 0.2125f , 0.1275f , 0.054f, 1.0f },
		{ 0.25f , 0.25f , 0.25f, 1.0f },
		{ 0.19125f , 0.0735f , 0.0225f, 1.0f },
		{ 0.24725f , 0.1995f , 0.0745f, 1.0f },
		{ 0.19225f , 0.19225f , 0.19225f, 1.0f },

		{ 0.0f , 0.0f , 0.0f, 1.0f },
		{ 0.0f , 0.1f , 0.06f, 1.0f },
		{ 0.0f , 0.0f , 0.0f, 1.0f },
		{ 0.0f , 0.0f , 0.0f, 1.0f },
		{ 0.0f , 0.0f , 0.0f, 1.0f },
		{ 0.0f , 0.0f , 0.0f, 1.0f },

		{ 0.02f , 0.02f , 0.02f, 1.0f },
		{ 0.0f , 0.05f , 0.05f, 1.0f },
		{ 0.0f , 0.05f , 0.0f, 1.0f },
		{ 0.05f , 0.0f , 0.0f, 1.0f },
		{ 0.05f , 0.05f , 0.05f, 1.0f },
		{ 0.05f , 0.05f , 0.0f, 1.0f }
	};

	GLfloat materialDiffuse[24][4] = {
		{ 0.07568f, 0.61424f, 0.07568f, 1.0f },
		{ 0.54f , 0.89f , 0.63f , 1.0f },
		{ 0.18275f , 0.17f , 0.22525f , 1.0f },
		{ 1.0f , 0.829f , 0.829f , 1.0f },
		{ 0.61424f , 0.04136f , 0.04136f , 1.0f },
		{ 0.396f , 0.74151f , 0.69102f , 1.0f },

		{ 0.780392f , 0.568627f , 0.113725f , 1.0f },
		{ 0.714f , 0.4284f , 0.18144f , 1.0f },
		{ 0.4f , 0.4f , 0.4f , 1.0f },
		{ 0.7038f , 0.27048f , 0.0828f , 1.0f },
		{ 0.75164f , 0.60648f , 0.22648f , 1.0f },
		{ 0.50754f , 0.50754f , 0.50754f , 1.0f },

		{ 0.01f , 0.01f , 0.01f , 1.0f },
		{ 0.0f , 0.0520980392f ,  0.50980392f , 1.0f },
		{ 0.1f , 0.35f , 0.2f , 1.0f },
		{ 0.5f , 0.0f , 0.0f , 1.0f },
		{ 0.55f , 0.55f , 0.55f , 1.0f },
		{ 0.5f , 0.5f , 0.0f , 1.0f },

		{ 0.01f , 0.01f , 0.01f , 1.0f },
		{ 0.4f , 0.5f , 0.5f , 1.0f },
		{ 0.4f , 0.5f , 0.4f , 1.0f },
		{ 0.5f , 0.4f , 0.4f , 1.0f },
		{ 0.5f , 0.5f , 0.5f , 1.0f },
		{ 0.5f , 0.5f , 0.4f , 1.0f }

	};

	GLfloat materialSpecular[24][4] = {
		{ 0.633f , 0.727811f , 0.633f , 1.0f },
		{ 0.316228f , 0.316228f , 0.316228f , 1.0f },
		{ 0.332741f , 0.328634f , 0.346435f , 1.0f },
		{ 0.296648f , 0.296648f , 0.296648f , 1.0f },
		{ 0.727811f , 0.626959f , 0.626959f , 1.0f },
		{ 0.297254f , 0.30829f , 0.306678f , 1.0f },

		{ 0.992157f , 0.941176f , 0.807843f , 1.0f },
		{ 0.393548f , 0.271906f , 0.166721f , 1.0f },
		{ 0.774597f , 0.774597f , 0.774597f , 1.0f },
		{ 0.256777f , 0.137622f , 0.086014f , 1.0f },
		{ 0.628281f , 0.555802f , 0.366065f , 1.0f },
		{ 0.508273f , 0.508273f , 0.508273f , 1.0f },

		{ 0.5f , 0.5f , 0.5f , 1.0f },
		{ 0.50196078f , 0.50196078f , 0.50196078f , 1.0f },
		{ 0.45f , 0.5f , 0.45f , 1.0f },
		{ 0.7f , 0.6f , 0.6f , 1.0f },
		{ 0.7f , 0.7f , 0.7f , 1.0f },
		{ 0.6f , 0.6f , 0.5f , 1.0f },

		{ 0.4f , 0.4f , 0.4f , 1.0f },
		{ 0.04f , 0.7f , 0.7f , 1.0f },
		{ 0.04f , 0.7f , 0.04f , 1.0f },
		{ 0.7f , 0.04f , 0.04f , 1.0f },
		{ 0.7f , 0.7f , 0.7f , 1.0f },
		{ 0.7f , 0.7f , 0.04f , 1.0f }
	};

	GLfloat materialShininess[24] = {
		 0.6f * 128 ,
		 0.1f * 128 ,
		 0.3f * 128 ,
		 0.088f * 128 ,
		 0.6f * 128 ,
		 0.1f * 128 ,

		 0.21794872f * 128 , 
		 0.2f * 128 ,
		 0.6f * 128 ,
		 0.1f * 128 ,
		 0.4f * 128 ,
		 0.4f * 128 ,

		 0.25f * 128 ,
		 0.25f * 128 ,
		 0.25f * 128 ,
		 0.25f * 128 ,
		 0.25f * 128 ,
		 0.25f * 128 ,

		 0.078125f * 128 ,
		 0.078125f * 128 ,
		 0.078125f * 128 ,
		 0.078125f * 128 ,
		 0.078125f * 128 ,
		 0.078125f * 128 
	};

	int viewPortWidth = width/4;
	int viewPortHeight = height/6;
	int viewPortX = 0;
	int viewPortY = 0;


	gAngle += 0.01f;
	if (gAngle >= 2*3.145)
	{
		gAngle = 0.0f;
	}
	
	if (gbXPressed)
	{
		lightPosition[1] = 100.0f * cos(gAngle);
		lightPosition[2] = 100.0f * sin(gAngle);
		lightPosition[0] = 0.0f;
	}
	else if (gbYPressed)
	{
		lightPosition[0] = 100.0f * cos(gAngle);
		lightPosition[2] = 100.0f * sin(gAngle);
		lightPosition[1] = 0.0f;
	}
	else if (gbZPressed)
	{
		lightPosition[0] = 100.0f * cos(gAngle);
		lightPosition[1] = 100.0f * sin(gAngle);
		lightPosition[2] = 0.0f;
	}

	
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(shaderProgramObject);
    

	for (int i = 1; i < 25; i++)
	{
		[self viewPort: viewPortWidth height:viewPortHeight viewPortX:viewPortX viewPortY:viewPortY];
		//glViewport(viewPortX, viewPortY, (GLsizei)viewPortWidth, (GLsizei)viewPortHeight);
//			fprintf(gpFile, "viewPortX %d and i %d\n", viewPortX, i);

		if (light == true)
		{
			glUniform1i(L_KeyPressed_uniform, 1);

			glUniform3fv(La_uniform, 1, lightAmbient);
			glUniform3fv(Ld_uniform, 1, lightDiffuse);
			glUniform3fv(Ls_uniform, 1, lightSpecular);
			glUniform4fv(light_position_uniform, 1, lightPosition);

			glUniform3fv(Ka_uniform, 1, materialAmbient[i-1]);
			glUniform3fv(Kd_uniform, 1, materialDiffuse[i-1]);
			glUniform3fv(Ks_uniform, 1, materialSpecular[i-1]);
			glUniform1f(material_shininess_uniform, materialShininess[i-1]);
		}
		else
		{
			glUniform1i(L_KeyPressed_uniform, 0);
		}

		vmath::mat4 modelMatrix = vmath::mat4::identity();
		vmath::mat4 viewMatrix = vmath::mat4::identity();
	    
	    modelMatrix = vmath::translate(0.0f, 0.0f, -8.0f);

	    viewPortY += viewPortHeight;

		if (i % 6 == 0)
		{
			viewPortX += viewPortWidth;
			viewPortY = 0;
		}
		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform, 1,GL_FALSE, perspectiveProjectionMatrix);

	   	draw();
	}

    glUseProgram(0);	

	
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27: // Esc key
            [ self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self]; // repainting occurs automatically
            break;
        case 'l':
        case 'L':
        	light = !light;
        	break;
        case 'x':
        case 'X':
	        gbXPressed = true;
			gbYPressed = false;
			gbZPressed = false;
			break;
		case 'y':
        case 'Y':
	        gbXPressed = false;
			gbYPressed = true;
			gbZPressed = false;
			break;
		case 'z':
        case 'Z':
	        gbXPressed = false;
			gbYPressed = false;
			gbZPressed = true;
			break;
		
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    // code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    // code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    // code
}

- (void) dealloc
{
	// code    

    glDetachShader(shaderProgramObject, vertexShaderObject);
   
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
   
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;

	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}
