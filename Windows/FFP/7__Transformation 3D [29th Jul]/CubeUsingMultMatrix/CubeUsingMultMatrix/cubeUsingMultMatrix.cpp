#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>

#define WIN_WIDTH 600
#define WIN_HEIGHT 600
#define PI 3.145
#define SIDE_LENGTH 1.0f

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float angleCube = 0.0f;
float identityMatrix[16] = { 0.0f };
float translateMatrix[16] = { 0.0f };
float scaleMatrix[16] = { 0.0f };
float xRotationMatrix[16] = { 0.0f };
float yRotationMatrix[16] = { 0.0f };
float zRotationMatrix[16] = { 0.0f };

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : Cube using MultMatrix"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
				update();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initMatrix()
{
	//IDENTITY MATRIX
	identityMatrix[0] = 1.0f;
	identityMatrix[1] = 0.0f;
	identityMatrix[2] = 0.0f;
	identityMatrix[3] = 0.0f;
	
	identityMatrix[4] = 0.0f;
	identityMatrix[5] = 1.0f;
	identityMatrix[6] = 0.0f;
	identityMatrix[7] = 0.0f;
	
	identityMatrix[8] = 0.0f;
	identityMatrix[9] = 0.0f;
	identityMatrix[10] = 1.0f;
	identityMatrix[11] = 0.0f;
	
	identityMatrix[12] = 0.0f;
	identityMatrix[13] = 0.0f;
	identityMatrix[14] = 0.0f;
	identityMatrix[15] = 1.0f;

	//TRANSLATE MATRIX
	translateMatrix[0] = 1.0f;
	translateMatrix[1] = 0.0f;
	translateMatrix[2] = 0.0f;
	translateMatrix[3] = 0.0f;

	translateMatrix[4] = 0.0f;
	translateMatrix[5] = 1.0f;
	translateMatrix[6] = 0.0f;
	translateMatrix[7] = 0.0f;

	translateMatrix[8] = 0.0f;
	translateMatrix[9] = 0.0f;
	translateMatrix[10] = 1.0f;
	translateMatrix[11] = 0.0f;

	translateMatrix[12] = 0.0f;
	translateMatrix[13] = 0.0f;
	translateMatrix[14] = -6.0f;
	translateMatrix[15] = 1.0f;

	//SCALE MATRIX
	scaleMatrix[0] = 0.75f;
	scaleMatrix[1] = 0.0f;
	scaleMatrix[2] = 0.0f;
	scaleMatrix[3] = 0.0f;

	scaleMatrix[4] = 0.0f;
	scaleMatrix[5] = 0.75f;
	scaleMatrix[6] = 0.0f;
	scaleMatrix[7] = 0.0f;

	scaleMatrix[8] = 0.0f;
	scaleMatrix[9] = 0.0f;
	scaleMatrix[10] = 0.75f;
	scaleMatrix[11] = 0.0f;

	scaleMatrix[12] = 0.0f;
	scaleMatrix[13] = 0.0f;
	scaleMatrix[14] = 0.0f;
	scaleMatrix[15] = 1.0f;
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	initMatrix();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void update(void)
{
	if (angleCube > 360)
		angleCube = 0.0f;
	angleCube += 0.05f;
}


void rotationMatrix()
{
	float angleRadian = angleCube * PI / 180;
	float cosTheta = cos(angleRadian);
	float sinTheta = sin(angleRadian);

	//XROTATION MATRIX
	xRotationMatrix[0] = 1.0f;
	xRotationMatrix[1] = 0.0f;
	xRotationMatrix[2] = 0.0f;
	xRotationMatrix[3] = 0.0f;

	xRotationMatrix[4] = 0.0f;
	xRotationMatrix[5] = cosTheta;
	xRotationMatrix[6] = -sinTheta;
	xRotationMatrix[7] = 0.0f;

	xRotationMatrix[8] = 0.0f;
	xRotationMatrix[9] = sinTheta;
	xRotationMatrix[10] = cosTheta;
	xRotationMatrix[11] = 0.0f;

	xRotationMatrix[12] = 0.0f;
	xRotationMatrix[13] = 0.0f;
	xRotationMatrix[14] = 0.0f;
	xRotationMatrix[15] = 1.0f;

	//YROTATION MATRIX
	yRotationMatrix[0] = cosTheta;
	yRotationMatrix[1] = 0.0f;
	yRotationMatrix[2] = sinTheta;
	yRotationMatrix[3] = 0.0f;

	yRotationMatrix[4] = 0.0f;
	yRotationMatrix[5] = 1.0f;
	yRotationMatrix[6] = 0.0f;
	yRotationMatrix[7] = 0.0f;

	yRotationMatrix[8] = -sinTheta;
	yRotationMatrix[9] = 0.0f;
	yRotationMatrix[10] = cosTheta;
	yRotationMatrix[11] = 0.0f;

	yRotationMatrix[12] = 0.0f;
	yRotationMatrix[13] = 0.0f;
	yRotationMatrix[14] = 0.0f;
	yRotationMatrix[15] = 1.0f;

	//ZROTATION MATRIX
	zRotationMatrix[0] = cosTheta;
	zRotationMatrix[1] = -sinTheta;
	zRotationMatrix[2] = 0.0f;
	zRotationMatrix[3] = 0.0f;

	zRotationMatrix[4] = sinTheta;
	zRotationMatrix[5] = cosTheta;
	zRotationMatrix[6] = 0.0f;
	zRotationMatrix[7] = 0.0f;

	zRotationMatrix[8] = 0.0f;
	zRotationMatrix[9] = 0.0f;
	zRotationMatrix[10] = 1.0f;
	zRotationMatrix[11] = 0.0f;

	zRotationMatrix[12] = 0.0f;
	zRotationMatrix[13] = 0.0f;
	zRotationMatrix[14] = 0.0f;
	zRotationMatrix[15] = 1.0f;
}

void DrawSquare(float vertice1[3], float vertice2[3], float vertice3[3], float vertice4[3], float color[3])
{
	glBegin(GL_QUADS);
	glColor3f(color[0], color[1], color[2]);
	glVertex3f(vertice1[0], vertice1[1], vertice1[2]);
	glVertex3f(vertice2[0], vertice2[1], vertice2[2]);
	glVertex3f(vertice3[0], vertice3[1], vertice3[2]);
	glVertex3f(vertice4[0], vertice4[1], vertice4[2]);
	glEnd();
}

void DrawMultiColorCube()
{
	float points[10][3] = { { SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH },
	{ -SIDE_LENGTH,SIDE_LENGTH, SIDE_LENGTH },
	{ -SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH },
	{ SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH },
	{ SIDE_LENGTH,SIDE_LENGTH, -SIDE_LENGTH },
	{ -SIDE_LENGTH, SIDE_LENGTH,-SIDE_LENGTH },
	{ -SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH },
	{ SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH }
	};

	float color1[3] = { 1.0f, 0.0f, 0.0f }; //red
	float color2[3] = { 0.0f, 1.0f, 0.0f }; //green
	float color3[3] = { 0.0f, 0.0f, 1.0f }; //blue
	float color4[3] = { 1.0f, 1.0f, 0.0f }; //yellow
	float color5[3] = { 0.0f, 1.0f, 1.0f }; //cyan
	float color6[3] = { 1.0f, 0.0f, 1.0f }; //magenta

	DrawSquare(points[0], points[1], points[2], points[3], color1);//top-red
	DrawSquare(points[4], points[5], points[6], points[7], color2);//bottom-green
	DrawSquare(points[1], points[5], points[6], points[2], color3);//left-blue
	DrawSquare(points[0], points[4], points[7], points[3], color4);//right-yellow
	DrawSquare(points[0], points[1], points[5], points[4], color5);//front-cyan
	DrawSquare(points[3], points[2], points[6], points[7], color6);//rear-magenta
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	rotationMatrix();
	glLoadMatrixf(identityMatrix);
	glMultMatrixf(translateMatrix);
	glMultMatrixf(scaleMatrix);
	glMultMatrixf(xRotationMatrix);
	glMultMatrixf(yRotationMatrix);
	glMultMatrixf(zRotationMatrix);

	DrawMultiColorCube();
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	if (width == 0)
		width = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
		gluPerspective(45.0f, 1.0f*(GLfloat)height / (GLfloat)width, 0.1f, 100.0f);
	else
		gluPerspective(45.0f, 1.0f*(GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	//glOrtho(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}
