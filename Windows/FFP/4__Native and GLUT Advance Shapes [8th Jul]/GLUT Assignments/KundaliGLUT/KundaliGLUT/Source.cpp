
#include <GL/freeglut.h>
#include <math.h>

#define PI 3.145
#define NUM_OF_LINES 40

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen
#define WIN_WIDTH 600
#define WIN_HEIGHT 600

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT); //to declare initial window size
	glutInitWindowPosition((width - WIN_WIDTH) / 2, (height - WIN_HEIGHT) / 2); //to declare initial window position
	glutCreateWindow("GLUT:: Kundali!!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void DrawKundali()
{
	glColor3f(1.0f, 0.0f, 0.0f);
	glLineWidth(2);
	glBegin(GL_LINES);
	//rectangle
	glVertex3f(0.6f, 0.4f, 0.0f);
	glVertex3f(-0.6f, 0.4f, 0.0f);

	glVertex3f(-0.6f, 0.4f, 0.0f);
	glVertex3f(-0.6f, -0.4f, 0.0f);

	glVertex3f(-0.6f, -0.4f, 0.0f);
	glVertex3f(0.6f, -0.4f, 0.0f);

	glVertex3f(0.6f, -0.4f, 0.0f);
	glVertex3f(0.6f, 0.4f, 0.0f);

	//diagonals
	glVertex3f(0.6f, 0.4f, 0.0f);
	glVertex3f(-0.6f, -0.4f, 0.0f);

	glVertex3f(-0.6f, 0.4f, 0.0f);
	glVertex3f(0.6f, -0.4f, 0.0f);

	//internal parallelogram
	glVertex3f(0.0f, 0.4f, 0.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);

	glVertex3f(-0.6f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.4f, 0.0f);

	glVertex3f(0.0f, -0.4f, 0.0f);
	glVertex3f(0.6f, 0.0f, 0.0f);

	glVertex3f(0.6f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.4f, 0.0f);

	glEnd();
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	DrawKundali();
	//to process buffered OpenGL Routines
	glutSwapBuffers();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	// code
}

void uninitialize(void)
{
	// code
}

