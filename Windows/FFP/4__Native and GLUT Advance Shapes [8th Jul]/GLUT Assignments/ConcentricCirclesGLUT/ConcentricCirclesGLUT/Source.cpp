
#include <GL/freeglut.h>
#include <math.h>

#define PI 3.145
#define NUM_OF_LINES 40

//global variable declaration
bool bFullscreen = false; //variable to toggle for fullscreen
#define WIN_WIDTH 600
#define WIN_HEIGHT 600

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT); //to declare initial window size
	glutInitWindowPosition((width - WIN_WIDTH) / 2, (height - WIN_HEIGHT) / 2); //to declare initial window position
	glutCreateWindow("GLUT:: Concentric Circles!!!");

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void DrawCircle(float radius, float color[3])
{
	float angle;
	glLineWidth(2);
	glBegin(GL_POINTS);
	glColor3f(color[0], color[1], color[2]);
	for (angle = 0.0f; angle < 2 * PI; angle = angle + 0.001f)
		glVertex3f(radius*cos(angle), radius*sin(angle), 0.0f);
	glEnd();
}

void DrawConcentricCircle()
{
	float points[10][3] = { 0 };
	float color[10][3] = {
		{ 1.0f, 0.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 1.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 1.0f },
		{ 1.0f, 0.0f, 1.0f },
		{ 1.0f, 1.0f, 1.0f },
		{ 0.5f, 0.5f, 0.5f },
		{ 0.5f, 0.5f, 0.0f },
		{ 0.0f, 0.5f, 0.5f }
	};
	int i = 0;
	float radius = 0.09f;

	for (i = 0;i < 10;i++)
	{
		DrawCircle(radius, color[i]);
		radius += 0.09f;
	}
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	DrawConcentricCircle();
	//to process buffered OpenGL Routines
	glutSwapBuffers();
}

void initialize(void)
{
	//code
	//to select clearing (background) clear
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	// code
}

void uninitialize(void)
{
	// code
}

