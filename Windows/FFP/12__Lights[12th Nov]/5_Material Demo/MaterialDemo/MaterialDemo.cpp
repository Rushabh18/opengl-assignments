#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>

#define WIN_WIDTH 600
#define WIN_HEIGHT 600

#define SPHERE_RADIUS 0.2f
#define SPHERE_GAP_X SPHERE_RADIUS * 6
#define SPHERE_GAP_Y SPHERE_RADIUS/2

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
typedef float axis;

struct Vertex
{
	axis x;
	axis y;
	axis z;
};


HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool gbLightsEnable = false;

bool gbXAxis = false;
bool gbYAxis = false;
bool gbZAxis = false;

float angleLight = 0.0f;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat lightModelAmbiet[] = { 0.2f, 0.2f, 0.2f, 0.2f };
GLfloat lightModelLocalViewer =  0.0f ;

GLfloat materialAmbient[24][4] = {
									{ 0.0215f, 0.1745f, 0.0215f, 1.0f },
								    { 0.135f , 0.225f , 0.1575f, 1.0f }, 
									{ 0.05375f , 0.05f , 0.06625f, 1.0f },
									{ 0.25f , 0.20725f , 0.20725f, 1.0f },
									{ 0.1745f , 0.01175f , 0.01175f, 1.0f },
									{ 0.1f , 0.18725f , 0.1745f, 1.0f },

									{ 0.329412f , 0.223529f , 0.027451f, 1.0f },
									{ 0.2125f , 0.1275f , 0.054f, 1.0f },
									{ 0.25f , 0.25f , 0.25f, 1.0f },
									{ 0.19125f , 0.0735f , 0.0225f, 1.0f },
									{ 0.24725f , 0.1995f , 0.0745f, 1.0f },
									{ 0.19225f , 0.19225f , 0.19225f, 1.0f },

									{ 0.0f , 0.0f , 0.0f, 1.0f },
									{ 0.0f , 0.1f , 0.06f, 1.0f },
									{ 0.0f , 0.0f , 0.0f, 1.0f },
									{ 0.0f , 0.0f , 0.0f, 1.0f },
									{ 0.0f , 0.0f , 0.0f, 1.0f },
									{ 0.0f , 0.0f , 0.0f, 1.0f },

									{ 0.02f , 0.02f , 0.02f, 1.0f },
									{ 0.0f , 0.05f , 0.05f, 1.0f },
									{ 0.0f , 0.05f , 0.0f, 1.0f }, 
									{ 0.05f , 0.0f , 0.0f, 1.0f }, 
									{ 0.05f , 0.05f , 0.05f, 1.0f }, 
									{ 0.05f , 0.05f , 0.0f, 1.0f }
								};
				
GLfloat materialDiffuse[24][4] = {
									{ 0.07568f, 0.61424f, 0.07568f, 1.0f },
									{ 0.54f , 0.89f , 0.63f , 1.0f},
									{ 0.18275f , 0.17f , 0.22525f , 1.0f },
									{ 1.0f , 0.829f , 0.829f , 1.0f },
									{ 0.61424f , 0.04136f , 0.04136f , 1.0f },
									{ 0.396f , 0.74151f , 0.69102f , 1.0f },

									{ 0.780392f , 0.568627f , 0.113725f , 1.0f },
									{ 0.714f , 0.4284f , 0.18144f , 1.0f },
									{ 0.4f , 0.4f , 0.4f , 1.0f },
									{ 0.7038f , 0.27048f , 0.0828f , 1.0f },
									{ 0.75164f , 0.60648f , 0.22648f , 1.0f },
									{ 0.50754f , 0.50754f , 0.50754f , 1.0f },

									{ 0.01f , 0.01f , 0.01f , 1.0f },
									{ 0.0f , 0.0520980392f ,  0.50980392f , 1.0f },
									{ 0.1f , 0.35f , 0.2f , 1.0f },
									{ 0.5f , 0.0f , 0.0f , 1.0f },
									{ 0.55f , 0.55f , 0.55f , 1.0f },
									{ 0.5f , 0.5f , 0.0f , 1.0f },

									{ 0.01f , 0.01f , 0.01f , 1.0f },
									{ 0.4f , 0.5f , 0.5f , 1.0f },
									{ 0.4f , 0.5f , 0.4f , 1.0f },
									{ 0.5f , 0.4f , 0.4f , 1.0f },
									{ 0.5f , 0.5f , 0.5f , 1.0f },
									{ 0.5f , 0.5f , 0.4f , 1.0f }

};

GLfloat materialSpecular[24][4] = { 
									{ 0.633f , 0.727811f , 0.633f , 1.0f },
									{ 0.316228f , 0.316228f , 0.316228f , 1.0f },
									{ 0.332741f , 0.328634f , 0.346435f , 1.0f },
									{ 0.296648f , 0.296648f , 0.296648f , 1.0f },
									{ 0.727811f , 0.626959f , 0.626959f , 1.0f },
									{ 0.297254f , 0.30829f , 0.306678f , 1.0f },

									{ 0.992157f , 0.941176f , 0.807843f , 1.0f },
									{ 0.393548f , 0.271906f , 0.166721f , 1.0f },
									{ 0.774597f , 0.774597f , 0.774597f , 1.0f },
									{ 0.256777f , 0.137622f , 0.086014f , 1.0f },
									{ 0.628281f , 0.555802f , 0.366065f , 1.0f },
									{ 0.508273f , 0.508273f , 0.508273f , 1.0f },

									{ 0.5f , 0.5f , 0.5f , 1.0f },
									{ 0.50196078f , 0.50196078f , 0.50196078f , 1.0f },
									{ 0.45f , 0.5f , 0.45f , 1.0f },
									{ 0.7f , 0.6f , 0.6f , 1.0f },
									{ 0.7f , 0.7f , 0.7f , 1.0f },
									{ 0.6f , 0.6f , 0.5f , 1.0f },

									{ 0.4f , 0.4f , 0.4f , 1.0f },
									{ 0.04f , 0.7f , 0.7f , 1.0f },
									{ 0.04f , 0.7f , 0.04f , 1.0f },
									{ 0.7f , 0.04f , 0.04f , 1.0f },
									{ 0.7f , 0.7f , 0.7f , 1.0f },
									{ 0.7f , 0.7f , 0.04f , 1.0f }
};

GLfloat materialShininess[24] = { 
									{ 0.6f * 128 },
									{ 0.1f * 128 },
									{ 0.3f * 128 },
									{ 0.088f * 128 },
									{ 0.6f * 128 },
									{ 0.1f * 128 },

									{ 0.21794872f * 128 },
									{ 0.2f * 128 },
									{ 0.6f * 128 },
									{ 0.1f * 128 },
									{ 0.4f * 128 },
									{ 0.4f * 128 },

									{ 0.25f * 128 },
									{ 0.25f * 128 },
									{ 0.25f * 128 },
									{ 0.25f * 128 },
									{ 0.25f * 128 },
									{ 0.25f * 128 },

									{ 0.078125f * 128 },
									{ 0.078125f * 128 },
									{ 0.078125f * 128 },
									{ 0.078125f * 128 },
									{ 0.078125f * 128 },
									{ 0.078125f * 128 }
};

GLUquadric *quadric = NULL;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Three Lights"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 'F'://for 'f' or 'F'
		case 'f':
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 'l':
		case 'L':
			if (gbLightsEnable)
			{
				gbLightsEnable = false;
				glDisable(GL_LIGHTING);
			}
			else
			{
				gbLightsEnable = true;
				glEnable(GL_LIGHTING);
			}
			break;
		case 'x':
		case 'X':
			gbXAxis = true;
			gbYAxis = false;
			gbZAxis = false;
			lightPosition[0] = 0.0f;
			lightPosition[1] = 1.0f;
			lightPosition[2] = 0.0f;
			break;

		case 'y':
		case 'Y':
			gbYAxis = true;
			gbXAxis = false;
			gbZAxis = false;
			lightPosition[0] = 1.0f;
			lightPosition[1] = 0.0f;
			lightPosition[2] = 0.0f;
			break;

		case 'z':
		case 'Z':
			gbZAxis = true;
			gbYAxis = false;
			gbXAxis = false;
			lightPosition[0] = 0.0f;
			lightPosition[1] = 1.0f;
			lightPosition[2] = 0.0f;
			break;

		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	quadric = gluNewQuadric();

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmbiet);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, lightModelLocalViewer);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

	glEnable(GL_LIGHT0);

	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void DrawSphere()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, SPHERE_RADIUS, 100, 100);
}

void DrawGridOfSpheres()
{
	int i = 0;
	int j = 0;
	int index = 0;
	float startX = 12 * SPHERE_RADIUS;
	float startY = 6 * SPHERE_RADIUS;
	float x = -startX;
	float y = startY;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 6; j++)
		{
			glPushMatrix();
				glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient[index]);
				glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse[index]);
				glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular[index]);
				glMaterialf(GL_FRONT, GL_SHININESS, materialShininess[index]);
				glTranslatef(x, y, 0.0f); 
				DrawSphere();
			glPopMatrix();
			index++;
			y = y - 2 * SPHERE_RADIUS - SPHERE_GAP_Y;
		}
		x = x + 2 * SPHERE_RADIUS + SPHERE_GAP_X;
		y = startY;
	}
}

void ApplyLights()
{
	glPushMatrix();
		if (gbXAxis == true)
		{
			glRotatef(angleLight, 1.0f, 0.0f, 0.0f);
		}
		else if (gbYAxis == true)
		{
			glRotatef(angleLight, 0.0f, 1.0f, 0.0f);
		}
		else if (gbZAxis == true)
		{
			glRotatef(angleLight, 0.0f, 0.0f, 1.0f);
		}
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glPopMatrix();
}

void update(void)
{
	if (angleLight > 360)
	{
		angleLight = 1.0f;
	}
	angleLight += 1.0f;
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//View Transformations
	glTranslatef(0.0f, 0.0f, -4.0f);
	ApplyLights();
	DrawGridOfSpheres();

	SwapBuffers(ghdc);
	update();
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	if (width == 0)
		width = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
		gluPerspective(45.0f, 1.0f*(GLfloat)height / (GLfloat)width, 0.1f, 100.0f);
	else
		gluPerspective(45.0f, 1.0f*(GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}
