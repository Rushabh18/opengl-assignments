#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>

#define WIN_WIDTH 600
#define WIN_HEIGHT 600
#define PI 3.145
#define SIDE_LENGTH 1.0f

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Global variable declarations
typedef float axis;

struct Vertex
{
	axis x;
	axis y;
	axis z;
};


HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool gbLightsEnable = false;

float angleCube = 0.0f;
float anglePyramid = 0.0f;

GLfloat lightAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, -1.0f, 1.0f, 0.0f };

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Cube Pyramid Lights"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 'F'://for 'f' or 'F'
		case 'f':
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 'l':
		case 'L':
			if (gbLightsEnable)
			{
				gbLightsEnable = false;
				glDisable(GL_LIGHTING);
			}
			else
			{
				gbLightsEnable = true;
				glEnable(GL_LIGHTING);
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	glEnable(GL_LIGHT0);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void update(void)
{
	if (anglePyramid > 360)
		anglePyramid = 0.0f;
	anglePyramid += 0.1f;

	if (angleCube > 360)
		angleCube = 0.0f;
	angleCube += 0.1f;
}

void FillVertexStruct(Vertex *v, axis x, axis y, axis z)
{
	v->x = x;
	v->y = y;
	v->z = z;
}

void FillVertexQuadArray(Vertex v[4], Vertex v1, Vertex v2, Vertex v3, Vertex v4)
{
	FillVertexStruct(&v[0], v1.x, v1.y, v1.z);
	FillVertexStruct(&v[1], v2.x, v2.y, v2.z);
	FillVertexStruct(&v[2], v3.x, v3.y, v3.z);
	FillVertexStruct(&v[3], v4.x, v4.y, v4.z);
}

void FillVertexTriArray(Vertex v[3], Vertex v1, Vertex v2, Vertex v3)
{
	FillVertexStruct(&v[0], v1.x, v1.y, v1.z);
	FillVertexStruct(&v[1], v2.x, v2.y, v2.z);
	FillVertexStruct(&v[2], v3.x, v3.y, v3.z);
}

void DrawSquare(Vertex vArr[4], Vertex normal)
{
	glBegin(GL_QUADS);
	glNormal3f(normal.x, normal.y, normal.z);
	glVertex3f(vArr[0].x, vArr[0].y, vArr[0].z);
	glVertex3f(vArr[1].x, vArr[1].y, vArr[1].z);
	glVertex3f(vArr[2].x, vArr[2].y, vArr[2].z);
	glVertex3f(vArr[3].x, vArr[3].y, vArr[3].z);
	glEnd();
}

void DrawCube()
{
	Vertex squareVertices[4];
	Vertex cubeVertices[8];
	Vertex normalVertices[6];

	FillVertexStruct(&cubeVertices[0], SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[1], -SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[2], -SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[3], SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[4], SIDE_LENGTH, SIDE_LENGTH, -SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[5], -SIDE_LENGTH, SIDE_LENGTH, -SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[6], -SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[7], SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH);

	FillVertexStruct(&normalVertices[0], 0.0f, 0.0f, 1.0f);//front
	FillVertexStruct(&normalVertices[1], 0.0f, 0.0f, -1.0f);//back
	FillVertexStruct(&normalVertices[2], -1.0f, 0.0f, 0.0f);//left
	FillVertexStruct(&normalVertices[3], 1.0f, 0.0f, 0.0f);//right
	FillVertexStruct(&normalVertices[4], 0.0f, 1.0f, 0.0f);//top
	FillVertexStruct(&normalVertices[5], 0.0f, -1.0f, 0.0f);//bottom

	FillVertexQuadArray(squareVertices, cubeVertices[0], cubeVertices[1], cubeVertices[2], cubeVertices[3]);
	DrawSquare(squareVertices, normalVertices[0]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[4], cubeVertices[5], cubeVertices[6], cubeVertices[7]);
	DrawSquare(squareVertices, normalVertices[1]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[1], cubeVertices[5], cubeVertices[6], cubeVertices[2]);
	DrawSquare(squareVertices, normalVertices[2]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[0], cubeVertices[4], cubeVertices[7], cubeVertices[3]);
	DrawSquare(squareVertices, normalVertices[3]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[0], cubeVertices[1], cubeVertices[5], cubeVertices[4]);
	DrawSquare(squareVertices, normalVertices[4]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[3], cubeVertices[2], cubeVertices[6], cubeVertices[7]);
	DrawSquare(squareVertices, normalVertices[5]);//front

}

void DrawTriangle(Vertex vArr[3], Vertex normal)
{
	glBegin(GL_TRIANGLES);
	glNormal3f(normal.x, normal.y, normal.z);
	glVertex3f(vArr[0].x, vArr[0].y, vArr[0].z);
	glVertex3f(vArr[1].x, vArr[1].y, vArr[1].z);
	glVertex3f(vArr[2].x, vArr[2].y, vArr[2].z);
	glEnd();
}

void DrawPyramid()
{
	Vertex triangleVertices[3];
	Vertex pyramidVertices[5];
	Vertex normalVertices[4];

	FillVertexStruct(&pyramidVertices[0], 0.0f, SIDE_LENGTH, 0.0f);
	FillVertexStruct(&pyramidVertices[1], SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&pyramidVertices[2], -SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&pyramidVertices[3], -SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH);
	FillVertexStruct(&pyramidVertices[4], SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH);

	FillVertexStruct(&normalVertices[0], 0.0f, 0.0f, 1.0f);//front
	FillVertexStruct(&normalVertices[1], -1.0f, 0.0f, -1.0f);//left
	FillVertexStruct(&normalVertices[2], 0.0f, 0.0f, -1.0f);//back
	FillVertexStruct(&normalVertices[3], 1.0f, 0.0f, 0.0f);//right
	
	FillVertexTriArray(triangleVertices, pyramidVertices[0], pyramidVertices[1], pyramidVertices[2]);
	DrawTriangle(triangleVertices, normalVertices[0]);//front

	FillVertexTriArray(triangleVertices, pyramidVertices[0], pyramidVertices[2], pyramidVertices[3]);
	DrawTriangle(triangleVertices, normalVertices[1]);//left

	FillVertexTriArray(triangleVertices, pyramidVertices[0], pyramidVertices[3], pyramidVertices[4]);
	DrawTriangle(triangleVertices, normalVertices[2]);//back

	FillVertexTriArray(triangleVertices, pyramidVertices[0], pyramidVertices[4], pyramidVertices[1]);
	DrawTriangle(triangleVertices, normalVertices[3]);//right
}


void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//View Transformations
	glTranslatef(-1.5f, 0.0f, -6.0f);
	glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);

	DrawPyramid();
	
	glLoadIdentity();
	glTranslatef(1.5f, 0.0f, -6.0f);
	glRotatef(angleCube, 1.0f, 1.0f, 1.0f);
	DrawCube();

	update();
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	if (width == 0)
		width = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
		gluPerspective(45.0f, 1.0f*(GLfloat)height / (GLfloat)width, 0.1f, 100.0f);
	else
		gluPerspective(45.0f, 1.0f*(GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}
