// MessageBox.cpp : Defines the entry point for the application.
//
#include <windows.h>
#include "stdafx.h"

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int m_r, m_g, m_b;
WINDOWPLACEMENT wPrev;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	//code 
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(hInstance, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	//create window
	hwnd = CreateWindow(szAppName,
			TEXT("MESSAGE BOX"),
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			NULL,
			NULL,
			hInstance,
			NULL);

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	HBRUSH hBrush;
	long wndStyle = 0;
	HMONITOR hMonitor;
	MONITORINFO monitorInfo;
	DEVMODE newSettings;

	int xPos = 0; 
	int yPos = 0;
	int width = 0, height = 0;
	int cX =0, cY = 0;
	wchar_t str[100]={0};
	//code
	switch(iMsg)
	{
	case WM_CREATE:
		MessageBox(NULL, L"Window  Created", L"WM_CREATE", MB_OK);
		break;
	case WM_KEYDOWN:
		switch(wParam)
		{
		case 0x1B:
			MessageBox(NULL, L"Escape key pressed", L"WM_KEYDOWN", MB_OK);
			break;
		case 0x41:
			MessageBox(NULL, L"'A' key pressed", L"WM_KEYDOWN", MB_OK);
			break;
		case 0x4C:
			MessageBox(NULL, L"'L' key pressed", L"WM_KEYDOWN", MB_OK);
			break;
		case 0x54:
			MessageBox(NULL, L"'T' key pressed", L"WM_KEYDOWN", MB_OK);
			break;
		case 0x51:
			MessageBox(NULL, L"'Q' key pressed", L"WM_KEYDOWN", MB_OK);
			SendMessage(hwnd,WM_DESTROY,0,0);
			break;

		case 0x52:
			MessageBox(NULL, L"'R' key pressed", L"WM_KEYDOWN", MB_OK);
			m_r = 255;
			m_g = 0;
			m_b = 0;
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 0x47:
			MessageBox(NULL, L"'G' key pressed", L"WM_KEYDOWN", MB_OK);
			m_r = 0;
			m_g = 255;
			m_b = 0;
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 0x42:
			MessageBox(NULL, L"'B' key pressed", L"WM_KEYDOWN", MB_OK);
			m_r = 0;
			m_g = 0;
			m_b = 255;
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 0x4B:
			MessageBox(NULL, L"'K' key pressed", L"WM_KEYDOWN", MB_OK);
			m_r = 0;
			m_g = 0;
			m_b = 0;
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 0x57:
			MessageBox(NULL, L"'W' key pressed", L"WM_KEYDOWN", MB_OK);
			m_r = 255;
			m_g = 255;
			m_b = 255;
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 0x43:
			MessageBox(NULL, L"'C' key pressed", L"WM_KEYDOWN", MB_OK);
			m_r = 0;
			m_g = 255;
			m_b = 255;
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 0x59:
			MessageBox(NULL, L"'Y' key pressed", L"WM_KEYDOWN", MB_OK);
			m_r = 255;
			m_g = 255;
			m_b = 0;		
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 0x4D:
			MessageBox(NULL, L"'M' key pressed", L"WM_KEYDOWN", MB_OK);
			m_r = 255;
			m_g = 0;
			m_b = 255;			
			InvalidateRect(hwnd, NULL, TRUE);
			break;

		case 0x58:
			MessageBox(NULL, L"'X' key pressed", L"WM_KEYDOWN", MB_OK);
		
			SetWindowLong(hwnd,GWL_STYLE,WS_POPUP | WS_VISIBLE);
			memset(&newSettings,0,sizeof(newSettings));
			newSettings.dmSize=sizeof(newSettings);
			newSettings.dmPelsHeight = GetSystemMetrics(SM_CYSCREEN);
			newSettings.dmPelsWidth = GetSystemMetrics(SM_CXSCREEN);
			newSettings.dmFields =  DM_PELSWIDTH | DM_PELSHEIGHT; 
			wndStyle = ChangeDisplaySettings(NULL,CDS_FULLSCREEN);
			ShowWindow(hwnd,SW_MAXIMIZE);
			break;

		case 0x46:
			MessageBox(NULL, L"'F' key pressed", L"WM_KEYDOWN", MB_OK);
			width = GetSystemMetrics(SM_CXSCREEN);
			height = GetSystemMetrics(SM_CYSCREEN);
			GetClientRect(hwnd, &rc);
			cX = rc.right - rc.left;
			cY = rc.bottom - rc.top;
			if(cX == width && cY == height)
			{
				//already in fullscreen
				SetWindowLong(hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);
				SetWindowPlacement(hwnd, &wPrev);
				SetWindowPos(hwnd,HWND_TOP,0,0,0,0,
								SWP_NOZORDER | 
								SWP_FRAMECHANGED |
								SWP_NOMOVE |
								SWP_NOSIZE |
								SWP_NOOWNERZORDER );
				ShowCursor(true);
			}
			else
			{
				wndStyle = GetWindowLong(hwnd, GWL_STYLE);
				if(wndStyle & WS_OVERLAPPEDWINDOW)
				{
					wndStyle = wndStyle & (~WS_OVERLAPPEDWINDOW);
				}
				GetWindowPlacement(hwnd, &wPrev);
				wPrev.length = sizeof(wPrev);
				hMonitor = MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY);
				monitorInfo.cbSize = sizeof(MONITORINFO);
				GetMonitorInfo(hMonitor, &monitorInfo);
				SetWindowLong(hwnd,GWL_STYLE, wndStyle);
				SetWindowPos(hwnd, 
								HWND_TOP,
								monitorInfo.rcMonitor.left,
								monitorInfo.rcMonitor.top,
								monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left,
								monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top,
								SWP_NOZORDER | SWP_FRAMECHANGED);
				ShowCursor(false);
			}
			break;
		}
		break;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);
		hBrush = CreateSolidBrush(RGB(m_r,m_g,m_b));
		FillRect(hdc, &rc, hBrush);
		break;

	case WM_LBUTTONDOWN:
		xPos = LOWORD(lParam); 
		yPos = HIWORD(lParam);
		wsprintf(str,L"Left Button Pressed at (%d, %d)", xPos, yPos);
		MessageBox(NULL, str, L"WM_LBUTTONDOWN", MB_OK);
		break;

	case WM_DESTROY:
		MessageBox(NULL, L"Window Destroyed", L"WM_DESTROY", MB_OK);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
