//common headers
#include <windows.h>
#include <stdio.h>

//OpenGL headers
#include <gl/glew.h>
#include <gl/GL.h>

#include "vmath.h"
#include "Sphere.h"

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 800

// import libraries
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "Sphere.lib")

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gbLight = false;

bool bIsFKeyPressed = false;
bool bIsVKeyPressed = true;

float gNumVertices = 0;
float gNumElements = 0;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLfloat gAngle = 0.0f;

FILE *gpFile = NULL;

GLuint gVertexShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerVertex;
GLuint gShaderProgramObjectPerVertex;

GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerFragment;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;
GLuint V_KeyPressed_uniform;
GLuint F_KeyPressed_uniform;

GLuint La0_uniform;
GLuint Ld0_uniform;
GLuint Ls0_uniform;
GLuint light0_position_uniform;

GLuint La1_uniform;
GLuint Ld1_uniform;
GLuint Ls1_uniform;
GLuint light1_position_uniform;

GLuint La2_uniform;
GLuint Ld2_uniform;
GLuint Ls2_uniform;
GLuint light2_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light0Diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0Specular[] = { 1.0f, 0.0f, 0.0f ,0.0f };
GLfloat light0Position[] = { 000.0f, 100.0f, 100.0f, 1.0f };

GLfloat light1Diffuse[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat light1Specular[] = { 0.0f, 1.0f, 0.0f ,0.0f };
GLfloat light1Position[] = { 100.0f, 000.0f, 100.0f, 1.0f };

GLfloat light2Diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light2Specular[] = { 0.0f, 0.0f, 1.0f ,0.0f };
GLfloat light2Position[] = { 100.0f, 100.0f, 000.0f, 1.0f };

GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess = 50.0f;

 
//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File can not be Created\nExitting...."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	fprintf(gpFile, "Log File is successfully opened. \n");

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipleline:: Orthographic Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}

			//display
			display();
			update();
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x51: // Q or q
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;

		case 0x46: //for 'f' or 'F'
			bIsFKeyPressed = true;
			bIsVKeyPressed = false;
			break;
		case 0x56: //V or v
			bIsFKeyPressed = false;
			bIsVKeyPressed = true;
			break;
		case 0x4C://L or l
			gbLight = !bIsLKeyPressed;
			bIsLKeyPressed = !bIsLKeyPressed;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();
	int LoadGLTextures(GLuint *, TCHAR[]);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	/*** VERTEX SHADER ***/
	//create shader
	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCodePerVertex =
		"#version 440 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_lighting_enabled;"			\
		"uniform vec3 u_La[3];"						\
		"uniform vec3 u_Ld[3];"						\
		"uniform vec3 u_Ls[3];"						\
		"uniform vec4 u_light_position[3];"			\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"out vec3 phong_ads_color;"					\
		"void main(void)"							\
		"{"											\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"					\
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"	\
		"int i = 0;"																			\
		"vec3 light_direction;"																	\
		"float tn_dot_ld;"																		\
		"vec3 ambient;"																			\
		"vec3 diffuse;"																			\
		"vec3 reflection_vector;"																\
		"vec3 viewer_vector;"																	\
		"vec3 specular;"																		\
		"vec3 phong_ads[3];"																	\
		"for(i = 0; i < 3; i++)"																\
		"{"																						\
		"light_direction = normalize(vec3(u_light_position[i]) - eye_coordinates.xyz);"			\
		"tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);"						\
		"ambient = u_La[i] * u_Ka;"																\
		"diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"													\
		"reflection_vector = reflect(-light_direction, transformed_normals);"					\
		"viewer_vector = normalize(-eye_coordinates.xyz);"										\
		"specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"\
		"phong_ads[i] = ambient + diffuse + specular;"											\
		"}"																						\
		"phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];"							\
		"}"																						\
		"else"																					\
		"{"																						\
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"												\
		"}"																						\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"		\
		"}";

	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar **)&vertexShaderSourceCodePerVertex, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectPerVertex);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Per Vertex - Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Per Vertex - Vertex shader compilation SUCCESS: %s\n", szInfoLog);
	}

	/*** FRAGMENT SHADER***/
	//create shader
	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar *fragmentShaderSourceCodePerVertex =
		"#version 440 core"							\
		"\n"										\
		"in vec3 phong_ads_color;"					\
		"out vec4 FragColor;"						\
		"void main(void)"							\
		"{"											\
		"FragColor = vec4(phong_ads_color, 1.0);"	\
		"}";

	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObjectPerVertex);
	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Per Vertex -  Fragment Shader Compilation Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Per Vertex - Frag shader compilation SUCCESS: %s\n", szInfoLog);
	}

	/**** SHADER PROGRAM ****/
	//create
	gShaderProgramObjectPerVertex = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	glBindAttribLocation(gShaderProgramObjectPerVertex, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectPerVertex, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObjectPerVertex);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Per Vertex - Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Per Vertex - Shader Program Link SUCCESS: %s\n", szInfoLog);
	}

	model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_lighting_enabled");
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_vertex_enabled");
	F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_fragment_enabled");

	La0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[0]");
	Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[0]");
	Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[0]");

	light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[0]");

	La1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[1]");
	Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[1]");
	Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[1]");

	light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[1]");

	La2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[2]");
	Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[2]");
	Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[2]");

	light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[2]");

	Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_material_shininess");

	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCodePerFragment =
		"#version 440 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_lighting_enabled;"			\
		"uniform vec4 u_light_position[3];"			\
		"out vec3 transformed_normals;"				\
		"out vec3 light_direction[3];"				\
		"out vec3 viewer_vector;"					\
		"void main(void)"							\
		"{"											\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"					\
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"					\
		"int i;"																				\
		"for(i = 0;i<3;i++)"																	\
		"{"																						\
		"light_direction[i] = u_light_position[i].xyz - eye_coordinates.xyz;"					\
		"}"																						\
		"viewer_vector = -eye_coordinates.xyz;"													\
		"}"																						\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"		\
		"}";

	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertexShaderSourceCodePerFragment, NULL);

	//compile shader
	glCompileShader(gVertexShaderObjectPerFragment);
	 iInfoLogLength = 0;
	iShaderCompiledStatus = 99;
	szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Per Frag - Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Per Frag - Vertex shader compilation SUCCESS: %s\n", szInfoLog);
	}

	/*** FRAGMENT SHADER***/
	//create shader
	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar *fragmentShaderSourceCodePerFragment =
		"#version 440 core"							\
		"\n"										\
		"in vec3 phong_ads_color;"					\
		"in vec3 transformed_normals;"				\
		"in vec3 light_direction[3];"				\
		"in vec3 viewer_vector;"					\
		"out vec4 FragColor;"						\
		"uniform int u_lighting_enabled;"			\
		"uniform vec3 u_La[3];"						\
		"uniform vec3 u_Ld[3];"						\
		"uniform vec3 u_Ls[3];"						\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"void main(void)"												\
		"{"																\
		"if(u_lighting_enabled == 1)"									\
		"{"																\
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"			\
		"vec3 normalized_light_direction[3];"											\
		"int i;"																		\
		"vec3 ambient;"													\
		"float tn_dot_ld;"		\
		"vec3 diffuse;"									\
		"vec3 reflection_vector;"														\
		"vec3 specular;"																\
		"vec3 phong_ads[3];"															\
		"for(i=0;i<3;i++)"																\
		"{"																				\
		"normalized_light_direction[i] = normalize(light_direction[i]);"				\
		"}"																				\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"						\
		"for(i=0;i<3;i++)"																\
		"{"																				\
		"ambient = u_La[i] * u_Ka;"														\
		"tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction[i]), 0.0);"		\
		"vec3 diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"									\
		"vec3 reflection_vector = reflect(-normalized_light_direction[i], normalized_transformed_normals);"	\
		"vec3 specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
		"phong_ads[i] = ambient + diffuse + specular;"									\
		"}"																				\
		"vec3 phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];"			\
		"FragColor = vec4(phong_ads_color, 1.0);"										\
		"}"																				\
		"else"																			\
		"{"																				\
		"FragColor = vec4(1.0,1.0,1.0,1.0);"											\
		"}"																				\
		"}";

	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObjectPerFragment);
	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Per Frag -  Fragment Shader Compilation Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Per Frag -  Fragment Shader compilation SUCCESS: %s\n", szInfoLog);
	}

	/**** SHADER PROGRAM ****/
	//create
	gShaderProgramObjectPerFragment = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	glBindAttribLocation(gShaderProgramObjectPerFragment, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectPerFragment, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObjectPerFragment);
	iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Per Frag - Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}
	else
	{
		fprintf(gpFile, "Per Frag - Shader Program Link SUCCESS: %s\n", szInfoLog);
	}

	model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lighting_enabled");
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_vertex_enabled");
	F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_fragment_enabled");

	La0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[0]");
	Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[0]");
	Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[0]");

	light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[0]");

	La1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[1]");
	Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[1]");
	Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[1]");

	light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[1]");

	La2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[2]");
	Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[2]");
	Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[2]");

	light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[2]");

	Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_material_shininess");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &gVao_sphere);

	glBindVertexArray(gVao_sphere);

		glGenBuffers(1, &gVbo_sphere_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
			glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);


		glGenBuffers(1, &gVbo_sphere_normal);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
			glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &gVbo_sphere_element);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//clear color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//shade model
	glShadeModel(GL_SMOOTH);
	//depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);
	
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	gPerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height), 0.1f, 100.0f);
}

void update()
{
	gAngle += 0.01f;
	if (gAngle >= 2*3.145)
	{
		gAngle = 0.0f;
	}
	light0Position[1] = 100.0f * cos(gAngle);
	light0Position[2] = 100.0f * sin(gAngle);

	light1Position[0] = 100.0f * cos(gAngle);
	light1Position[2] = 100.0f * sin(gAngle);

	light2Position[0] = 100.0f * cos(gAngle);
	light2Position[1] = 100.0f * sin(gAngle);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//start using opengl program object
	if (gbLight == true)
	{
		if (bIsFKeyPressed)
		{
			glUseProgram(gShaderProgramObjectPerFragment);
		}
		else
		{
			glUseProgram(gShaderProgramObjectPerVertex);
		}

		glUniform1i(L_KeyPressed_uniform, 1);

		glUniform3fv(La0_uniform, 1, lightAmbient);
		glUniform3fv(Ld0_uniform, 1, light0Diffuse);
		glUniform3fv(Ls0_uniform, 1, light0Specular);
		glUniform4fv(light0_position_uniform, 1, light0Position);

		glUniform3fv(La1_uniform, 1, lightAmbient);
		glUniform3fv(Ld1_uniform, 1, light1Diffuse);
		glUniform3fv(Ls1_uniform, 1, light1Specular);
		glUniform4fv(light1_position_uniform, 1, light1Position);

		glUniform3fv(La2_uniform, 1, lightAmbient);
		glUniform3fv(Ld2_uniform, 1, light2Diffuse);
		glUniform3fv(Ls2_uniform, 1, light2Specular);
		glUniform4fv(light2_position_uniform, 1, light2Position);

		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(L_KeyPressed_uniform, 0);

	}

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1,GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//stop using opengl program object
	glUseProgram(0);

	//double buffering
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}


	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	glDeleteShader(gVertexShaderObjectPerVertex);
	gVertexShaderObjectPerVertex = 0;

	glDeleteShader(gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;

	glDeleteProgram(gShaderProgramObjectPerVertex);
	gShaderProgramObjectPerVertex = 0;

	glDeleteShader(gVertexShaderObjectPerFragment);
	gVertexShaderObjectPerFragment = 0;

	glDeleteShader(gFragmentShaderObjectPerFragment);
	gFragmentShaderObjectPerFragment = 0;

	glDeleteProgram(gShaderProgramObjectPerFragment);
	gShaderProgramObjectPerFragment = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File is Successfully closed.");
		fclose(gpFile);
		gpFile = NULL;
	}
}
