//common headers
#include <windows.h>
#include <stdio.h>

//OpenGL headers
#include <gl/glew.h>
#include <gl/GL.h>

#include "vmath.h"
#include "Sphere.h"

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 800

// import libraries
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "Sphere.lib")

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gbLight = false;

float gNumVertices = 0;
float gNumElements = 0;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint  giWindowWidth = 0;
GLuint  giWindowHeight = 0;

GLfloat gAngle = 0.0f;
GLfloat gbXPressed = false;
GLfloat gbYPressed = false;
GLfloat gbZPressed = false;

FILE *gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f ,1.0f };
GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f };

GLfloat materialAmbient[24][4] = {
	{ 0.0215f, 0.1745f, 0.0215f, 1.0f },
	{ 0.135f , 0.225f , 0.1575f, 1.0f },
	{ 0.05375f , 0.05f , 0.06625f, 1.0f },
	{ 0.25f , 0.20725f , 0.20725f, 1.0f },
	{ 0.1745f , 0.01175f , 0.01175f, 1.0f },
	{ 0.1f , 0.18725f , 0.1745f, 1.0f },

	{ 0.329412f , 0.223529f , 0.027451f, 1.0f },
	{ 0.2125f , 0.1275f , 0.054f, 1.0f },
	{ 0.25f , 0.25f , 0.25f, 1.0f },
	{ 0.19125f , 0.0735f , 0.0225f, 1.0f },
	{ 0.24725f , 0.1995f , 0.0745f, 1.0f },
	{ 0.19225f , 0.19225f , 0.19225f, 1.0f },

	{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.1f , 0.06f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },

	{ 0.02f , 0.02f , 0.02f, 1.0f },
	{ 0.0f , 0.05f , 0.05f, 1.0f },
	{ 0.0f , 0.05f , 0.0f, 1.0f },
	{ 0.05f , 0.0f , 0.0f, 1.0f },
	{ 0.05f , 0.05f , 0.05f, 1.0f },
	{ 0.05f , 0.05f , 0.0f, 1.0f }
};

GLfloat materialDiffuse[24][4] = {
	{ 0.07568f, 0.61424f, 0.07568f, 1.0f },
	{ 0.54f , 0.89f , 0.63f , 1.0f },
	{ 0.18275f , 0.17f , 0.22525f , 1.0f },
	{ 1.0f , 0.829f , 0.829f , 1.0f },
	{ 0.61424f , 0.04136f , 0.04136f , 1.0f },
	{ 0.396f , 0.74151f , 0.69102f , 1.0f },

	{ 0.780392f , 0.568627f , 0.113725f , 1.0f },
	{ 0.714f , 0.4284f , 0.18144f , 1.0f },
	{ 0.4f , 0.4f , 0.4f , 1.0f },
	{ 0.7038f , 0.27048f , 0.0828f , 1.0f },
	{ 0.75164f , 0.60648f , 0.22648f , 1.0f },
	{ 0.50754f , 0.50754f , 0.50754f , 1.0f },

	{ 0.01f , 0.01f , 0.01f , 1.0f },
	{ 0.0f , 0.0520980392f ,  0.50980392f , 1.0f },
	{ 0.1f , 0.35f , 0.2f , 1.0f },
	{ 0.5f , 0.0f , 0.0f , 1.0f },
	{ 0.55f , 0.55f , 0.55f , 1.0f },
	{ 0.5f , 0.5f , 0.0f , 1.0f },

	{ 0.01f , 0.01f , 0.01f , 1.0f },
	{ 0.4f , 0.5f , 0.5f , 1.0f },
	{ 0.4f , 0.5f , 0.4f , 1.0f },
	{ 0.5f , 0.4f , 0.4f , 1.0f },
	{ 0.5f , 0.5f , 0.5f , 1.0f },
	{ 0.5f , 0.5f , 0.4f , 1.0f }

};

GLfloat materialSpecular[24][4] = {
	{ 0.633f , 0.727811f , 0.633f , 1.0f },
	{ 0.316228f , 0.316228f , 0.316228f , 1.0f },
	{ 0.332741f , 0.328634f , 0.346435f , 1.0f },
	{ 0.296648f , 0.296648f , 0.296648f , 1.0f },
	{ 0.727811f , 0.626959f , 0.626959f , 1.0f },
	{ 0.297254f , 0.30829f , 0.306678f , 1.0f },

	{ 0.992157f , 0.941176f , 0.807843f , 1.0f },
	{ 0.393548f , 0.271906f , 0.166721f , 1.0f },
	{ 0.774597f , 0.774597f , 0.774597f , 1.0f },
	{ 0.256777f , 0.137622f , 0.086014f , 1.0f },
	{ 0.628281f , 0.555802f , 0.366065f , 1.0f },
	{ 0.508273f , 0.508273f , 0.508273f , 1.0f },

	{ 0.5f , 0.5f , 0.5f , 1.0f },
	{ 0.50196078f , 0.50196078f , 0.50196078f , 1.0f },
	{ 0.45f , 0.5f , 0.45f , 1.0f },
	{ 0.7f , 0.6f , 0.6f , 1.0f },
	{ 0.7f , 0.7f , 0.7f , 1.0f },
	{ 0.6f , 0.6f , 0.5f , 1.0f },

	{ 0.4f , 0.4f , 0.4f , 1.0f },
	{ 0.04f , 0.7f , 0.7f , 1.0f },
	{ 0.04f , 0.7f , 0.04f , 1.0f },
	{ 0.7f , 0.04f , 0.04f , 1.0f },
	{ 0.7f , 0.7f , 0.7f , 1.0f },
	{ 0.7f , 0.7f , 0.04f , 1.0f }
};

GLfloat materialShininess[24] = {
	{ 0.6f * 128 },
	{ 0.1f * 128 },
	{ 0.3f * 128 },
	{ 0.088f * 128 },
	{ 0.6f * 128 },
	{ 0.1f * 128 },

	{ 0.21794872f * 128 },
	{ 0.2f * 128 },
	{ 0.6f * 128 },
	{ 0.1f * 128 },
	{ 0.4f * 128 },
	{ 0.4f * 128 },

	{ 0.25f * 128 },
	{ 0.25f * 128 },
	{ 0.25f * 128 },
	{ 0.25f * 128 },
	{ 0.25f * 128 },
	{ 0.25f * 128 },

	{ 0.078125f * 128 },
	{ 0.078125f * 128 },
	{ 0.078125f * 128 },
	{ 0.078125f * 128 },
	{ 0.078125f * 128 },
	{ 0.078125f * 128 }
};

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File can not be Created\nExitting...."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	fprintf(gpFile, "Log File is successfully opened. \n");

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipleline:: Orthographic Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}

			//display
			display();
			update();
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C://L or l
			gbLight = !bIsLKeyPressed;
			bIsLKeyPressed = !bIsLKeyPressed;
			break;
		case 0x58://x or X
			gbXPressed = true;
			gbYPressed = false;
			gbZPressed = false;
			break;
		case 0x59://y or Y
			gbXPressed = false;
			gbYPressed = true;
			gbZPressed = false;
			break;
		case 0x5A://z or Z
			gbXPressed = false;
			gbYPressed = false;
			gbZPressed = true;
			break;

		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();
	int LoadGLTextures(GLuint *, TCHAR[]);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	/*** VERTEX SHADER ***/
	//create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 440 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_lighting_enabled;"			\
		"uniform vec4 u_light_position;"			\
		"out vec3 transformed_normals;"				\
		"out vec3 light_direction;"					\
		"out vec3 viewer_vector;"					\
		"void main(void)"							\
		"{"											\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"				\
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"				\
		"light_direction = vec3(u_light_position - eye_coordinates.xyz);"					\
		"viewer_vector = -eye_coordinates.xyz;"												\
		"}"																					\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"	\
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/*** FRAGMENT SHADER***/
	//create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 440 core"							\
		"\n"										\
		"in vec3 transformed_normals;"				\
		"in vec3 light_direction;"					\
		"in vec3 viewer_vector;"					\
		"out vec4 FragColor;"						\
		"uniform vec3 u_La;"						\
		"uniform vec3 u_Ld;"						\
		"uniform vec3 u_Ls;"						\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"uniform int u_lighting_enabled;"			\
		"void main(void)"							\
		"{"											\
		"vec3 phong_ads_color;"						\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"					\
		"vec3 normalize_light_direction = normalize(light_direction);"							\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"								\
		"vec3 ambient = u_La * u_Ka;"															\
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalize_light_direction), 0.0);"				\
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"												\
		"vec3 reflection_vector = reflect(-normalize_light_direction, normalized_transformed_normals);"				\
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
		"phong_ads_color = ambient + diffuse + specular;"										\
		"}"																						\
		"else"																					\
		"{"																						\
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"												\
		"}"																						\
		"FragColor = vec4(phong_ads_color, 1.0);"												\
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/**** SHADER PROGRAM ****/
	//create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");

	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &gVao_sphere);

	glBindVertexArray(gVao_sphere);

		glGenBuffers(1, &gVbo_sphere_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
			glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);


		glGenBuffers(1, &gVbo_sphere_normal);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
			glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &gVbo_sphere_element);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//clear color
	glClearColor(0.05f, 0.05f, 0.05f, 0.0f);

	//shade model
	glShadeModel(GL_SMOOTH);
	//depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);
	
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	giWindowWidth = width;
	giWindowHeight = height;

	gPerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height), 0.1f, 100.0f);
}

void update()
{
	gAngle += 0.001f;
	if (gAngle >= 2*3.145)
	{
		gAngle = 0.0f;
	}
	
	if (gbXPressed)
	{
		lightPosition[1] = 100.0f * cos(gAngle);
		lightPosition[2] = 100.0f * sin(gAngle);
		lightPosition[0] = 0.0f;
	}
	else if (gbYPressed)
	{
		lightPosition[0] = 100.0f * cos(gAngle);
		lightPosition[2] = 100.0f * sin(gAngle);
		lightPosition[1] = 0.0f;
	}
	else if (gbZPressed)
	{
		lightPosition[0] = 100.0f * cos(gAngle);
		lightPosition[1] = 100.0f * sin(gAngle);
		lightPosition[2] = 0.0f;
	}

}

void display(void)
{
	int i = 0;
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//start using opengl program object
	glUseProgram(gShaderProgramObject);


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 scaleMatrix = mat4::identity();

	int viewPortWidth = giWindowWidth/4;
	int viewPortHeight = giWindowHeight/6;
	int viewPortX = 0;
	int viewPortY =0;

	for (i = 1; i < 25; i++)
	{
		glViewport(viewPortX, viewPortY, (GLsizei)viewPortWidth, (GLsizei)viewPortHeight);
		if (gbLight == true)
		{
			glUniform1i(L_KeyPressed_uniform, 1);

			glUniform3fv(La_uniform, 1, lightAmbient);
			glUniform3fv(Ld_uniform, 1, lightDiffuse);
			glUniform3fv(Ls_uniform, 1, lightSpecular);
			glUniform4fv(light_position_uniform, 1, lightPosition);

			glUniform3fv(Ka_uniform, 1, materialAmbient[i-1]);
			glUniform3fv(Kd_uniform, 1, materialDiffuse[i-1]);
			glUniform3fv(Ks_uniform, 1, materialSpecular[i-1]);
			glUniform1f(material_shininess_uniform, materialShininess[i-1]);
		}
		else
		{
			glUniform1i(L_KeyPressed_uniform, 0);
		}

		modelMatrix = mat4::identity();
		viewMatrix = mat4::identity();
		scaleMatrix = mat4::identity();

		modelMatrix = translate(0.0f, 0.0f, -2.0f);
	
		scaleMatrix = scale((float)viewPortHeight/(float)viewPortWidth * ((float)giWindowWidth/(float)giWindowHeight), 1.0f, 1.0f);

		modelMatrix = scaleMatrix * modelMatrix;

		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		glBindVertexArray(gVao_sphere);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		glBindVertexArray(0);

		viewPortY += viewPortHeight;

		if (i % 6 == 0)
		{
			//x += length;
			//y = -length * 2.5;
			viewPortX += viewPortWidth;
			viewPortY = 0;
		}

	}


	//stop using opengl program object
	glUseProgram(0);

	//double buffering
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}


	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File is Successfully closed.");
		fclose(gpFile);
		gpFile = NULL;
	}
}
