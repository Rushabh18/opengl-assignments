//common headers
#include <windows.h>
#include <stdio.h>

//OpenGL headers
#include <gl/glew.h>
#include <gl/GL.h>

#include "vmath.h"
#include "Sphere.h"

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 800

// import libraries
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "Sphere.lib")

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gbLight = false;

bool bIsFKeyPressed = false;
bool bIsVKeyPressed = true;

float gNumVertices = 0;
float gNumElements = 0;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLfloat gAngle = 0.0f;

FILE *gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;
GLuint V_KeyPressed_uniform;
GLuint F_KeyPressed_uniform;

GLuint La0_uniform;
GLuint Ld0_uniform;
GLuint Ls0_uniform;
GLuint light0_position_uniform;

GLuint La1_uniform;
GLuint Ld1_uniform;
GLuint Ls1_uniform;
GLuint light1_position_uniform;

GLuint La2_uniform;
GLuint Ld2_uniform;
GLuint Ls2_uniform;
GLuint light2_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light0Diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0Specular[] = { 1.0f, 0.0f, 0.0f ,0.0f };
GLfloat light0Position[] = { 000.0f, 100.0f, 100.0f, 1.0f };

GLfloat light1Diffuse[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat light1Specular[] = { 0.0f, 1.0f, 0.0f ,0.0f };
GLfloat light1Position[] = { 100.0f, 000.0f, 100.0f, 1.0f };

GLfloat light2Diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light2Specular[] = { 0.0f, 0.0f, 1.0f ,0.0f };
GLfloat light2Position[] = { 100.0f, 100.0f, 000.0f, 1.0f };

GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess = 50.0f;

 
//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File can not be Created\nExitting...."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	fprintf(gpFile, "Log File is successfully opened. \n");

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipleline:: Orthographic Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}

			//display
			display();
			update();
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x51: // Q or q
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;

		case 0x46: //for 'f' or 'F'
			bIsFKeyPressed = true;
			bIsVKeyPressed = false;
			break;
		case 0x56: //V or v
			bIsFKeyPressed = false;
			bIsVKeyPressed = true;
			break;
		case 0x4C://L or l
			gbLight = !bIsLKeyPressed;
			bIsLKeyPressed = !bIsLKeyPressed;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();
	int LoadGLTextures(GLuint *, TCHAR[]);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	/*** VERTEX SHADER ***/
	//create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 440 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform int u_lighting_enabled;"			\
		"uniform int u_per_fragment_enabled;"		\
		"uniform int u_per_vertex_enabled;"			\
		"uniform vec3 u_La0;"						\
		"uniform vec3 u_Ld0;"						\
		"uniform vec3 u_Ls0;"						\
		"uniform vec4 u_light0_position;"			\
		"uniform vec3 u_La1;"						\
		"uniform vec3 u_Ld1;"						\
		"uniform vec3 u_Ls1;"						\
		"uniform vec4 u_light1_position;"			\
		"uniform vec3 u_La2;"						\
		"uniform vec3 u_Ld2;"						\
		"uniform vec3 u_Ls2;"						\
		"uniform vec4 u_light2_position;"			\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"out vec3 phong_ads_color;"					\
		"out vec3 transformed_normals;"				\
		"out vec3 light0_direction;"				\
		"out vec3 light1_direction;"				\
		"out vec3 light2_direction;"				\
		"out vec3 viewer_vector;"					\
		"void main(void)"							\
		"{"											\
		"if(u_lighting_enabled == 1)"				\
		"{"											\
		"if(u_per_vertex_enabled == 1)"				\
		"{"											\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"					\
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"	\
		"vec3 light0_direction = normalize(vec3(u_light0_position - eye_coordinates.xyz));"		\
		"float tn_dot_ld0 = max(dot(transformed_normals, light0_direction), 0.0);"				\
		"vec3 ambient0 = u_La0 * u_Ka;"															\
		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;"												\
		"vec3 reflection0_vector = reflect(-light0_direction, transformed_normals);"				\
		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);"									\
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection0_vector, viewer_vector), 0.0), u_material_shininess);"\
		"vec3 phong_ads_color0 = ambient0 + diffuse0 + specular0;"										\
		"vec3 light1_direction = normalize(vec3(u_light1_position - eye_coordinates.xyz));"		\
		"float tn_dot_ld1 = max(dot(transformed_normals, light1_direction), 0.0);"				\
		"vec3 ambient1 = u_La1 * u_Ka;"															\
		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"												\
		"vec3 reflection1_vector = reflect(-light1_direction, transformed_normals);"				\
		"viewer_vector = normalize(-eye_coordinates.xyz);"									\
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection1_vector, viewer_vector), 0.0), u_material_shininess);"\
		"vec3 phong_ads_color1 = ambient1 + diffuse1 + specular1;"										\
		"vec3 light2_direction = normalize(vec3(u_light2_position - eye_coordinates.xyz));"		\
		"float tn_dot_ld2 = max(dot(transformed_normals, light2_direction), 0.0);"				\
		"vec3 ambient2 = u_La2 * u_Ka;"															\
		"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;"												\
		"vec3 reflection2_vector = reflect(-light2_direction, transformed_normals);"				\
		"viewer_vector = normalize(-eye_coordinates.xyz);"									\
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection2_vector, viewer_vector), 0.0), u_material_shininess);"\
		"vec3 phong_ads_color2 = ambient2 + diffuse2 + specular2;"										\
		"phong_ads_color = phong_ads_color0 + phong_ads_color1 + phong_ads_color2;"
		"}"																						\
		"else"																					\
		"{"																						\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"					\
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"	\
		"light0_direction = vec3(u_light0_position - eye_coordinates.xyz);"		\
		"light1_direction = vec3(u_light1_position - eye_coordinates.xyz);"		\
		"light2_direction = vec3(u_light2_position - eye_coordinates.xyz);"		\
		"viewer_vector = -eye_coordinates.xyz;"									\
		"}"																						\
		"}"\
		"else"																					\
		"{"																						\
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"												\
		"}"																						\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"		\
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/*** FRAGMENT SHADER***/
	//create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 440 core"							\
		"\n"										\
		"in vec3 phong_ads_color;"					\
		"in vec3 transformed_normals;"				\
		"in vec3 light0_direction;"					\
		"in vec3 light1_direction;"					\
		"in vec3 light2_direction;"					\
		"in vec3 viewer_vector;"					\
		"out vec4 FragColor;"						\
		"uniform int u_lighting_enabled;"			\
		"uniform int u_per_fragment_enabled;"		\
		"uniform int u_per_vertex_enabled;"			\
		"uniform vec3 u_La0;"						\
		"uniform vec3 u_Ld0;"						\
		"uniform vec3 u_Ls0;"						\
		"uniform vec3 u_La1;"						\
		"uniform vec3 u_Ld1;"						\
		"uniform vec3 u_Ls1;"						\
		"uniform vec3 u_La2;"						\
		"uniform vec3 u_Ld2;"						\
		"uniform vec3 u_Ls2;"						\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"void main(void)"												\
		"{"																\
		"if(u_lighting_enabled == 1)"									\
		"{"																\
		"if(u_per_fragment_enabled == 1)"								\
		"{"																				\
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"			\
		"vec3 normalized_light0_direction = normalize(light0_direction);"				\
		"vec3 normalized_light1_direction = normalize(light1_direction);"				\
		"vec3 normalized_light2_direction = normalize(light2_direction);"				\
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"						\
		"vec3 ambient0 = u_La0 * u_Ka;"													\
		"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction), 0.0);"		\
		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;"									\
		"vec3 reflection0_vector = reflect(-normalized_light0_direction, normalized_transformed_normals);"	\
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection0_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
		"vec3 phong_ads_color0 = ambient0 + diffuse0 + specular0;"						\
		"vec3 ambient1 = u_La1 * u_Ka;"													\
		"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction), 0.0);"		\
		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"									\
		"vec3 reflection1_vector = reflect(-normalized_light1_direction, normalized_transformed_normals);"	\
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection1_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
		"vec3 phong_ads_color1 = ambient1 + diffuse1 + specular1;"						\
		"vec3 ambient2 = u_La2 * u_Ka;"													\
		"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light2_direction), 0.0);"		\
		"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;"									\
		"vec3 reflection2_vector = reflect(-normalized_light2_direction, normalized_transformed_normals);"	\
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection2_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
		"vec3 phong_ads_color2 = ambient2 + diffuse2 + specular2;"						\
		"vec3 phong_ads_color = phong_ads_color0 + phong_ads_color1 + phong_ads_color2;"\
		"FragColor = vec4(phong_ads_color, 1.0);"										\
		"}"																				\
		"else"																			\
		"{"																				\
		"FragColor = vec4(phong_ads_color, 1.0);"										\
		"}"																				\
		"}"																				\
		"else"																			\
		"{"																				\
		"FragColor = vec4(1.0,1.0,1.0,1.0);"											\
		"}"																				\
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/**** SHADER PROGRAM ****/
	//create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_per_vertex_enabled");
	F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_per_fragment_enabled");

	La0_uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
	Ld0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
	Ls0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");

	light0_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");

	La1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
	Ld1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	Ls1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");

	light1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light1_position");

	La2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");
	Ld2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
	Ls2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");

	light2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light2_position");

	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &gVao_sphere);

	glBindVertexArray(gVao_sphere);

		glGenBuffers(1, &gVbo_sphere_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
			glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);


		glGenBuffers(1, &gVbo_sphere_normal);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
			glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &gVbo_sphere_element);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//clear color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//shade model
	glShadeModel(GL_SMOOTH);
	//depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);
	
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	gPerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height), 0.1f, 100.0f);
}

void update()
{
	gAngle += 0.001f;
	if (gAngle >= 2*3.145)
	{
		gAngle = 0.0f;
	}
	light0Position[1] = 100.0f * cos(gAngle);
	light0Position[2] = 100.0f * sin(gAngle);

	light1Position[0] = 100.0f * cos(gAngle);
	light1Position[2] = 100.0f * sin(gAngle);

	light2Position[0] = 100.0f * cos(gAngle);
	light2Position[1] = 100.0f * sin(gAngle);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//start using opengl program object
	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);
		if(bIsFKeyPressed)
			glUniform1i(F_KeyPressed_uniform, 1);
		else
			glUniform1i(F_KeyPressed_uniform, 0);
		if(bIsVKeyPressed)
			glUniform1i(V_KeyPressed_uniform, 1);
		else
			glUniform1i(V_KeyPressed_uniform, 0);

		glUniform3fv(La0_uniform, 1, lightAmbient);
		glUniform3fv(Ld0_uniform, 1, light0Diffuse);
		glUniform3fv(Ls0_uniform, 1, light0Specular);
		glUniform4fv(light0_position_uniform, 1, light0Position);

		glUniform3fv(La1_uniform, 1, lightAmbient);
		glUniform3fv(Ld1_uniform, 1, light1Diffuse);
		glUniform3fv(Ls1_uniform, 1, light1Specular);
		glUniform4fv(light1_position_uniform, 1, light1Position);

		glUniform3fv(La2_uniform, 1, lightAmbient);
		glUniform3fv(Ld2_uniform, 1, light2Diffuse);
		glUniform3fv(Ls2_uniform, 1, light2Specular);
		glUniform4fv(light2_position_uniform, 1, light2Position);

		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(L_KeyPressed_uniform, 0);

	}

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1,GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_sphere);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//stop using opengl program object
	glUseProgram(0);

	//double buffering
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}


	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File is Successfully closed.");
		fclose(gpFile);
		gpFile = NULL;
	}
}
