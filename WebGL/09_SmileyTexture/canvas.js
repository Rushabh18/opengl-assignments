//onload function
var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	RGP_ATTRIBUTE_VERTEX:0,
	RGP_ATTRIBUTE_COLOR:1,
	RGP_ATTRIBUTE_NORMAL:2,
	RGP_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_quad;
var vbo_quad_position;
var vbo_quad_texture;
var texture_smiley = 0;

var mvpUniform;
var texture0_sampler_uniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame || 
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;
	
var cancelAnimationFrame = 
	window.cancelAnimationFrame ||
	window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
	window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
	window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
	window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

	
function main()
{
	//get <canvas> element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed :( \n");
	else 
		console.log("Obtaining Canvas Passes :) \n");
	
	//print canvas width and height on console
	console.log("Canvas width::" +canvas.width+ " and Canvas height::" +canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initailize webGL
	init();
	
	resize();
	draw();
}

function toggleFullScreen()
{
	var fullscreen_element = 
		document.fullscreenElement || 
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document. msFullscreenElement||
		null;
		
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		
		bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		
		bFullScreen = false;
	}
}

function init()
{
	//code 
	//get webGL 2.0 contextgl 
	gl = canvas.getContext("webgl2");
	if(gl == null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	
	var vertexShaderSourceCode = 
	"#version 300 es"							+
	"\n"										+
	"in vec4 vPosition;"						+
	"in vec2 vTexture0_Coord;"					+
	"uniform mat4 u_mvp_uniform;"				+
	"out vec2 out_texture0_coord;"				+
	"void main(void)"							+
	"{"											+
	"gl_Position = u_mvp_uniform * vPosition;"	+
	"out_texture0_coord = vTexture0_Coord;"		+
	"}";
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject)
		if(error.length > 0)
		{
			alert("vertex shader");
			alert(error);
			uninitialize();
		}
	}

	var fragmentShaderSourceCode = 
	"#version 300 es"			+
	"\n"						+
	"precision highp float;"	+
	"in vec2 out_texture0_coord;"		+
	"uniform highp sampler2D u_texture0_sampler;" +
	"out vec4 FragColor;"		+
	"void main(void)"			+
	"{"							+
	"FragColor = texture(u_texture0_sampler, out_texture0_coord);"	+
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject)
		if(error.length > 0)
		{
			alert("fragment shader");
			alert(error);
			uninitialize();
		}
	}

	//shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.RGP_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.RGP_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

    mvpUniform =gl.getUniformLocation(shaderProgramObject,"u_mvp_uniform");
    texture0_sampler_uniform =gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");
	
    texture_smiley = gl.createTexture();
    texture_smiley.image = new Image();
    texture_smiley.image.src="smiley.png";
    texture_smiley.image.onload = function ()
    {
        gl.bindTexture(gl.TEXTURE_2D, texture_smiley);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_smiley.image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.bindTexture(gl.TEXTURE_2D, null);
    }
										
	var quadVertices = new Float32Array([
											1.0, 1.0, 0.0, 
											1.0, -1.0, 0.0,
											-1.0, -1.0, 0.0,
											-1.0, 1.0, 0.0,
										]);

	var quadTextures = new Float32Array([
											0.0, 0.0, 
											0.0, 1.0,
											1.0, 1.0, 
											1.0, 0.0,
										]);
											
	vao_quad = gl.createVertexArray();
	gl.bindVertexArray(vao_quad);
		vbo_quad_position = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_quad_position);
			gl.bufferData(gl.ARRAY_BUFFER, quadVertices, gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.RGP_ATTRIBUTE_VERTEX,
									3,
									gl.FLOAT,
									false, 0, 0);
			gl.enableVertexAttribArray(WebGLMacros.RGP_ATTRIBUTE_VERTEX);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		vbo_quad_texture = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vbo_quad_texture);
			gl.bufferData(gl.ARRAY_BUFFER, quadTextures, gl.STATIC_DRAW);
			gl.vertexAttribPointer(WebGLMacros.RGP_ATTRIBUTE_TEXTURE0,
									2,
									gl.FLOAT,
									false, 0, 0);
			gl.enableVertexAttribArray(WebGLMacros.RGP_ATTRIBUTE_TEXTURE0);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);
	
	gl.enable(gl.DEPTH_TEST);
//	gl.enable(gl.CULL_FACE);
	
	gl.clearColor(0.0, 0.0, 0.0, 1.0);
	
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//code 
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0, perspectiveProjectionMatrix);
}

function draw()
{	
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	
	
	gl.bindVertexArray(vao_quad);
	
		gl.activeTexture(gl.TEXTURE0);
		
		gl.bindTexture(gl.TEXTURE_2D, texture_smiley);
		gl.uniform1i(texture0_sampler_uniform, 0);
		
		gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	
	gl.bindVertexArray(null);
	
	gl.useProgram(null);

	requestAnimationFrame(draw, canvas);
}

function uninitialize()
{
	if(vao_quad)
	{
		gl.deleteVertexArray(vao_quad);
		vao_quad = null;
	}

	if(vbo_quad_position)
	{
		gl.deleteBuffer(vbo_quad_position);
		vbo_quad_position = null;
	}
		
	if(vbo_quad_texture)
	{
		gl.deleteBuffer(vbo_quad_texture);
		vbo_quad_texture = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
			uninitialize();
			window.close();
			break;
		case 70:
			toggleFullScreen();
			break;	

	}
}

function mouseDown()
{
	//code 
}