TEXTURE ASSIGNMENT::

ISSUE:: The complete quad was filled with just one color of texture. 
REASON:: Typo in variable name used in shader and the one defined in gl.BindAttribLocation.

ISSUE:: drawArrays: Vertex fetch requires vertex #3, but attribs only supply 0.
REASON:: Instead of using Float32Array used just Array in draw function due to which it may not be getting the right color value i.e. in this case texture value and hence reporting error.

ISSUE:: No shader compiler error or shader linker error still black screen.
TIP:: Check the name of all the uniform and attributes you are defining in " " 

ISSUE:: Was getting the wrong colors of sphere in materials.
REASON:: Accidently replaced a value of material_diffuse during Find and Replace. 
TIP:: Use find and replace carefully.

ISSUE:: Error while using uniform in 3 lights using two shaders.
REASON:: the mapping of the uniforms to both the shader object was done in init but to only one shader objects the uniform was mapped after init. So when the other shaderobjects were used the mapping of the uniform was missing. 
TIP:: Always check whether the uniforms are correctly mapped before using the shader object.
	