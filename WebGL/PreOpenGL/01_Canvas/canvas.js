//onload function
function main()
{
	//get <canvas> element
	var canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed :( \n");
	else 
		console.log("Obtaining Canvas Passes :) \n");
	
	//print canvas width and height on console
	console.log("Canvas width::" +canvas.width+ " and Canvas height::" +canvas.height);
	
	//get 2d context
	var context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2d Context Failed :( \n");
	else
		console.log("Obtaining 2d Context Passed :) \n");
	
	//fill canvas with black color
	context.fillStyle = "black";
	context.fillRect(0, 0, canvas.width, canvas.height);
	
	//center the text
	context.textAlign = "center"; //center horizontally
	context.textBaseline = "middle"; //center vertically
	
	//text
	var str = "Hello World !! :)";
	
	//text font
	context.font = "48px sans-serif";
	
	//text color
	context.fillStyle = "white";
	
	//display the text in center
	context.fillText(str, canvas.width/2, canvas.height/2);
}