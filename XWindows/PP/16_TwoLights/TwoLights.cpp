#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <SOIL/SOIL.h> 

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>

#include "vmath.h"
#include "Sphere.h"

//namespace
using namespace std;
using namespace vmath;

//global variable decalarations
bool gbFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXContext gGLXContext;
GLXFBConfig gGLXFBConfig;

int giWindowWidth = 800;
int giWindowHeight = 600;

FILE *gpFile = NULL;

GLfloat gAngle = 0.0f;

enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0
};

GLuint gVertexShaderObject = 0;
GLuint gFragmentShaderObject = 0;
GLuint gShaderProgramObject = 0;

GLuint gVao_pyramid;
GLuint gVbo_pyramid_position;
GLuint gVbo_pyramid_normal;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint La_1_uniform;
GLuint Ld_1_uniform;
GLuint Ls_1_uniform;
GLuint light_1_position_uniform;

GLuint La_2_uniform;
GLuint Ld_2_uniform;
GLuint Ls_2_uniform;
GLuint light_2_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

GLfloat light_1_Ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_1_Diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light_1_Specular[] = { 1.0f, 0.0f, 0.0f ,0.0f };
GLfloat light_1_Position[] = { 100.0f, 100.0f, 100.0f, 1.0f };

GLfloat light_2_Ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat light_2_Diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light_2_Specular[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light_2_Position[] = { -100.0f, 100.0f, 100.0f, 1.0f };

GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess = 50.0f;

mat4 gPerspectiveProjectionMatrix;

//entry-point function
int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void initialize(void);
	void display(void);
	void resize(int, int);
	void uninitialize();
	void update(void);

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	
	bool bDone = false;
	gpFile = fopen("Log.txt", "w");
	if( gpFile == NULL)
	{
		exit(0);
	}

	fprintf(gpFile, "Log File successfully opened.\n");

	//code
	CreateWindow();

	//initialize 
	initialize();
	
	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(gbFullScreen == false)
							{	
								ToggleFullScreen();
								gbFullScreen = true;
							}
							else
							{
								ToggleFullScreen();
								gbFullScreen = false;
							}
							break;

						default:
			break;						
					}
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);	
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone = true;
					break;
				default:
					break;	
			}
		}

		display();
		update();
	}

	return(0);
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;	
	GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int styleMask = 0;
	int i =0;

	static int frameBufferAttributes[]=
 	{
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_STENCIL_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None	
	};

	//code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR:: Unable to open X Display. \nExitting Now...");
		uninitialize();
		exit(1);
	}

	pGLXFBConfigs=glXChooseFBConfig(gpDisplay,DefaultScreen(gpDisplay),frameBufferAttributes,&iNumFBConfigs);
	if(pGLXFBConfigs==NULL)
	{
		printf( "Failed To Get Valid Framebuffer Config. Exiting Now ...\n");
		uninitialize();
		exit(1);
	}
	printf("%d Matching FB Configs Found.\n",iNumFBConfigs);
	
	int bestFramebufferconfig=-1,worstFramebufferConfig=-1,bestNumberOfSamples=-1,worstNumberOfSamples=999;
	for(i=0;i<iNumFBConfigs;i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffer,samples;
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLE_BUFFERS,&sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfigs[i],GLX_SAMPLES,&samples);
			printf("Matching Framebuffer Config=%d : Visual ID=0x%lu : SAMPLE_BUFFERS=%d : SAMPLES=%d\n",i,pTempXVisualInfo->visualid,sampleBuffer,samples);
			if(bestFramebufferconfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferconfig=i;
				bestNumberOfSamples=samples;
			}
			if( worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig=i;
			    worstNumberOfSamples=samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferconfig];
	// set global GLXFBConfig
	gGLXFBConfig=bestGLXFBConfig;
	
	// be sure to free FBConfig list allocated by glXChooseFBConfig()
	XFree(pGLXFBConfigs);
	
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);
	printf("Chosen Visual ID=0x%lu\n",gpXVisualInfo->visualid );


	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;

	winAttribs.colormap = XCreateColormap(gpDisplay, 
				              RootWindow(gpDisplay, gpXVisualInfo->screen), 
					      gpXVisualInfo->visual, 	
					      AllocNone);		
	
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)
	{
		printf("ERROR:: failed to Create Main Window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "Programmable Pipeline Template");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declarations
	Atom wmState;
	Atom fullScreen;
	XEvent xev = {0};

	//code
	wmState = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wmState;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;

	fullScreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullScreen;

	XSendEvent(gpDisplay,
		   RootWindow(gpDisplay, gpXVisualInfo->screen),
		   False,
		   StructureNotifyMask,
	           &xev);
}

void initialize()
{
	//function prototype
	void resize(int, int);
	void uninitialize();

	//code 
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	
	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,3,
		GLX_CONTEXT_MINOR_VERSION_ARB,1,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0 }; // array must be terminated by 0
		
	gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);

	if(!gGLXContext) // fallback to safe old style 2.x context
	{
		// When a context version below 3.0 is requested, implementations will return 
		// the newest context version compatible with OpenGL versions less than version 3.0.
		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			0 }; // array must be terminated by 0
		printf("Failed To Create GLX 4.5 context. Hence Using Old-Style GLX Context\n");
		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	}
	else // successfully created 4.1 context
	{
		printf("OpenGL Context 4.5 Is Created.\n");
	}
	
	// verifying that context is a direct context
	if(!glXIsDirect(gpDisplay,gGLXContext))
	{
		printf("Indirect GLX Rendering Context Obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering Context Obtained\n" );
	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	GLenum glew_error = glewInit();
	if(glew_error != GLEW_OK)
	{
		uninitialize();
		exit(0);		
	}

	/***** VERTEX SHADER *****/
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode = 
		"#version 430 core"							\
		"\n"										\
		"in vec4 vPosition;"						\
		"in vec3 vNormal;"							\
		"uniform mat4 u_model_matrix;"				\
		"uniform mat4 u_view_matrix;"				\
		"uniform mat4 u_projection_matrix;"			\
		"uniform vec3 u_La_1;"						\
		"uniform vec3 u_Ld_1;"						\
		"uniform vec3 u_Ls_1;"						\
		"uniform vec4 u_light_position_1;"			\
		"uniform vec3 u_La_2;"						\
		"uniform vec3 u_Ld_2;"						\
		"uniform vec3 u_Ls_2;"						\
		"uniform vec4 u_light_position_2;"			\
		"uniform vec3 u_Ka;"						\
		"uniform vec3 u_Kd;"						\
		"uniform vec3 u_Ks;"						\
		"uniform float u_material_shininess;"		\
		"out vec3 phong_ads_color;"					\
		"void main(void)"							\
		"{"											\
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"						\
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"		\
		"vec3 light_direction_1 = normalize(vec3(u_light_position_1) - eye_coordinates.xyz);"		\
		"vec3 light_direction_2 = normalize(vec3(u_light_position_2) - eye_coordinates.xyz);"		\
		"float tn_dot_ld_1 = max(dot(transformed_normals, light_direction_1), 0.0);"				\
		"float tn_dot_ld_2 = max(dot(transformed_normals, light_direction_2), 0.0);"				\
		"vec3 ambient_1 = u_La_1 * u_Ka;"															\
		"vec3 ambient_2 = u_La_2 * u_Ka;"															\
		"vec3 diffuse_1 = u_Ld_1 * u_Kd * tn_dot_ld_1;"												\
		"vec3 diffuse_2 = u_Ld_2 * u_Kd * tn_dot_ld_2;"												\
		"vec3 reflection_vector_1 = reflect(-light_direction_1, transformed_normals);"				\
		"vec3 reflection_vector_2 = reflect(-light_direction_2, transformed_normals);"				\
		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);"										\
		"vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1, viewer_vector), 0.0), u_material_shininess);"\
		"vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2, viewer_vector), 0.0), u_material_shininess);"\
		"vec3 phong_ads_1 = ambient_1 + diffuse_1 + specular_1;"									\
		"vec3 phong_ads_2 = ambient_2 + diffuse_2 + specular_2;"									\
		"phong_ads_color = phong_ads_1 + phong_ads_2;"												\
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"			\
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log :: %s\n",szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/***** FRAGMENT SHADER *****/
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar *fragmentShaderSourceCode = 
		"#version 430 core"												\
		"\n"															\
		"in vec3 phong_ads_color;"										\
		"out vec4 FragColor;"											\
		"void main(void)"												\
		"{"																\
		"FragColor = vec4(phong_ads_color, 1.0);"											\
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if(iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragrment Shader Compilation Log :: %s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/***** SHADER PROGRAM *****/
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject);

	GLint iShaderProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if(iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if(iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if(szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Shader Link Log :: %s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	La_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_1");
	Ld_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_1");
	Ls_1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_1");

	light_1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position_1");

	La_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La_2");
	Ld_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld_2");
	Ls_2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls_2");

	light_2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position_2");


	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	const GLfloat pyramidVertices[] =
	{
		0.0f, 1.0f, 0.0f, //right
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f, //front
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f, //left
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f, //back
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f
	};

	const GLfloat pyramidNormals[] =
	{
		0.894427f, 0.447214f, 0.0f, //right
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, 0.894427f, //front
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		-0.894427f, 0.447214f, 0.0f, //left
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, -0.894427f, //back
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

 
	};
	glGenVertexArrays(1, &gVao_pyramid);

	glBindVertexArray(gVao_pyramid);

		glGenBuffers(1, &gVbo_pyramid_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_position);
			glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);


		glGenBuffers(1, &gVbo_pyramid_normal);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_pyramid_normal);
			glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(giWindowWidth, giWindowHeight);
}

void update()
{
	gAngle += 0.1f;
	if (gAngle >= 360)
	{
		gAngle = 0.0f;
	}
}

void display()
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	
	glUniform3fv(La_1_uniform, 1, light_1_Ambient);
	glUniform3fv(Ld_1_uniform, 1, light_1_Diffuse);
	glUniform3fv(Ls_1_uniform, 1, light_1_Specular);
	glUniform4fv(light_1_position_uniform, 1, light_1_Position);

	glUniform3fv(La_2_uniform, 1, light_2_Ambient);
	glUniform3fv(Ld_2_uniform, 1, light_2_Diffuse);
	glUniform3fv(Ls_2_uniform, 1, light_2_Specular);
	glUniform4fv(light_2_position_uniform, 1, light_2_Position);

	glUniform3fv(Ka_uniform, 1, material_ambient);
	glUniform3fv(Kd_uniform, 1, material_diffuse);
	glUniform3fv(Ks_uniform, 1, material_specular);
	glUniform1f(material_shininess_uniform, material_shininess);

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = rotate(gAngle, 0.0f, 1.0f, 0.0f);
	
	modelMatrix = modelMatrix * rotationMatrix;
	
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1,GL_FALSE, gPerspectiveProjectionMatrix);

	glBindVertexArray(gVao_pyramid);

		glDrawArrays(GL_TRIANGLES, 0, 12);

	glBindVertexArray(0);
	
	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height)
{
	//code
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);

}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}

	if (gVao_pyramid)
	{
		glDeleteVertexArrays(1, &gVao_pyramid);
		gVao_pyramid = 0;
	}

	if (gVbo_pyramid_position)
	{
		glDeleteBuffers(1, &gVbo_pyramid_position);
		gVbo_pyramid_position = 0;
	}


	if (gVbo_pyramid_normal)
	{
		glDeleteBuffers(1, &gVbo_pyramid_normal);
		gVbo_pyramid_normal = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glUseProgram(0);

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	if(gpFile)
	{
		fprintf(gpFile, "Log file successfully closed :) ");
		fclose(gpFile);
	}
}
