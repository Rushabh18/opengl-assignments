#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//namespaces
using namespace std;

//global variable declarations
bool gbFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 600;
int giWindowHeight = 600;

float gfAngleCube = 0.0f;
float gfSpeed = 1.0f;

GLXContext gGLXContext;

//entry-point function 
int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void initialize(void);
	void display(void);
	void resize(int, int, int, int);
	void uninitialize(void);

	//variable declarations
	bool bDone = false;

	//code 
	CreateWindow();

	//initialize
	initialize();
	
	//Game Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(gbFullScreen == false)
							{
								ToggleFullScreen();
								gbFullScreen = true;
							}
							else
							{
								ToggleFullScreen();
								gbFullScreen = false;
						
							}
							break;
						case XK_1:							
							resize(0,0, giWindowWidth / 2 , giWindowHeight);
							break;
						case XK_2:							
							resize(giWindowWidth/2,0, giWindowWidth / 2 , giWindowHeight);
							break;
						case XK_3:							
							resize(0,0, giWindowWidth , giWindowHeight/2);
							break;
						case XK_4:							
							resize(0,giWindowHeight/2, giWindowWidth , giWindowHeight/2);
							break;
						case XK_5:							
							resize(0,0, giWindowWidth / 2 , giWindowHeight/2);
							break;				
						case XK_6:							
							resize(giWindowWidth/2,0, giWindowWidth / 2 , giWindowHeight/2);
							break;
						case XK_7:							
							resize(0,giWindowHeight/2, giWindowWidth / 2 , giWindowHeight/2);
							break;							
						case XK_8:							
							resize(giWindowWidth/2,giWindowHeight/2, giWindowWidth / 2 , giWindowHeight/2);
							break;						
						case XK_9:							
							resize(giWindowWidth/4,giWindowHeight/4, giWindowWidth / 2 , giWindowHeight/2);
							break;
						default:
							break;					
					}
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;

					} 
				case MotionNotify:
					break;
				case ConfigureNotify:
					giWindowWidth = event.xconfigure.width;
					giWindowHeight = event.xconfigure.height;
					resize(0,0, giWindowWidth, giWindowHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone = true;
					break;	
			}
		}	
		display();
	}
	return(0);
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen = 0;
	int defaultDepth = 0;
	int styleMask = 0;

	static int frameBufferAttributes[]=
 	{
		GLX_RGBA,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		None	
	};

	//code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("Error::Unable to Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;
	
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)
	{
		printf("Error:: Failed to Create Window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "View Port");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen(void)
{
	//variable declarations
	Atom wmState;
	Atom fullScreen;
	XEvent xev = {0};

	//code 
	wmState = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wmState;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
	fullScreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullScreen;

	XSendEvent(gpDisplay,
		   RootWindow(gpDisplay, gpXVisualInfo->screen),
		   False, 
	 	    StructureNotifyMask,
		   &xev);
}

void initialize()
{
	//function prototype
	void resize(int ,int, int, int);
	
	//code
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	glClearColor(0.0, 0.0, 0.0, 0.0);

	glClearDepth(1.0f);
	
	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	resize(0,0, giWindowWidth, giWindowHeight);
}

void DrawSquare(float vertice1[3], float vertice2[3], float vertice3[3], float vertice4[3], float color[3])
{
	glColor3f(color[0], color[1], color[2]);
	
	glBegin(GL_QUADS);
		glVertex3f(vertice1[0],vertice1[1], vertice1[2]);
		glVertex3f(vertice2[0],vertice2[1], vertice2[2]);
		glVertex3f(vertice3[0],vertice3[1], vertice3[2]);
		glVertex3f(vertice4[0],vertice4[1], vertice4[2]);
	glEnd();
}

void DrawMultiColorCube()
{
	float cubePoints[8][3] ={
					{1.0f, 1.0f, 1.0f},
					{-1.0f, 1.0f, 1.0f},
					{-1.0f, -1.0f, 1.0f},
					{1.0f, -1.0f, 1.0f},

					{1.0f, 1.0f, -1.0f},
					{-1.0f, 1.0f, -1.0f},
					{-1.0f, -1.0f, -1.0f},
					{1.0f, -1.0f, -1.0f},
				};

	float color[6][3] = {
			    	{1.0f, 0.0f, 0.0f},
			    	{0.0f, 1.0f, 0.0f},
			    	{0.0f, 0.0f, 1.0f},
			    	{1.0f, 1.0f, 0.0f},
			    	{1.0f, 0.0f, 1.0f},
			    	{0.0f, 1.0f, 1.0f},
			    };

	DrawSquare(cubePoints[0], cubePoints[1], cubePoints[2], cubePoints[3], color[0]);
	DrawSquare(cubePoints[4], cubePoints[5], cubePoints[6], cubePoints[7], color[1]);
	DrawSquare(cubePoints[1], cubePoints[5], cubePoints[6], cubePoints[2], color[2]);
	DrawSquare(cubePoints[0], cubePoints[4], cubePoints[7], cubePoints[3], color[3]);
	DrawSquare(cubePoints[0], cubePoints[1], cubePoints[5], cubePoints[4], color[4]);
	DrawSquare(cubePoints[3], cubePoints[2], cubePoints[6], cubePoints[7], color[5]);
		
}
void update()
{
	if(gfAngleCube > 360)
	{
		gfAngleCube = 0.0f;
	}
	gfAngleCube += gfSpeed;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -8.0f);
	glRotatef(gfAngleCube, 1.0f, 1.0f, 1.0f);
	DrawMultiColorCube();
	
	update();
	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int x, int y, int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	if (width == 0)
		width = 1;

	glViewport(x, y, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
		gluPerspective(45.0f, 1.0f*(GLfloat)height / (GLfloat)width, 0.1f, 100.0f);
	else
		gluPerspective(45.0f, 1.0f*(GLfloat)width / (GLfloat)height, 0.1f, 100.0f);}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0 ,0 );
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);	
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

}
