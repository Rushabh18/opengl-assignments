#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//namespaces
using namespace std;

//global variable declarations
bool gbFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 600;
int giWindowHeight = 600;

float gfAnglePyramid = 0.0f;
float gfAngleCube = 0.0f;
float gfSpeed = 1.0f;

FILE *gpFILE = NULL;

GLXContext gGLXContext;

//entry-point function 
int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void initialize(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);

	//variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;

	//code 
	gpFILE = fopen("OpenGLLogFile.txt", "w");
	if(gpFILE == NULL)
	{
		printf("Error:: LogFile not Created.\n Exitting now...");
		exit(1);		
	}	
	CreateWindow();
	fprintf(gpFILE, "\n[Info]: Created Window Successfully");
	//initialize
	initialize();
	fprintf(gpFILE, "\n[Info]: Initialization Successful");
	
	//Game Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							fprintf(gpFILE, "\n[Info]: Escape Pressed");
							bDone = true;
							break;
						case XK_F:
						case XK_f:
							fprintf(gpFILE, "\n[Info]: 'F' key Pressed");

							if(gbFullScreen == false)
							{
								ToggleFullScreen();
								gbFullScreen = true;
								fprintf(gpFILE, "\n[Info]: FullScreen Window Successful");
							}
							else
							{
								ToggleFullScreen();
								gbFullScreen = false;
								fprintf(gpFILE, "\n[Info]: Window Restored to normal Successful");
							}
							break;
						default:
							break;					
					}
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
						default:
							break;

					} 
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					fprintf(gpFILE, "\n[Info]: New Window Size{%d x %d}",winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					fprintf(gpFILE, "\n[Info]: Close Button Pressed");
					bDone = true;
					break;	
			}
		}	
		display();
	}
	return(0);
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen = 0;
	int defaultDepth = 0;
	int styleMask = 0;

	static int frameBufferAttributes[]=
 	{
		GLX_RGBA,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		None	
	};

	//code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		fprintf(gpFILE, "\n[ERROR]: Unable to Open X Display.");
		printf("Error::Unable to Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	fprintf(gpFILE, "\n[INFO]: Open X Display Successful.");

	defaultScreen = XDefaultScreen(gpDisplay);
	fprintf(gpFILE, "\n[INFO]: Default Screen Successful.");

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	fprintf(gpFILE, "\n[INFO]: Visual Info Successful.");

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	fprintf(gpFILE, "\n[INFO]: Colormap Successful.");

	gColormap = winAttribs.colormap;
	
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				0,
				0,
				giWindowWidth,
				giWindowHeight,
				0,
				gpXVisualInfo->depth,
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)
	{
		fprintf(gpFILE, "\n[ERROR]: Failed to Create Window.");
		printf("Error:: Failed to Create Window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	fprintf(gpFILE, "\n[INFO]: Window Creation Successful.");

	XStoreName(gpDisplay, gWindow, "3D Shapes LogFile");
	fprintf(gpFILE, "\n[INFO]: XStoreName Successful.");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	fprintf(gpFILE, "\n[INFO]: XInterAtom Successful.");
	
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	fprintf(gpFILE, "\n[INFO]: XSetWMProtocols Successful.");

	XMapWindow(gpDisplay, gWindow);
	fprintf(gpFILE, "\n[INFO]: XMapWindow Successful.");
}

void ToggleFullScreen(void)
{
	//variable declarations
	Atom wmState;
	Atom fullScreen;
	XEvent xev = {0};

	//code 
	wmState = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));
	fprintf(gpFILE, "\n[INFO]: XInternAtom Successful.");

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wmState;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = gbFullScreen ? 0 : 1;
	fullScreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullScreen;

	XSendEvent(gpDisplay,
		   RootWindow(gpDisplay, gpXVisualInfo->screen),
		   False, 
	 	    StructureNotifyMask,
		   &xev);
	fprintf(gpFILE, "\n[INFO]: XSendEvent Successful.");

}

void initialize()
{
	//function prototype
	void resize(int, int);
	
	//code
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	fprintf(gpFILE, "\n[INFO]: glXCreateContext Successful.");

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	fprintf(gpFILE, "\n[INFO]: glXMakeCurrent Successful.");

	glClearColor(0.0, 0.0, 0.0, 0.0);
	fprintf(gpFILE, "\n[INFO]: glClearColor Successful.");

	glClearDepth(1.0f);
	fprintf(gpFILE, "\n[INFO]: glClearDepth Successful.");
	
	glEnable(GL_DEPTH_TEST);
	fprintf(gpFILE, "\n[INFO]: glEnable Successful.");

	glDepthFunc(GL_LEQUAL);
	fprintf(gpFILE, "\n[INFO]: glDepthFunc Successful.");

	resize(giWindowWidth, giWindowHeight);
	fprintf(gpFILE, "\n[INFO]: resize Successful.");
}

void DrawSquare(float vertice1[3], float vertice2[3], float vertice3[3], float vertice4[3], float color[3])
{
	glColor3f(color[0], color[1], color[2]);
	
	glBegin(GL_QUADS);
		glVertex3f(vertice1[0],vertice1[1], vertice1[2]);
		glVertex3f(vertice2[0],vertice2[1], vertice2[2]);
		glVertex3f(vertice3[0],vertice3[1], vertice3[2]);
		glVertex3f(vertice4[0],vertice4[1], vertice4[2]);
	glEnd();
}

void DrawMultiColorCube()
{
	float cubePoints[8][3] ={
					{1.0f, 1.0f, 1.0f},
					{-1.0f, 1.0f, 1.0f},
					{-1.0f, -1.0f, 1.0f},
					{1.0f, -1.0f, 1.0f},

					{1.0f, 1.0f, -1.0f},
					{-1.0f, 1.0f, -1.0f},
					{-1.0f, -1.0f, -1.0f},
					{1.0f, -1.0f, -1.0f},
				};

	float color[6][3] = {
			    	{1.0f, 0.0f, 0.0f},
			    	{0.0f, 1.0f, 0.0f},
			    	{0.0f, 0.0f, 1.0f},
			    	{1.0f, 1.0f, 0.0f},
			    	{1.0f, 0.0f, 1.0f},
			    	{0.0f, 1.0f, 1.0f},
			    };

	DrawSquare(cubePoints[0], cubePoints[1], cubePoints[2], cubePoints[3], color[0]);
	DrawSquare(cubePoints[4], cubePoints[5], cubePoints[6], cubePoints[7], color[1]);
	DrawSquare(cubePoints[1], cubePoints[5], cubePoints[6], cubePoints[2], color[2]);
	DrawSquare(cubePoints[0], cubePoints[4], cubePoints[7], cubePoints[3], color[3]);
	DrawSquare(cubePoints[0], cubePoints[1], cubePoints[5], cubePoints[4], color[4]);
	DrawSquare(cubePoints[3], cubePoints[2], cubePoints[6], cubePoints[7], color[5]);
		
}
void update()
{
	if(gfAngleCube > 360)
	{
		gfAngleCube = 0.0f;
	}
	gfAngleCube += gfSpeed;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -8.0f);
	glRotatef(gfAngleCube, 1.0f, 1.0f, 1.0f);
	DrawMultiColorCube();
	
	update();
	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height)
{
	//code
	if (height == 0) {
		height = 1;
	}
	if (width == 0) {
		width = 1;
	}

	//Viewport/window of OpenGL	- Parameters x,y,width,height
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	fprintf(gpFILE, "\n[INFO]: glViewport Successful.");

	//Set projection matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
	fprintf(gpFILE, "\n[INFO]: gluPerspective Successful.");

}

void uninitialize()
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0 ,0 );
	}

	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);	
		fprintf(gpFILE, "\n[INFO]: destroyed Context Successfully.");
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
		fprintf(gpFILE, "\n[INFO]: destroyed Window Successfully.");
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
		fprintf(gpFILE, "\n[INFO]: Free Colormap Successfully.");
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
		fprintf(gpFILE, "\n[INFO]: Free VisualInfo Successfully.");
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
		fprintf(gpFILE, "\n[INFO]: Free Display Successfully.");
	}
	fclose(gpFILE);
}
