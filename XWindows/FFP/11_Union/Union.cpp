#include <iostream>
#include <stdio.h>
#include <memory.h>

int main(void)
{
	union dataStructures{
		int iNum;
		float fNum;
		char str[10];
	}ds;

	memset(&ds, 0, 10* sizeof(char));
	printf("\nEnter String::");
	scanf("%s", ds.str);
	printf("\nYou Endtered %s",ds.str);
	
	memset(&ds, 0, sizeof(float));
	printf("\nEnter Float::");
	scanf("%f", &ds.fNum);
	printf("\nYou Endtered %f",ds.fNum);
	
	memset(&ds, 0, sizeof(int));
	printf("\nEnter Integer::");
	scanf("%d", &ds.iNum);
	printf("\nYou Endtered %d",ds.iNum);
	return 0;
}
