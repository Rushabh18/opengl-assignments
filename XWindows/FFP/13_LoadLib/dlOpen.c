#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main()
{
	typedef int (*myAddNumFunc)(int, int);
	myAddNumFunc pfnAddNum = NULL;
	char *error;	
	void *handle = NULL;

	handle = dlopen("mylib.so", RTLD_LAZY);
	if(handle == NULL)
	{
		fputs(dlerror(), stderr);
		exit(1);
	}
	
	pfnAddNum = (myAddNumFunc)dlsym(handle, "addNum");
	if((error = dlerror()) != NULL)
	{
		fputs(error, stderr);
		exit(1);
	}
	
	printf("\n100 + 100 = %d", pfnAddNum(100,100));
	printf("\n199 + 301 = %d", pfnAddNum(199,301));
	
	dlclose(handle);

}
