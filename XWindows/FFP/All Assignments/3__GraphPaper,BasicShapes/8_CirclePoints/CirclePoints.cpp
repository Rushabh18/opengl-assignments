#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glx.h>

#define PI 3.145
using namespace std;

bool bFullScreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 600;
int giWindowHeight = 600;

GLXContext gGLXContext;

struct Vertex
{
	float x;
	float y;
	float z;
}; 

struct Color
{
	float r;
	float g;
	float b;
}; 

int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;

	//code
	CreateWindow();

	//initialize
	initialize();
	
	//Game Loop
	XEvent event;
	KeySym keysym;

	while( bDone == false )
	{
		while (XPending(gpDisplay))
		{
		    XNextEvent(gpDisplay, &event);
		    switch (event.type)
		    {
		    case MapNotify:
		        break;

		    case KeyPress:
		        keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
		        switch (keysym)
		        {
		        case XK_Escape:
		            uninitialize();
		            exit(0);
		        case XK_f:
		            if (bFullScreen == false)
		            {
		                ToggleFullScreen();
		                bFullScreen = true;
		            }
		            else
		            {
		                ToggleFullScreen();
		                bFullScreen = false;
		            }
		        default:
		            break;
		        }
		        break;

		    case ButtonPress:
		        switch (event.xbutton.button)
		        {
		        case 1:
		            break;
		        case 2:
		            break;
		        case 3:
		            break;
		        }
		        break;

		    case MotionNotify:
		        break;

		    case ConfigureNotify:
		        winWidth = event.xconfigure.width;
		        winHeight = event.xconfigure.height;
	 		resize(winWidth, winHeight);
		        break;

		    case Expose:
		        break;

		    case DestroyNotify:
		        break;

		    case 33:
			bDone = true;
			break;
		    default:
		        break;
		    }
		}
       		display();
	}
	return 0;
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen = 0;
	int defaultDepth = 0;
	int styleMask = 0;

	static int frameBufferAttributes[] =
	{
	GLX_RGBA,
	GLX_RED_SIZE, 1,
	GLX_GREEN_SIZE, 1,
	GLX_BLUE_SIZE, 1,
	GLX_ALPHA_SIZE, 1,
	None
	};

	gpDisplay = XOpenDisplay(NULL);
	if( gpDisplay == NULL )
	{
		printf("Error in XOpenDisplay");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;

	winAttribs.background_pixmap = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, 
				RootWindow(gpDisplay, gpXVisualInfo->screen), 
				0,
				0, 
				giWindowWidth, 
				giWindowHeight, 
				0, 
				gpXVisualInfo->depth, 
				InputOutput, 
				gpXVisualInfo->visual, 
				styleMask, 
				&winAttribs) ; 
	if(!gWindow)
	{
		printf("Error:: Failed to Create Window.\nExitting Now...");
		uninitialize();
		exit(1);
	}

	XStoreName( gpDisplay, gWindow, "Red Line");

	Atom wmDelete = XInternAtom( gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &wmDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), False, StructureNotifyMask, &xev);	
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(giWindowWidth, giWindowHeight);
}

void DrawLine(Vertex v[2], Color color)
{
	glColor3f(color.r, color.g, color.b);
	glBegin(GL_LINES);
	glVertex3f(v[0].x, v[0].y, v[0].z);
	glVertex3f(v[1].x, v[1].y, v[1].z);		
	glEnd();
}

void FillVertex(Vertex *v, float x, float y, float z)
{
	v->x = x;
	v->y = y;
	v->z = z;
}

void FillColor(Color *color, float r, float g, float b)
{
	color->r = r;
	color->g = g;
	color->b = b;
}

void DrawVerticalLines()
{
	Vertex v[2];
	Color blueColor;

	float xSpacing = 2.0f/40;
	float x = 1.0f;

	int i = 0;

	FillColor(&blueColor, 0.0f, 0.0f, 1.0f);

	glLineWidth(1);
	for(i=0;i<41;i++)
	{
		FillVertex(&v[0], x, -1.0f, 0.0f);
		FillVertex(&v[1], x, 1.0f, 0.0f);
		DrawLine(v, blueColor);	
		x -= xSpacing;	
	}
}

void DrawHorizontalLines()
{
	Vertex v[2];
	Color blueColor;

	float ySpacing = 2.0f/40;
	float y = 1.0f;

	int i = 0;

	FillColor(&blueColor, 0.0f, 0.0f, 1.0f);

	glLineWidth(1);
	for(i=0;i<41;i++)
	{
		FillVertex(&v[0], -1.0f, y, 0.0f);
		FillVertex(&v[1], 1.0f, y, 0.0f);
		DrawLine(v, blueColor);	
		y -= ySpacing;	
	}
}

void DrawAxis()
{
	Color greenColor;
	Color redColor;
	Vertex v[2];

	glLineWidth(3);
	FillColor(&greenColor, 0.0f, 1.0f, 0.0f);
	FillVertex(&v[0], 0.0f, -1.0f, 0.0f);
	FillVertex(&v[1], 0.0f, 1.0f, 0.0f);
	DrawLine(v, greenColor);

	glLineWidth(3);
	FillColor(&redColor, 1.0f, 0.0f, 0.0f);
	FillVertex(&v[0], -1.0f, 0.0f, 0.0f);
	FillVertex(&v[1], 1.0f, 0.0f, 0.0f);
	DrawLine(v, redColor);
}

void DrawLineCirclePoints(float radius, Color color)
{	
	float angle = 0.0f;
	glColor3f(color.r, color.g, color.b);
	glBegin(GL_POINTS);
	for(angle = 0.0f; angle < 2*PI; angle+=0.001f)
	{
		glVertex3f(radius*cos(angle), radius*sin(angle), 0.0f);
	}		
	glEnd();
}

void display(void)
{
	float radius = 0.5f;
	Color circleColor;
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	DrawHorizontalLines();
	DrawVerticalLines();
	DrawAxis();

	FillColor(&circleColor, 1.0f, 1.0f, 0.0f);
	DrawLineCirclePoints(radius, circleColor);

	glFlush();
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void uninitialize() 
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if (currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
	glXMakeCurrent(gpDisplay, 0, 0);
	}

	if (gGLXContext)
	{
	glXDestroyContext(gpDisplay, gGLXContext);
	}

	if( gWindow )
	{
		XDestroyWindow(gpDisplay,gWindow);
	
	}		

	if( gColormap ) 
	{
		XFreeColormap(gpDisplay, gColormap);
	}		

	if( gpXVisualInfo )
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}	

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}	
}


