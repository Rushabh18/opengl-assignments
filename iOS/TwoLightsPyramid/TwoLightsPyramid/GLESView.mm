//
//  MyView.m
//  bluescreen
//
//  Created by Rushabh on 11/07/18.
//  Copyright © 2018 Rushabh. All rights reserved.
//


#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    float angle;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_pyramid;
    
    GLuint vbo_position;
    GLuint vbo_normal;
    
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    
    GLuint L_KeyPressed_uniform;
    
    GLuint La_1_uniform;
    GLuint Ld_1_uniform;
    GLuint Ls_1_uniform;
    GLuint light_1_position_uniform;
    
    GLuint La_2_uniform;
    GLuint Ld_2_uniform;
    GLuint Ls_2_uniform;
    GLuint light_2_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
    
    bool light;
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    angle = 0.0f;
    light = false;
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        // *** VERTEX SHADER ***
        // create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n"                                        \
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                            \
        "uniform mat4 u_model_matrix;"                \
        "uniform mat4 u_view_matrix;"                \
        "uniform mat4 u_projection_matrix;"            \
        "uniform mediump int u_lighting_enabled;"            \
        "uniform vec3 u_La_1;"                        \
        "uniform vec3 u_Ld_1;"                        \
        "uniform vec3 u_Ls_1;"                        \
        "uniform vec4 u_light_position_1;"            \
        "uniform vec3 u_La_2;"                        \
        "uniform vec3 u_Ld_2;"                        \
        "uniform vec3 u_Ls_2;"                        \
        "uniform vec4 u_light_position_2;"            \
        "uniform vec3 u_Ka;"                        \
        "uniform vec3 u_Kd;"                        \
        "uniform vec3 u_Ks;"                        \
        "uniform float u_material_shininess;"        \
        "out vec3 phong_ads_color;"                    \
        "void main(void)"                            \
        "{"                                            \
        "if(u_lighting_enabled == 1)"                \
        "{"                                            \
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"                        \
        "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"        \
        "vec3 light_direction_1 = normalize(vec3(u_light_position_1) - eye_coordinates.xyz);"        \
        "vec3 light_direction_2 = normalize(vec3(u_light_position_2) - eye_coordinates.xyz);"        \
        "float tn_dot_ld_1 = max(dot(transformed_normals, light_direction_1), 0.0);"                \
        "float tn_dot_ld_2 = max(dot(transformed_normals, light_direction_2), 0.0);"                \
        "vec3 ambient_1 = u_La_1 * u_Ka;"                                                            \
        "vec3 ambient_2 = u_La_2 * u_Ka;"                                                            \
        "vec3 diffuse_1 = u_Ld_1 * u_Kd * tn_dot_ld_1;"                                                \
        "vec3 diffuse_2 = u_Ld_2 * u_Kd * tn_dot_ld_2;"                                                \
        "vec3 reflection_vector_1 = reflect(-light_direction_1, transformed_normals);"                \
        "vec3 reflection_vector_2 = reflect(-light_direction_2, transformed_normals);"                \
        "vec3 viewer_vector = normalize(-eye_coordinates.xyz);"                                        \
        "vec3 specular_1 = u_Ls_1 * u_Ks * pow(max(dot(reflection_vector_1, viewer_vector), 0.0), u_material_shininess);"\
        "vec3 specular_2 = u_Ls_2 * u_Ks * pow(max(dot(reflection_vector_2, viewer_vector), 0.0), u_material_shininess);"\
        "vec3 phong_ads_1 = ambient_1 + diffuse_1 + specular_1;"                                    \
        "vec3 phong_ads_2 = ambient_2 + diffuse_2 + specular_2;"                                    \
        "phong_ads_color = phong_ads_1 + phong_ads_2;"                                                \
        "}"                                                                                            \
        "else"                                                                                        \
        "{"                                                                                            \
        "phong_ads_color = vec3(1.0, 1.0, 1.0);"                                                    \
        "}"                                                                                            \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"            \
        "}";

        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision mediump float;"                  \
        "in vec3 phong_ads_color;"                                        \
        "out vec4 FragColor;"                                            \
        "void main(void)"                                                \
        "{"                                                                \
        "FragColor = vec4(phong_ads_color, 1.0);"                                            \
        "}";
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        // attach fragment shader to shader program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        glBindAttribLocation(shaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        model_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        
        L_KeyPressed_uniform = glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");
        
        La_1_uniform = glGetUniformLocation(shaderProgramObject, "u_La_1");
        Ld_1_uniform = glGetUniformLocation(shaderProgramObject, "u_Ld_1");
        Ls_1_uniform = glGetUniformLocation(shaderProgramObject, "u_Ls_1");
        light_1_position_uniform = glGetUniformLocation(shaderProgramObject, "u_light_position_1");
        
        La_2_uniform = glGetUniformLocation(shaderProgramObject, "u_La_2");
        Ld_2_uniform = glGetUniformLocation(shaderProgramObject, "u_Ld_2");
        Ls_2_uniform = glGetUniformLocation(shaderProgramObject, "u_Ls_2");
        light_2_position_uniform = glGetUniformLocation(shaderProgramObject, "u_light_position_2");
        
        Ka_uniform = glGetUniformLocation(shaderProgramObject, "u_Ka");
        Kd_uniform = glGetUniformLocation(shaderProgramObject, "u_Kd");
        Ks_uniform = glGetUniformLocation(shaderProgramObject, "u_Ks");
        material_shininess_uniform = glGetUniformLocation(shaderProgramObject, "u_material_shininess");
        // get texture sampler uniform location
      
        // *** vertices, colors, shader attribs, vbo, vao initializations ***
        const GLfloat pyramidVertices[] =
        {
            0.0f, 1.0f, 0.0f, //right
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            
            0.0f, 1.0f, 0.0f, //front
            1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            
            0.0f, 1.0f, 0.0f, //left
            -1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,
            
            0.0f, 1.0f, 0.0f, //back
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f
        };
        
        const GLfloat pyramidNormals[] =
        {
            0.894427f, 0.447214f, 0.0f, //right
            0.894427f, 0.447214f, 0.0f,
            0.894427f, 0.447214f, 0.0f,
            
            0.0f, 0.447214f, 0.894427f, //front
            0.0f, 0.447214f, 0.894427f,
            0.0f, 0.447214f, 0.894427f,
            
            -0.894427f, 0.447214f, 0.0f, //left
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f, 0.447214f, 0.0f,
            
            0.0f, 0.447214f, -0.894427f, //back
            0.0f, 0.447214f, -0.894427f,
            0.0f, 0.447214f, -0.894427f,
        };
        

        glGenVertexArrays(1, &vao_pyramid);
        
        glBindVertexArray(vao_pyramid);
        
            glGenBuffers(1, &vbo_position);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
                glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        
                glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
            glGenBuffers(1, &vbo_normal);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_normal);
                glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
        
                glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);

        // enable depth testing
        glEnable(GL_DEPTH_TEST);
        // depth test to do
        glDepthFunc(GL_LEQUAL);
        // We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        glEnable(GL_TEXTURE_2D);
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // blue
        
        // set projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    // code
    GLfloat light_1_Ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat light_1_Diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
    GLfloat light_1_Specular[] = { 1.0f, 0.0f, 0.0f ,0.0f };
    GLfloat light_1_Position[] = { 100.0f, 100.0f, 100.0f, 1.0f };
    
    GLfloat light_2_Ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat light_2_Diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
    GLfloat light_2_Specular[] = { 0.0f, 0.0f, 1.0f, 0.0f };
    GLfloat light_2_Position[] = { -100.0f, 100.0f, 100.0f, 1.0f };
    
    GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat material_shininess = 50.0f;
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using OpenGL program object
    glUseProgram(shaderProgramObject);
    
    if (light == true)
    {
        glUniform1i(L_KeyPressed_uniform, 1);
        
        glUniform3fv(La_1_uniform, 1, light_1_Ambient);
        glUniform3fv(Ld_1_uniform, 1, light_1_Diffuse);
        glUniform3fv(Ls_1_uniform, 1, light_1_Specular);
        glUniform4fv(light_1_position_uniform, 1, light_1_Position);
        
        glUniform3fv(La_2_uniform, 1, light_2_Ambient);
        glUniform3fv(Ld_2_uniform, 1, light_2_Diffuse);
        glUniform3fv(Ls_2_uniform, 1, light_2_Specular);
        glUniform4fv(light_2_position_uniform, 1, light_2_Position);
        
        glUniform3fv(Ka_uniform, 1, material_ambient);
        glUniform3fv(Kd_uniform, 1, material_diffuse);
        glUniform3fv(Ks_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);
    }
    else
    {
        glUniform1i(L_KeyPressed_uniform, 0);
    }
    
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 rotationMatrix = vmath::mat4::identity();
    
    modelMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    
    rotationMatrix = vmath::rotate(angle , 0.0f, 1.0f, 0.0f);
    
    modelMatrix = modelMatrix * rotationMatrix;
    
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1,GL_FALSE, perspectiveProjectionMatrix);
    
    glBindVertexArray(vao_pyramid);
    
    glDrawArrays(GL_TRIANGLES, 0, 12);
    
    glBindVertexArray(0);
    
    glUseProgram(0);

    
    if(angle > 360)
        angle = 0.0f;
    angle += 0.5f;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f,((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
  
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    light = !light;
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

- (void)dealloc
{
    // code
    // destroy vao
    if (vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }
    
    // destroy vbo
    if (vbo_position)
    {
        glDeleteBuffers(1, &vbo_position);
        vbo_position = 0;
    }
    if (vbo_normal)
    {
        glDeleteBuffers(1, &vbo_normal);
        vbo_normal = 0;
    }
    
    // detach vertex shader from shader program object
    glDetachShader(shaderProgramObject, vertexShaderObject);
    // detach fragment  shader from shader program object
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    // delete vertex shader object
    glDeleteShader(vertexShaderObject);
    vertexShaderObject = 0;
    // delete fragment shader object
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject = 0;
    
    // delete shader program object
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
