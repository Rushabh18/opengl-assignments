//
//  main.m
//  Ortho
//
//  Created by Rushabh on 11/07/18.
//  Copyright © 2018 Rushabh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
