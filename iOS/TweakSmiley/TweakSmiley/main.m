//
//  main.m
//  TweakSmiley
//
//  Created by Rushabh on 16/07/18.
//  Copyright © 2018 Rushabh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
