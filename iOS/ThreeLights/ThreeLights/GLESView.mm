//
//  MyView.m
//  bluescreen
//
//  Created by Rushabh on 11/07/18.
//  Copyright © 2018 Rushabh. All rights reserved.
//


#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"

#import "GLESView.h"
#import "SphereMesh.h"

@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObjectPerVertex;
    GLuint gFragmentShaderObjectPerVertex;
    GLuint gShaderProgramObjectPerVertex;
    
    GLuint gVertexShaderObjectPerFragment;
    GLuint gFragmentShaderObjectPerFragment;
    GLuint gShaderProgramObjectPerFragment;
    
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
    
    GLuint L_KeyPressed_uniform;
    GLuint V_KeyPressed_uniform;
    GLuint F_KeyPressed_uniform;
    
    GLuint La0_uniform;
    GLuint Ld0_uniform;
    GLuint Ls0_uniform;
    GLuint light0_position_uniform;
    
    GLuint La1_uniform;
    GLuint Ld1_uniform;
    GLuint Ls1_uniform;
    GLuint light1_position_uniform;
    
    GLuint La2_uniform;
    GLuint Ld2_uniform;
    GLuint Ls2_uniform;
    GLuint light2_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    GLfloat angle;
    vmath::mat4 perspectiveProjectionMatrix;
    
    bool light;
    bool bLongPressed;
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];
    light = false;
    bLongPressed = true;
    
    if(self)
    {
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1,&defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER,colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,backingWidth,backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteRenderbuffers(1,&colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            
            return(nil);
        }
        
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;
        animationFrameInterval=60; // default since iOS 8.2
        
        // *** VERTEX SHADER ***
        // create shader
        gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCodePerVertex =
        "#version 300 es"                            \
        "\n"                                        \
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                            \
        "uniform mat4 u_model_matrix;"                \
        "uniform mat4 u_view_matrix;"                \
        "uniform mat4 u_projection_matrix;"            \
        "uniform int u_lighting_enabled;"            \
        "uniform vec3 u_La[3];"                        \
        "uniform vec3 u_Ld[3];"                        \
        "uniform vec3 u_Ls[3];"                        \
        "uniform vec4 u_light_position[3];"            \
        "uniform vec3 u_Ka;"                        \
        "uniform vec3 u_Kd;"                        \
        "uniform vec3 u_Ks;"                        \
        "uniform mediump float u_material_shininess;"        \
        "out vec3 phong_ads_color;"                    \
        "void main(void)"                            \
        "{"                                            \
        "if(u_lighting_enabled == 1)"                \
        "{"                                            \
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"                    \
        "vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"    \
        "int i = 0;"                                                                            \
        "vec3 light_direction;"                                                                    \
        "float tn_dot_ld;"                                                                        \
        "vec3 ambient;"                                                                            \
        "vec3 diffuse;"                                                                            \
        "vec3 reflection_vector;"                                                                \
        "vec3 viewer_vector;"                                                                    \
        "vec3 specular;"                                                                        \
        "vec3 phong_ads[3];"                                                                    \
        "for(i = 0; i < 3; i++)"                                                                \
        "{"                                                                                        \
        "light_direction = normalize(vec3(u_light_position[i]) - eye_coordinates.xyz);"            \
        "tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);"                        \
        "ambient = u_La[i] * u_Ka;"                                                                \
        "diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"                                                    \
        "reflection_vector = reflect(-light_direction, transformed_normals);"                    \
        "viewer_vector = normalize(-eye_coordinates.xyz);"                                        \
        "specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"\
        "phong_ads[i] = ambient + diffuse + specular;"                                            \
        "}"                                                                                        \
        "phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];"                            \
        "}"                                                                                        \
        "else"                                                                                    \
        "{"                                                                                        \
        "phong_ads_color = vec3(1.0, 1.0, 1.0);"                                                \
        "}"                                                                                        \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"        \
        "}";
        
        glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar **)&vertexShaderSourceCodePerVertex, NULL);
        
        // compile shader
        glCompileShader(gVertexShaderObjectPerVertex);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCodePerVertex =
        "#version 300 es"                                                \
        "\n"                                                            \
        "precision mediump float;"                      \
        "in vec3 phong_ads_color;"                    \
        "out vec4 FragColor;"                        \
        "void main(void)"                            \
        "{"                                            \
        "FragColor = vec4(phong_ads_color, 1.0);"    \
        "}";
        glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);
        
        // compile shader
        glCompileShader(gFragmentShaderObjectPerVertex);
        glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        gShaderProgramObjectPerVertex = glCreateProgram();
        
        // attach vertex shader to shader program
        glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
        glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(gShaderProgramObjectPerVertex, VDG_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(gShaderProgramObjectPerVertex, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(gShaderProgramObjectPerVertex);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_projection_matrix");
        
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_lighting_enabled");
        V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_vertex_enabled");
        F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_fragment_enabled");
        
        La0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[0]");
        Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[0]");
        Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[0]");
        
        light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[0]");
        
        La1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[1]");
        Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[1]");
        Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[1]");
        
        light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[1]");
        
        La2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[2]");
        Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[2]");
        Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[2]");
        
        light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[2]");
        
        Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ka");
        Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Kd");
        Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ks");
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_material_shininess");
        

        // *** VERTEX SHADER ***
        // create shader
        gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
        
        // provide source code to shader
        const GLchar *vertexShaderSourceCodePerFragment =
        "#version 300 es"                            \
        "\n"                                        \
        "in vec4 vPosition;"                        \
        "in vec3 vNormal;"                            \
        "uniform mat4 u_model_matrix;"                \
        "uniform mat4 u_view_matrix;"                \
        "uniform mat4 u_projection_matrix;"            \
        "uniform mediump int u_lighting_enabled;"            \
        "uniform vec4 u_light_position[3];"            \
        "out vec3 transformed_normals;"                \
        "out vec3 light_direction[3];"                \
        "out vec3 viewer_vector;"                    \
        "void main(void)"                            \
        "{"                                            \
        "if(u_lighting_enabled == 1)"                \
        "{"                                            \
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"                    \
        "transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"                    \
        "int i;"                                                                                \
        "for(i = 0;i<3;i++)"                                                                    \
        "{"                                                                                        \
        "light_direction[i] = u_light_position[i].xyz - eye_coordinates.xyz;"                    \
        "}"                                                                                        \
        "viewer_vector = -eye_coordinates.xyz;"                                                    \
        "}"                                                                                        \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"        \
        "}";
        
        glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertexShaderSourceCodePerFragment, NULL);
        
        // compile shader
        glCompileShader(gVertexShaderObjectPerFragment);
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** FRAGMENT SHADER ***
        // re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // create shader
        gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);
        
        // provide source code to shader
        const GLchar *fragmentShaderSourceCodePerFragment =
        "#version 300 es"                                                \
        "\n"                                                            \
        "precision mediump float;"                      \
        "in vec3 phong_ads_color;"                    \
        "in vec3 transformed_normals;"                \
        "in vec3 light_direction[3];"                \
        "in vec3 viewer_vector;"                    \
        "out vec4 FragColor;"                        \
        "uniform int u_lighting_enabled;"            \
        "uniform vec3 u_La[3];"                        \
        "uniform vec3 u_Ld[3];"                        \
        "uniform vec3 u_Ls[3];"                        \
        "uniform vec3 u_Ka;"                        \
        "uniform vec3 u_Kd;"                        \
        "uniform vec3 u_Ks;"                        \
        "uniform float u_material_shininess;"        \
        "void main(void)"                                                \
        "{"                                                                \
        "if(u_lighting_enabled == 1)"                                    \
        "{"                                                                \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);"            \
        "vec3 normalized_light_direction[3];"                                            \
        "int i;"                                                                        \
        "vec3 ambient;"                                                    \
        "float tn_dot_ld;"        \
        "vec3 diffuse;"                                    \
        "vec3 reflection_vector;"                                                        \
        "vec3 specular;"                                                                \
        "vec3 phong_ads[3];"                                                            \
        "for(i=0;i<3;i++)"                                                                \
        "{"                                                                                \
        "normalized_light_direction[i] = normalize(light_direction[i]);"                \
        "}"                                                                                \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);"                        \
        "for(i=0;i<3;i++)"                                                                \
        "{"                                                                                \
        "ambient = u_La[i] * u_Ka;"                                                        \
        "tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction[i]), 0.0);"        \
        "vec3 diffuse = u_Ld[i] * u_Kd * tn_dot_ld;"                                    \
        "vec3 reflection_vector = reflect(-normalized_light_direction[i], normalized_transformed_normals);"    \
        "vec3 specular = u_Ls[i] * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"\
        "phong_ads[i] = ambient + diffuse + specular;"                                    \
        "}"                                                                                \
        "vec3 phong_ads_color = phong_ads[0] + phong_ads[1] + phong_ads[2];"            \
        "FragColor = vec4(phong_ads_color, 1.0);"                                        \
        "}"                                                                                \
        "else"                                                                            \
        "{"                                                                                \
        "FragColor = vec4(1.0,1.0,1.0,1.0);"                                            \
        "}"                                                                                \
        "}";
        
        glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);
        
        // compile shader
        glCompileShader(gFragmentShaderObjectPerFragment);
       glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // *** SHADER PROGRAM ***
        // create
        gShaderProgramObjectPerFragment = glCreateProgram();
        
        glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
        glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
        
        // pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(gShaderProgramObjectPerFragment, VDG_ATTRIBUTE_POSITION, "vPosition");
        glBindAttribLocation(gShaderProgramObjectPerFragment, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        // link shader
        glLinkProgram(gShaderProgramObjectPerFragment);
        iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength>0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        // get MVP uniform location
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projection_matrix");
        
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lighting_enabled");
        V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_vertex_enabled");
        F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_fragment_enabled");
        
        La0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[0]");
        Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[0]");
        Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[0]");
        
        light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[0]");
        
        La1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[1]");
        Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[1]");
        Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[1]");
        
        light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[1]");
        
        La2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[2]");
        Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[2]");
        Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[2]");
        
        light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[2]");
        
        Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ka");
        Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Kd");
        Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ks");
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_material_shininess");


        
        makeSphere(2.0 ,60, 60);
        // get texture sampler uniform location
      
        // *** vertices, colors, shader attribs, vbo, vao initializations ***

        // enable depth testing
        glEnable(GL_DEPTH_TEST);
        // depth test to do
        glDepthFunc(GL_LEQUAL);
        // We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        glEnable(GL_TEXTURE_2D);
        
        // set background color
        glClearColor(0.1f, 0.1f, 0.1f, 0.0f); // blue
        
        // set projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        // GESTURE RECOGNITION
        // Tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; // touch of 1 finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        // this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

+(Class)layerClass
{
    // code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    // code
    GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat light0Diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
    GLfloat light0Specular[] = { 1.0f, 0.0f, 0.0f ,0.0f };
    GLfloat light0Position[] = { 000.0f, 100.0f, 100.0f, 1.0f };
    
    GLfloat light1Diffuse[] = { 0.0f, 1.0f, 0.0f, 0.0f };
    GLfloat light1Specular[] = { 0.0f, 1.0f, 0.0f ,0.0f };
    GLfloat light1Position[] = { 100.0f, 000.0f, 100.0f, 1.0f };
    
    GLfloat light2Diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
    GLfloat light2Specular[] = { 0.0f, 0.0f, 1.0f ,0.0f };
    GLfloat light2Position[] = { 100.0f, 100.0f, 000.0f, 1.0f };
    
    GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat material_shininess = 50.0f;
    
    angle += 0.01f;
    if (angle >= 2*3.145)
    {
        angle = 0.0f;
    }
    light0Position[1] = 100.0f * cos(angle);
    light0Position[2] = 100.0f * sin(angle);
    
    light1Position[0] = 100.0f * cos(angle);
    light1Position[2] = 100.0f * sin(angle);
    
    light2Position[0] = 100.0f * cos(angle);
    light2Position[1] = 100.0f * sin(angle);
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // start using OpenGL program object
    if (light == true)
    {
        if (bLongPressed)
        {
            glUseProgram(gShaderProgramObjectPerFragment);
        }
        else
        {
            glUseProgram(gShaderProgramObjectPerVertex);
        }
        
        glUniform1i(L_KeyPressed_uniform, 1);
        
        glUniform3fv(La0_uniform, 1, lightAmbient);
        glUniform3fv(Ld0_uniform, 1, light0Diffuse);
        glUniform3fv(Ls0_uniform, 1, light0Specular);
        glUniform4fv(light0_position_uniform, 1, light0Position);
        
        glUniform3fv(La1_uniform, 1, lightAmbient);
        glUniform3fv(Ld1_uniform, 1, light1Diffuse);
        glUniform3fv(Ls1_uniform, 1, light1Specular);
        glUniform4fv(light1_position_uniform, 1, light1Position);
        
        glUniform3fv(La2_uniform, 1, lightAmbient);
        glUniform3fv(Ld2_uniform, 1, light2Diffuse);
        glUniform3fv(Ls2_uniform, 1, light2Specular);
        glUniform4fv(light2_position_uniform, 1, light2Position);
        
        glUniform3fv(Ka_uniform, 1, material_ambient);
        glUniform3fv(Kd_uniform, 1, material_diffuse);
        glUniform3fv(Ks_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);
    }
    else
    {
        glUniform1i(L_KeyPressed_uniform, 0);
    }
    
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    
    modelMatrix = vmath::translate(0.0f, 0.0f, -8.0f);
    
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1,GL_FALSE, perspectiveProjectionMatrix);
    
    draw();
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    // code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_WIDTH,&width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,GL_RENDERBUFFER_HEIGHT,&height);
    
    glGenRenderbuffers(1,&depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT16,width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthRenderbuffer);
    
    glViewport(0,0,width,height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f,((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
  
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if (!isAnimating)
    {
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    light = !light;
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    bLongPressed = !bLongPressed;
    if(bLongPressed)
    {
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projection_matrix");
        
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lighting_enabled");
        V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_vertex_enabled");
        F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_per_fragment_enabled");
        
        La0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[0]");
        Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[0]");
        Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[0]");
        
        light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[0]");
        
        La1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[1]");
        Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[1]");
        Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[1]");
        
        light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[1]");
        
        La2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_La[2]");
        Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ld[2]");
        Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ls[2]");
        
        light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position[2]");
        
        Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ka");
        Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Kd");
        Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_Ks");
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_material_shininess");
    }
    else
    {
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_projection_matrix");
        
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_lighting_enabled");
        V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_vertex_enabled");
        F_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_per_fragment_enabled");
        
        La0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[0]");
        Ld0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[0]");
        Ls0_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[0]");
        
        light0_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[0]");
        
        La1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[1]");
        Ld1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[1]");
        Ls1_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[1]");
        
        light1_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[1]");
        
        La2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_La[2]");
        Ld2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ld[2]");
        Ls2_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ls[2]");
        
        light2_position_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position[2]");
        
        Ka_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ka");
        Kd_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Kd");
        Ks_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_Ks");
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_material_shininess");

    }
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{

}

- (void)dealloc
{
    // code
    
    glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
    glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
    
    glDetachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
    glDetachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);
    
    glDeleteShader(gVertexShaderObjectPerVertex);
    gVertexShaderObjectPerVertex = 0;
    
    glDeleteShader(gFragmentShaderObjectPerVertex);
    gFragmentShaderObjectPerVertex = 0;
    
    glDeleteProgram(gShaderProgramObjectPerVertex);
    gShaderProgramObjectPerVertex = 0;
    
    glDeleteShader(gVertexShaderObjectPerFragment);
    gVertexShaderObjectPerFragment = 0;
    
    glDeleteShader(gFragmentShaderObjectPerFragment);
    gFragmentShaderObjectPerFragment = 0;
    
    glDeleteProgram(gShaderProgramObjectPerFragment);
    gShaderProgramObjectPerFragment = 0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1,&defaultFramebuffer);
        defaultFramebuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
