//
//  MyView.h
//  bluescreen
//
//  Created by Rushabh on 11/07/18.
//  Copyright © 2018 Rushabh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

-(void)startAnimation;
-(void)stopAnimation;

@end
