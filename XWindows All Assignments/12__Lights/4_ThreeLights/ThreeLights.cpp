#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#define ANGLE_INCREMENT 0.2f

using namespace std;

bool bFullScreen = false;
bool gbLightsEnable  = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth = 600;
int giWindowHeight = 600;

float angleRed = 0.0f;
float angleBlue = 0.0f;
float angleGreen = 0.0f;

GLfloat light0_Ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_Diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_Specular[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_Position[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat light1_Ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light1_Diffuse[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat light1_Specular[] = { 0.0f, 1.0f, 0.0f, 0.0f };
GLfloat light1_Position[] = { 0.0f, 0.0f, 0.0f, 0.0f };;

GLfloat light2_Ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light2_Diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light2_Specular[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light2_Position[] = { 0.0f, 0.0f, 0.0f, 0.0f };

GLfloat material_Ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat material_Diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_Specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_Shininess[] = { 50.0f };

GLUquadric *quadric = NULL;

GLXContext gGLXContext;

int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);
	void update(void);

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;

	//code
	CreateWindow();

	//initialize
	initialize();
	
	//Game Loop
	XEvent event;
	KeySym keysym;

	while( bDone == false )
	{
		while (XPending(gpDisplay))
		{
		    XNextEvent(gpDisplay, &event);
		    switch (event.type)
		    {
		    case MapNotify:
		        break;

		    case KeyPress:
		        keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
		        switch (keysym)
		        {
		        case XK_Escape:
		            uninitialize();
		            exit(0);
		        case XK_f:
		            if (bFullScreen == false)
		            {
		                ToggleFullScreen();
		                bFullScreen = true;
		            }
		            else
		            {
		                ToggleFullScreen();
		                bFullScreen = false;
		            }
		            break;
		         case XK_l:
		         	if(gbLightsEnable == true)
		         	{
		         		gbLightsEnable = false;
		         		glDisable(GL_LIGHTING);
		         	}
		         	else
		         	{
		         		gbLightsEnable = true;
		         		glEnable(GL_LIGHTING);
		         	}
		         	break;	         	
		        default:
		            break;
		        }
		        break;

		    case ButtonPress:
		        switch (event.xbutton.button)
		        {
		        case 1:
		            break;
		        case 2:
		            break;
		        case 3:
		            break;
		        }
		        break;

		    case MotionNotify:
		        break;

		    case ConfigureNotify:
		        winWidth = event.xconfigure.width;
		        winHeight = event.xconfigure.height;
	 		resize(winWidth, winHeight);
		        break;

		    case Expose:
		        break;

		    case DestroyNotify:
		        break;

		    case 33:
			bDone = true;
			break;
		    default:
		        break;
		    }
		}
       		display();
       		update();
	}
	return 0;
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen = 0;
	int defaultDepth = 0;
	int styleMask = 0;

	static int frameBufferAttributes[] =
	{
	GLX_RGBA,
	GLX_RED_SIZE, 8,
	GLX_GREEN_SIZE, 8,
	GLX_BLUE_SIZE, 8,
	GLX_ALPHA_SIZE, 8,
	GLX_DOUBLEBUFFER, True,
	GLX_DEPTH_SIZE, 24,
	None
	};

	gpDisplay = XOpenDisplay(NULL);
	if( gpDisplay == NULL )
	{
		printf("Error in XOpenDisplay");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;

	winAttribs.background_pixmap = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, 
				RootWindow(gpDisplay, gpXVisualInfo->screen), 
				0,
				0, 
				giWindowWidth, 
				giWindowHeight, 
				0, 
				gpXVisualInfo->depth, 
				InputOutput, 
				gpXVisualInfo->visual, 
				styleMask, 
				&winAttribs) ; 
	if(!gWindow)
	{
		printf("Error:: Failed to Create Window.\nExitting Now...");
		uninitialize();
		exit(1);
	}

	XStoreName( gpDisplay, gWindow, "3 Lights");

	Atom wmDelete = XInternAtom( gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &wmDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), False, StructureNotifyMask, &xev);	
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	quadric = gluNewQuadric();

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light0_Ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_Diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light0_Specular);

	glLightfv(GL_LIGHT1, GL_AMBIENT, light1_Ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_Diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light1_Specular);

	glLightfv(GL_LIGHT2, GL_AMBIENT, light2_Ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light2_Diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light2_Specular);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_Ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_Shininess);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(giWindowWidth, giWindowHeight);
}

void update(void)
{
	if (angleRed > 360)
		angleRed = 0.0f;
	angleRed += ANGLE_INCREMENT;

	if (angleGreen > 360)
		angleGreen = 0.0f;
	angleGreen += ANGLE_INCREMENT;

	if (angleBlue > 360)
		angleBlue = 0.0f;
	angleBlue += ANGLE_INCREMENT;
}

void DrawSphere()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 0.75f, 30, 30);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
		gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		glPushMatrix();
			glRotatef(angleRed, 1.0f, 0.0f, 0.0f);
			light0_Position[1] = angleRed;
			glLightfv(GL_LIGHT0, GL_POSITION, light0_Position);
		glPopMatrix();

		glPushMatrix();
			glRotatef(angleGreen, 0.0f, 1.0f, 0.0f);
			light1_Position[0] = angleGreen;
			glLightfv(GL_LIGHT1, GL_POSITION, light1_Position);
		glPopMatrix();

		glPushMatrix();
			glRotatef(angleBlue, 0.0f, 0.0f, 1.0f);
			light2_Position[0] = angleBlue;
			glLightfv(GL_LIGHT2, GL_POSITION, light2_Position);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0.0f, 0.0f, -3.0f);
			DrawSphere();
		glPopMatrix();

	glPopMatrix();

	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize() 
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if (currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
	glXMakeCurrent(gpDisplay, 0, 0);
	}

	if (gGLXContext)
	{
	glXDestroyContext(gpDisplay, gGLXContext);
	}

	if( gWindow )
	{
		XDestroyWindow(gpDisplay,gWindow);
	
	}		

	if( gColormap ) 
	{
		XFreeColormap(gpDisplay, gColormap);
	}		

	if( gpXVisualInfo )
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}	

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}	
}
