#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#define ANGLE_INCREMENT 0.2f
#define SIDE_LENGTH 1.0f

using namespace std;

bool bFullScreen = false;
bool gbLightsEnable  = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth = 600;
int giWindowHeight = 600;

float angleCube = 0.0f;
float anglePyramid = 0.0f;
float angleSphere = 0.0f;

GLfloat light0_Ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_Diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_Specular[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_Position[] = { 2.0f, 1.0f, 1.0f, 0.0f };

GLfloat light1_Ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light1_Diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light1_Specular[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light1_Position[] = { -2.0f, 1.0f, 1.0f, 0.0f };

GLfloat material_Ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat material_Diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_Specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_Shininess[] = { 50.0f };

GLboolean gbCube = GL_FALSE;
GLboolean gbPyramid = GL_TRUE;
GLboolean gbSphere = GL_FALSE;

GLUquadric *quadric = NULL;

GLXContext gGLXContext;

typedef float axis;

struct Vertex
{
	axis x;
	axis y;
	axis z;
};

int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);
	void update(void);

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;

	//code
	CreateWindow();

	//initialize
	initialize();
	
	//Game Loop
	XEvent event;
	KeySym keysym;

	while( bDone == false )
	{
		while (XPending(gpDisplay))
		{
		    XNextEvent(gpDisplay, &event);
		    switch (event.type)
		    {
		    case MapNotify:
		        break;

		    case KeyPress:
		        keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
		        switch (keysym)
		        {
		        case XK_Escape:
		            uninitialize();
		            exit(0);
		        case XK_f:
		            if (bFullScreen == false)
		            {
		                ToggleFullScreen();
		                bFullScreen = true;
		            }
		            else
		            {
		                ToggleFullScreen();
		                bFullScreen = false;
		            }
		            break;
		         case XK_l:
		         	if(gbLightsEnable == true)
		         	{
		         		gbLightsEnable = false;
		         		glDisable(GL_LIGHTING);
		         	}
		         	else
		         	{
		         		gbLightsEnable = true;
		         		glEnable(GL_LIGHTING);
		         	}
		         	break;
		         case XK_c:
		         	gbCube = true;
		         	gbSphere = false;
		         	gbPyramid = false;
		         	break;
		         case XK_s:
		         	gbCube = false;
		         	gbSphere = true;
		         	gbPyramid = false;
		         	break;
		         case XK_p:
		         	gbCube = false;
		         	gbSphere = false;
		         	gbPyramid = true;
		         	break;		         	
		        default:
		            break;
		        }
		        break;

		    case ButtonPress:
		        switch (event.xbutton.button)
		        {
		        case 1:
		            break;
		        case 2:
		            break;
		        case 3:
		            break;
		        }
		        break;

		    case MotionNotify:
		        break;

		    case ConfigureNotify:
		        winWidth = event.xconfigure.width;
		        winHeight = event.xconfigure.height;
	 		resize(winWidth, winHeight);
		        break;

		    case Expose:
		        break;

		    case DestroyNotify:
		        break;

		    case 33:
			bDone = true;
			break;
		    default:
		        break;
		    }
		}
       		display();
       		update();
	}
	return 0;
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen = 0;
	int defaultDepth = 0;
	int styleMask = 0;

	static int frameBufferAttributes[] =
	{
	GLX_RGBA,
	GLX_RED_SIZE, 8,
	GLX_GREEN_SIZE, 8,
	GLX_BLUE_SIZE, 8,
	GLX_ALPHA_SIZE, 8,
	GLX_DOUBLEBUFFER, True,
	GLX_DEPTH_SIZE, 24,
	None
	};

	gpDisplay = XOpenDisplay(NULL);
	if( gpDisplay == NULL )
	{
		printf("Error in XOpenDisplay");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;

	winAttribs.background_pixmap = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, 
				RootWindow(gpDisplay, gpXVisualInfo->screen), 
				0,
				0, 
				giWindowWidth, 
				giWindowHeight, 
				0, 
				gpXVisualInfo->depth, 
				InputOutput, 
				gpXVisualInfo->visual, 
				styleMask, 
				&winAttribs) ; 
	if(!gWindow)
	{
		printf("Error:: Failed to Create Window.\nExitting Now...");
		uninitialize();
		exit(1);
	}

	XStoreName( gpDisplay, gWindow, "Lights");

	Atom wmDelete = XInternAtom( gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &wmDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), False, StructureNotifyMask, &xev);	
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	quadric = gluNewQuadric();

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light0_Ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_Diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light0_Specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light0_Position);

	glLightfv(GL_LIGHT1, GL_AMBIENT, light1_Ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_Diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light1_Specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_Position);

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_Ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_Shininess);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(giWindowWidth, giWindowHeight);
}

void update(void)
{
	if (anglePyramid > 360)
		anglePyramid = 0.0f;
	anglePyramid += ANGLE_INCREMENT;

	if (angleCube > 360)
		angleCube = 0.0f;
	angleCube += ANGLE_INCREMENT;

	if (angleSphere > 360)
		angleSphere = 0.0f;
	angleSphere += ANGLE_INCREMENT;
}

void FillVertexStruct(Vertex *v, axis x, axis y, axis z)
{
	v->x = x;
	v->y = y;
	v->z = z;
}

void FillVertexQuadArray(Vertex v[4], Vertex v1, Vertex v2, Vertex v3, Vertex v4)
{
	FillVertexStruct(&v[0], v1.x, v1.y, v1.z);
	FillVertexStruct(&v[1], v2.x, v2.y, v2.z);
	FillVertexStruct(&v[2], v3.x, v3.y, v3.z);
	FillVertexStruct(&v[3], v4.x, v4.y, v4.z);
}

void FillVertexTriArray(Vertex v[3], Vertex v1, Vertex v2, Vertex v3)
{
	FillVertexStruct(&v[0], v1.x, v1.y, v1.z);
	FillVertexStruct(&v[1], v2.x, v2.y, v2.z);
	FillVertexStruct(&v[2], v3.x, v3.y, v3.z);
}

void DrawSquare(Vertex vArr[4], Vertex normal)
{
	glBegin(GL_QUADS);
	glNormal3f(normal.x, normal.y, normal.z);
	glVertex3f(vArr[0].x, vArr[0].y, vArr[0].z);
	glVertex3f(vArr[1].x, vArr[1].y, vArr[1].z);
	glVertex3f(vArr[2].x, vArr[2].y, vArr[2].z);
	glVertex3f(vArr[3].x, vArr[3].y, vArr[3].z);
	glEnd();
}

void DrawCube()
{
	Vertex squareVertices[4];
	Vertex cubeVertices[8];
	Vertex normalVertices[6];

	FillVertexStruct(&cubeVertices[0], SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[1], -SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[2], -SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[3], SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[4], SIDE_LENGTH, SIDE_LENGTH, -SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[5], -SIDE_LENGTH, SIDE_LENGTH, -SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[6], -SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH);
	FillVertexStruct(&cubeVertices[7], SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH);

	FillVertexStruct(&normalVertices[0], 0.0f, 0.0f, 1.0f);//front
	FillVertexStruct(&normalVertices[1], 0.0f, 0.0f, -1.0f);//back
	FillVertexStruct(&normalVertices[2], -1.0f, 0.0f, 0.0f);//left
	FillVertexStruct(&normalVertices[3], 1.0f, 0.0f, 0.0f);//right
	FillVertexStruct(&normalVertices[4], 0.0f, 1.0f, 0.0f);//top
	FillVertexStruct(&normalVertices[5], 0.0f, -1.0f, 0.0f);//bottom

	FillVertexQuadArray(squareVertices, cubeVertices[0], cubeVertices[1], cubeVertices[2], cubeVertices[3]);
	DrawSquare(squareVertices, normalVertices[0]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[4], cubeVertices[5], cubeVertices[6], cubeVertices[7]);
	DrawSquare(squareVertices, normalVertices[1]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[1], cubeVertices[5], cubeVertices[6], cubeVertices[2]);
	DrawSquare(squareVertices, normalVertices[2]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[0], cubeVertices[4], cubeVertices[7], cubeVertices[3]);
	DrawSquare(squareVertices, normalVertices[3]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[0], cubeVertices[1], cubeVertices[5], cubeVertices[4]);
	DrawSquare(squareVertices, normalVertices[4]);//front

	FillVertexQuadArray(squareVertices, cubeVertices[3], cubeVertices[2], cubeVertices[6], cubeVertices[7]);
	DrawSquare(squareVertices, normalVertices[5]);//front

}

void DrawTriangle(Vertex vArr[3], Vertex normal)
{
	glBegin(GL_TRIANGLES);
	glNormal3f(normal.x, normal.y, normal.z);
	glVertex3f(vArr[0].x, vArr[0].y, vArr[0].z);
	glVertex3f(vArr[1].x, vArr[1].y, vArr[1].z);
	glVertex3f(vArr[2].x, vArr[2].y, vArr[2].z);
	glEnd();
}

void DrawPyramid()
{
	Vertex triangleVertices[3];
	Vertex pyramidVertices[5];
	Vertex normalVertices[4];

	FillVertexStruct(&pyramidVertices[0], 0.0f, SIDE_LENGTH, 0.0f);
	FillVertexStruct(&pyramidVertices[1], SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&pyramidVertices[2], -SIDE_LENGTH, -SIDE_LENGTH, SIDE_LENGTH);
	FillVertexStruct(&pyramidVertices[3], -SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH);
	FillVertexStruct(&pyramidVertices[4], SIDE_LENGTH, -SIDE_LENGTH, -SIDE_LENGTH);

	FillVertexStruct(&normalVertices[0], 0.0f, 0.447214f, 0.894427f);//front
	FillVertexStruct(&normalVertices[1], -0.894427f, 0.447214f, 0.0f);//left
	FillVertexStruct(&normalVertices[2], 0.0f, 0.447214f, -0.894427f);//back
	FillVertexStruct(&normalVertices[3], 0.894427f, 0.447214f, 0.0f);//right
	
	FillVertexTriArray(triangleVertices, pyramidVertices[0], pyramidVertices[1], pyramidVertices[2]);
	DrawTriangle(triangleVertices, normalVertices[0]);//front

	FillVertexTriArray(triangleVertices, pyramidVertices[0], pyramidVertices[2], pyramidVertices[3]);
	DrawTriangle(triangleVertices, normalVertices[1]);//left

	FillVertexTriArray(triangleVertices, pyramidVertices[0], pyramidVertices[3], pyramidVertices[4]);
	DrawTriangle(triangleVertices, normalVertices[2]);//back

	FillVertexTriArray(triangleVertices, pyramidVertices[0], pyramidVertices[4], pyramidVertices[1]);
	DrawTriangle(triangleVertices, normalVertices[3]);//right
}

void DrawSphere()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, 1.25f, 30, 30);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (gbPyramid == GL_TRUE)
	{
		glTranslatef(0.0f, 0.0f, -6.0f);
		glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);
		DrawPyramid();
	}
	else if (gbCube == GL_TRUE)
	{
		glTranslatef(0.0f, 0.0f, -6.0f);
		glRotatef(angleCube, 0.0f, 1.0f, 0.0f);
		glScalef(0.75f, 0.75f, 0.75f);
		DrawCube();
	}
	else if (gbSphere == GL_TRUE)
	{
		glTranslatef(0.0f, 0.0f, -6.0f);
		glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);
		DrawSphere();
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize() 
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if (currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
	glXMakeCurrent(gpDisplay, 0, 0);
	}

	if (gGLXContext)
	{
	glXDestroyContext(gpDisplay, gGLXContext);
	}

	if( gWindow )
	{
		XDestroyWindow(gpDisplay,gWindow);
	
	}		

	if( gColormap ) 
	{
		XFreeColormap(gpDisplay, gColormap);
	}		

	if( gpXVisualInfo )
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}	

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}	
}

