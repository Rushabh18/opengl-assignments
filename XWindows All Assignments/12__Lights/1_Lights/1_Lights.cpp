#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

using namespace std;

bool bFullScreen = false;
bool gbLightsEnable  = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth = 600;
int giWindowHeight = 600;

GLUquadric *quadric = NULL;

GLfloat lightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
GLfloat lightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat lightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat lightPosition[] = {0.0f, 0.0f, 1.0f, 0.0f};

GLfloat materialSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat materialShininess = 50.0f;
GLXContext gGLXContext;

int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;

	//code
	CreateWindow();

	//initialize
	initialize();
	
	//Game Loop
	XEvent event;
	KeySym keysym;

	while( bDone == false )
	{
		while (XPending(gpDisplay))
		{
		    XNextEvent(gpDisplay, &event);
		    switch (event.type)
		    {
		    case MapNotify:
		        break;

		    case KeyPress:
		        keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
		        switch (keysym)
		        {
		        case XK_Escape:
		            uninitialize();
		            exit(0);
		        case XK_f:
		            if (bFullScreen == false)
		            {
		                ToggleFullScreen();
		                bFullScreen = true;
		            }
		            else
		            {
		                ToggleFullScreen();
		                bFullScreen = false;
		            }
		            break;
		         case XK_l:
		         	if(gbLightsEnable == true)
		         	{
		         		gbLightsEnable = false;
		         		glDisable(GL_LIGHTING);
		         	}
		         	else
		         	{
		         		gbLightsEnable = true;
		         		glEnable(GL_LIGHTING);
		         	}
		        default:
		            break;
		        }
		        break;

		    case ButtonPress:
		        switch (event.xbutton.button)
		        {
		        case 1:
		            break;
		        case 2:
		            break;
		        case 3:
		            break;
		        }
		        break;

		    case MotionNotify:
		        break;

		    case ConfigureNotify:
		        winWidth = event.xconfigure.width;
		        winHeight = event.xconfigure.height;
	 		resize(winWidth, winHeight);
		        break;

		    case Expose:
		        break;

		    case DestroyNotify:
		        break;

		    case 33:
			bDone = true;
			break;
		    default:
		        break;
		    }
		}
       		display();
	}
	return 0;
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen = 0;
	int defaultDepth = 0;
	int styleMask = 0;

	static int frameBufferAttributes[] =
	{
	GLX_RGBA,
	GLX_RED_SIZE, 8,
	GLX_GREEN_SIZE, 8,
	GLX_BLUE_SIZE, 8,
	GLX_ALPHA_SIZE, 8,
	GLX_DOUBLEBUFFER, True,
	GLX_DEPTH_SIZE, 24,
	None
	};

	gpDisplay = XOpenDisplay(NULL);
	if( gpDisplay == NULL )
	{
		printf("Error in XOpenDisplay");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;

	winAttribs.background_pixmap = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, 
				RootWindow(gpDisplay, gpXVisualInfo->screen), 
				0,
				0, 
				giWindowWidth, 
				giWindowHeight, 
				0, 
				gpXVisualInfo->depth, 
				InputOutput, 
				gpXVisualInfo->visual, 
				styleMask, 
				&winAttribs) ; 
	if(!gWindow)
	{
		printf("Error:: Failed to Create Window.\nExitting Now...");
		uninitialize();
		exit(1);
	}

	XStoreName( gpDisplay, gWindow, "Lights");

	Atom wmDelete = XInternAtom( gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &wmDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), False, StructureNotifyMask, &xev);	
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);
		
	glEnable(GL_LIGHT0);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(giWindowWidth, giWindowHeight);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();	
	gluSphere(quadric, 0.75f, 30, 30);

	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize() 
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if (currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
	glXMakeCurrent(gpDisplay, 0, 0);
	}

	if (gGLXContext)
	{
	glXDestroyContext(gpDisplay, gGLXContext);
	}

	if( gWindow )
	{
		XDestroyWindow(gpDisplay,gWindow);
	
	}		

	if( gColormap ) 
	{
		XFreeColormap(gpDisplay, gColormap);
	}		

	if( gpXVisualInfo )
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}	

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}	
}


