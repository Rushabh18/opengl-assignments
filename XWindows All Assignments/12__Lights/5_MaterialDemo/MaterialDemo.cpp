#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#define ANGLE_INCREMENT 1.0f
#define SPHERE_RADIUS 0.2f
#define SPHERE_GAP SPHERE_RADIUS/2

using namespace std;

bool bFullScreen = false;
bool gbLightsEnable  = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth = 600;
int giWindowHeight = 600;

bool gbXAxis = false;
bool gbYAxis = false;
bool gbZAxis = false;

float angleLight = 0.0f;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat lightModelAmbiet[] = { 0.2f, 0.2f, 0.2f, 0.2f };
GLfloat lightModelLocalViewer =  0.0f ;


GLfloat materialAmbient[24][4] = {
									{ 0.0215f, 0.1745f, 0.0215f, 1.0f },
								    { 0.135f , 0.225f , 0.1575f, 1.0f }, 
									{ 0.05375f , 0.05f , 0.06625f, 1.0f },
									{ 0.25f , 0.20725f , 0.20725f, 1.0f },
									{ 0.1745f , 0.01175f , 0.01175f, 1.0f },
									{ 0.1f , 0.18725f , 0.1745f, 1.0f },

									{ 0.329412f , 0.223529f , 0.027451f, 1.0f },
									{ 0.2125f , 0.1275f , 0.054f, 1.0f },
									{ 0.25f , 0.25f , 0.25f, 1.0f },
									{ 0.19125f , 0.0735f , 0.0225f, 1.0f },
									{ 0.24725f , 0.1995f , 0.0745f, 1.0f },
									{ 0.19225f , 0.19225f , 0.19225f, 1.0f },

									{ 0.0f , 0.0f , 0.0f, 1.0f },
									{ 0.0f , 0.1f , 0.06f, 1.0f },
									{ 0.0f , 0.0f , 0.0f, 1.0f },
									{ 0.0f , 0.0f , 0.0f, 1.0f },
									{ 0.0f , 0.0f , 0.0f, 1.0f },
									{ 0.0f , 0.0f , 0.0f, 1.0f },

									{ 0.02f , 0.02f , 0.02f, 1.0f },
									{ 0.0f , 0.05f , 0.05f, 1.0f },
									{ 0.0f , 0.05f , 0.0f, 1.0f }, 
									{ 0.05f , 0.0f , 0.0f, 1.0f }, 
									{ 0.05f , 0.05f , 0.05f, 1.0f }, 
									{ 0.05f , 0.05f , 0.0f, 1.0f }
								};
				
GLfloat materialDiffuse[24][4] = {
									{ 0.07568f, 0.61424f, 0.07568f, 1.0f },
									{ 0.54f , 0.89f , 0.63f , 1.0f},
									{ 0.18275f , 0.17f , 0.22525f , 1.0f },
									{ 1.0f , 0.829f , 0.829f , 1.0f },
									{ 0.61424f , 0.04136f , 0.04136f , 1.0f },
									{ 0.396f , 0.74151f , 0.69102f , 1.0f },

									{ 0.780392f , 0.568627f , 0.113725f , 1.0f },
									{ 0.714f , 0.4284f , 0.18144f , 1.0f },
									{ 0.4f , 0.4f , 0.4f , 1.0f },
									{ 0.7038f , 0.27048f , 0.0828f , 1.0f },
									{ 0.75164f , 0.60648f , 0.22648f , 1.0f },
									{ 0.50754f , 0.50754f , 0.50754f , 1.0f },

									{ 0.01f , 0.01f , 0.01f , 1.0f },
									{ 0.0f , 0.0520980392f ,  0.50980392f , 1.0f },
									{ 0.1f , 0.35f , 0.2f , 1.0f },
									{ 0.5f , 0.0f , 0.0f , 1.0f },
									{ 0.55f , 0.55f , 0.55f , 1.0f },
									{ 0.5f , 0.5f , 0.0f , 1.0f },

									{ 0.01f , 0.01f , 0.01f , 1.0f },
									{ 0.4f , 0.5f , 0.5f , 1.0f },
									{ 0.4f , 0.5f , 0.4f , 1.0f },
									{ 0.5f , 0.4f , 0.4f , 1.0f },
									{ 0.5f , 0.5f , 0.5f , 1.0f },
									{ 0.5f , 0.5f , 0.4f , 1.0f }

};

GLfloat materialSpecular[24][4] = { 
									{ 0.633f , 0.727811f , 0.633f , 1.0f },
									{ 0.316228f , 0.316228f , 0.316228f , 1.0f },
									{ 0.332741f , 0.328634f , 0.346435f , 1.0f },
									{ 0.296648f , 0.296648f , 0.296648f , 1.0f },
									{ 0.727811f , 0.626959f , 0.626959f , 1.0f },
									{ 0.297254f , 0.30829f , 0.306678f , 1.0f },

									{ 0.992157f , 0.941176f , 0.807843f , 1.0f },
									{ 0.393548f , 0.271906f , 0.166721f , 1.0f },
									{ 0.774597f , 0.774597f , 0.774597f , 1.0f },
									{ 0.256777f , 0.137622f , 0.086014f , 1.0f },
									{ 0.628281f , 0.555802f , 0.366065f , 1.0f },
									{ 0.508273f , 0.508273f , 0.508273f , 1.0f },

									{ 0.5f , 0.5f , 0.5f , 1.0f },
									{ 0.50196078f , 0.50196078f , 0.50196078f , 1.0f },
									{ 0.45f , 0.5f , 0.45f , 1.0f },
									{ 0.7f , 0.6f , 0.6f , 1.0f },
									{ 0.7f , 0.7f , 0.7f , 1.0f },
									{ 0.6f , 0.6f , 0.5f , 1.0f },

									{ 0.4f , 0.4f , 0.4f , 1.0f },
									{ 0.04f , 0.7f , 0.7f , 1.0f },
									{ 0.04f , 0.7f , 0.04f , 1.0f },
									{ 0.7f , 0.04f , 0.04f , 1.0f },
									{ 0.7f , 0.7f , 0.7f , 1.0f },
									{ 0.7f , 0.7f , 0.04f , 1.0f }
};

GLfloat materialShininess[24] = { 
									 0.6f * 128 ,
									 0.1f * 128 ,
									 0.3f * 128 ,
									 0.088f * 128 ,
									 0.6f * 128 ,
									 0.1f * 128 ,

									 0.21794872f * 128 ,
									 0.2f * 128 ,
									 0.6f * 128 ,
									 0.1f * 128 ,
									 0.4f * 128 ,
									 0.4f * 128 ,

									 0.25f * 128 ,
									 0.25f * 128 ,
									 0.25f * 128 ,
									 0.25f * 128 ,
									 0.25f * 128 ,
									 0.25f * 128 ,

									 0.078125f * 128 ,
									 0.078125f * 128 ,
									 0.078125f * 128 ,
									 0.078125f * 128 ,
									 0.078125f * 128 ,
									 0.078125f * 128 
};

GLUquadric *quadric = NULL;

GLXContext gGLXContext;

int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);
	void update(void);

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;

	//code
	CreateWindow();

	//initialize
	initialize();
	
	//Game Loop
	XEvent event;
	KeySym keysym;

	while( bDone == false )
	{
		while (XPending(gpDisplay))
		{
		    XNextEvent(gpDisplay, &event);
		    switch (event.type)
		    {
		    case MapNotify:
		        break;

		    case KeyPress:
		        keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
		        switch (keysym)
		        {
		        case XK_Escape:
		            uninitialize();
		            exit(0);
		        case XK_f:
		            if (bFullScreen == false)
		            {
		                ToggleFullScreen();
		                bFullScreen = true;
		            }
		            else
		            {
		                ToggleFullScreen();
		                bFullScreen = false;
		            }
		            break;
		         case XK_l:
		         	if(gbLightsEnable == true)
		         	{
		         		gbLightsEnable = false;
		         		glDisable(GL_LIGHTING);
		         	}
		         	else
		         	{
		         		gbLightsEnable = true;
		         		glEnable(GL_LIGHTING);
		         	}
		         	break;	  
		         case XK_x:
     				gbXAxis = true;
					gbYAxis = false;
					gbZAxis = false;
					lightPosition[0] = 0.0f;
					lightPosition[1] = 1.0f;
					lightPosition[2] = 0.0f;
					break;
		         case XK_y:
     				gbXAxis = false;
					gbYAxis = true;
					gbZAxis = false;
					lightPosition[0] = 1.0f;
					lightPosition[1] = 0.0f;
					lightPosition[2] = 0.0f;
					break;
		         case XK_z:
     				gbXAxis = false;
					gbYAxis = false;
					gbZAxis = true;
					lightPosition[0] = 0.0f;
					lightPosition[1] = 1.0f;
					lightPosition[2] = 0.0f;
					break;					       	
		        default:
		            break;
		        }
		        break;

		    case ButtonPress:
		        switch (event.xbutton.button)
		        {
		        case 1:
		            break;
		        case 2:
		            break;
		        case 3:
		            break;
		        }
		        break;

		    case MotionNotify:
		        break;

		    case ConfigureNotify:
		        winWidth = event.xconfigure.width;
		        winHeight = event.xconfigure.height;
	 		resize(winWidth, winHeight);
		        break;

		    case Expose:
		        break;

		    case DestroyNotify:
		        break;

		    case 33:
			bDone = true;
			break;
		    default:
		        break;
		    }
		}
       		display();
       		update();
	}
	return 0;
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen = 0;
	int defaultDepth = 0;
	int styleMask = 0;

	static int frameBufferAttributes[] =
	{
	GLX_RGBA,
	GLX_RED_SIZE, 8,
	GLX_GREEN_SIZE, 8,
	GLX_BLUE_SIZE, 8,
	GLX_ALPHA_SIZE, 8,
	GLX_DOUBLEBUFFER, True,
	GLX_DEPTH_SIZE, 24,
	None
	};

	gpDisplay = XOpenDisplay(NULL);
	if( gpDisplay == NULL )
	{
		printf("Error in XOpenDisplay");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;

	winAttribs.background_pixmap = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, 
				RootWindow(gpDisplay, gpXVisualInfo->screen), 
				0,
				0, 
				giWindowWidth, 
				giWindowHeight, 
				0, 
				gpXVisualInfo->depth, 
				InputOutput, 
				gpXVisualInfo->visual, 
				styleMask, 
				&winAttribs) ; 
	if(!gWindow)
	{
		printf("Error:: Failed to Create Window.\nExitting Now...");
		uninitialize();
		exit(1);
	}

	XStoreName( gpDisplay, gWindow, "3 Lights");

	Atom wmDelete = XInternAtom( gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &wmDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), False, StructureNotifyMask, &xev);	
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	quadric = gluNewQuadric();

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmbiet);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, lightModelLocalViewer);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

	glEnable(GL_LIGHT0);

	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	resize(giWindowWidth, giWindowHeight);
}

void DrawSphere()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric, SPHERE_RADIUS, 100, 100);
}

void DrawGridOfSpheres()
{
	int i = 0;
	int j = 0;
	int index = 0;
	float startX = 3 * SPHERE_RADIUS + 1.5*SPHERE_GAP;
	float startY = 5 * SPHERE_RADIUS + 2.5*SPHERE_GAP;
	float x = -startX;
	float y = startY;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 6; j++)
		{
			glPushMatrix();
				glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient[index]);
				glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse[index]);
				glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular[index]);
				glMaterialf(GL_FRONT, GL_SHININESS, materialShininess[index]);
				glTranslatef(x, y, 0.0f);
				DrawSphere();
			glPopMatrix();
			index++;
			y = y - 2 * SPHERE_RADIUS - SPHERE_GAP;
		}
		x = x + 2 * SPHERE_RADIUS + SPHERE_GAP;
		y = startY;
	}
}

void ApplyLights()
{
	glPushMatrix();
		if (gbXAxis == true)
		{
			glRotatef(angleLight, 1.0f, 0.0f, 0.0f);
		}
		else if (gbYAxis == true)
		{
			glRotatef(angleLight, 0.0f, 1.0f, 0.0f);
		}
		else if (gbZAxis == true)
		{
			glRotatef(angleLight, 0.0f, 0.0f, 1.0f);
		}
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glPopMatrix();
}

void update(void)
{
	if (angleLight > 360)
	{
		angleLight = 1.0f;
	}
	angleLight += ANGLE_INCREMENT;
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//View Transformations
	glTranslatef(0.0f, 0.0f, -4.0f);
	ApplyLights();
	DrawGridOfSpheres();

	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize() 
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if (currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
	glXMakeCurrent(gpDisplay, 0, 0);
	}

	if (gGLXContext)
	{
	glXDestroyContext(gpDisplay, gGLXContext);
	}

	if( gWindow )
	{
		XDestroyWindow(gpDisplay,gWindow);
	
	}		

	if( gColormap ) 
	{
		XFreeColormap(gpDisplay, gColormap);
	}		

	if( gpXVisualInfo )
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}	

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}	
}
