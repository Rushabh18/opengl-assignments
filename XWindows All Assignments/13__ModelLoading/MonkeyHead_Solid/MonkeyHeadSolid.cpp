#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include <vector>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE 256
#define S_EQUAL 0

#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3

#define MONKEYHEAD_X_TRANSLATE 0.0f
#define MONKEYHEAD_Y_TRANSLATE -0.0f
#define MONKEYHEAD_Z_TRANSLATE -5.0f

#define MONKEYHEAD_X_SCALEFACTOR 1.5f
#define MONKEYHEAD_Y_SCALEFACTOR 1.5f
#define MONKEYHEAD_Z_SCALEFACTOR 1.5f

#define START_ANGLE_POS 0.0f
#define END_ANGLE_POS 360.0f
#define MONKEYHEAD_ANGLE_INCREMENT 0.5f

bool bFullScreen = false;
bool gbLightsEnable  = false;

Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;

int giWindowWidth = 600;
int giWindowHeight = 600;

GLXContext gGLXContext;

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 1.0f, 0.0f };

GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialShininess = 50.0f;

GLfloat g_rotate;

std::vector<std::vector<float> > g_vertices;

std::vector<std::vector<float> > g_texture;

std::vector<std::vector<float> > g_normals;

std::vector<std::vector<int> >g_face_tri, g_face_texture, g_face_normals;

FILE *g_fp_meshfile = NULL;

FILE *g_fp_logfile = NULL;

char line[BUFFER_SIZE] = { '\0' };

int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void initialize(void);
	void ToggleFullScreen(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);
	void update(void);

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
	bool bDone = false;

	//code
	CreateWindow();

	//initialize
	initialize();
	
	//Game Loop
	XEvent event;
	KeySym keysym;

	while( bDone == false )
	{
		while (XPending(gpDisplay))
		{
		    XNextEvent(gpDisplay, &event);
		    switch (event.type)
		    {
		    case MapNotify:
		        break;

		    case KeyPress:
		        keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
		        switch (keysym)
		        {
		        case XK_Escape:
		            uninitialize();
		            exit(0);
		        case XK_f:
		            if (bFullScreen == false)
		            {
		                ToggleFullScreen();
		                bFullScreen = true;
		            }
		            else
		            {
		                ToggleFullScreen();
		                bFullScreen = false;
		            }
		            break;
		         case XK_l:
		         	if(gbLightsEnable == true)
		         	{
		         		gbLightsEnable = false;
		         		glDisable(GL_LIGHTING);
		         	}
		         	else
		         	{
		         		gbLightsEnable = true;
		         		glEnable(GL_LIGHTING);
		         	}
		         	break;	  

		        default:
		            break;
		        }
		        break;

		    case ButtonPress:
		        switch (event.xbutton.button)
		        {
		        case 1:
		            break;
		        case 2:
		            break;
		        case 3:
		            break;
		        }
		        break;

		    case MotionNotify:
		        break;

		    case ConfigureNotify:
		        winWidth = event.xconfigure.width;
		        winHeight = event.xconfigure.height;
	 		resize(winWidth, winHeight);
		        break;

		    case Expose:
		        break;

		    case DestroyNotify:
		        break;

		    case 33:
			bDone = true;
			break;
		    default:
		        break;
		    }
		}
       		display();
       		update();
	}
	return 0;
}

void CreateWindow(void)
{
	//function prototypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen = 0;
	int defaultDepth = 0;
	int styleMask = 0;

	static int frameBufferAttributes[] =
	{
	GLX_RGBA,
	GLX_RED_SIZE, 8,
	GLX_GREEN_SIZE, 8,
	GLX_BLUE_SIZE, 8,
	GLX_ALPHA_SIZE, 8,
	GLX_DOUBLEBUFFER, True,
	GLX_DEPTH_SIZE, 24,
	None
	};

	gpDisplay = XOpenDisplay(NULL);
	if( gpDisplay == NULL )
	{
		printf("Error in XOpenDisplay");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;

	winAttribs.background_pixmap = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, 
				RootWindow(gpDisplay, gpXVisualInfo->screen), 
				0,
				0, 
				giWindowWidth, 
				giWindowHeight, 
				0, 
				gpXVisualInfo->depth, 
				InputOutput, 
				gpXVisualInfo->visual, 
				styleMask, 
				&winAttribs) ; 
	if(!gWindow)
	{
		printf("Error:: Failed to Create Window.\nExitting Now...");
		uninitialize();
		exit(1);
	}

	XStoreName( gpDisplay, gWindow, "3 Lights");

	Atom wmDelete = XInternAtom( gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &wmDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

void ToggleFullScreen()
{
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay, RootWindow(gpDisplay,gpXVisualInfo->screen), False, StructureNotifyMask, &xev);	
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void LoadMeshData(void);

	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, materialShininess);

	glEnable(GL_LIGHT0);
	
	LoadMeshData();

	resize(giWindowWidth, giWindowHeight);
}

void update(void)
{
	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;
	if (g_rotate >= END_ANGLE_POS)
	{
		g_rotate = START_ANGLE_POS;
	}
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//View Transformations
	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALEFACTOR, MONKEYHEAD_Y_SCALEFACTOR, MONKEYHEAD_Z_SCALEFACTOR);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	for (int i = 0; i != g_face_tri.size(); ++i)
	{
		int vn = g_face_normals[i][0] - 1;
		glNormal3f(g_normals[vn][0], g_normals[vn][1], g_normals[vn][2]);
		glBegin(GL_TRIANGLES);
		for (int j = 0; j != g_face_tri[i].size(); j++)
		{
			int vi = g_face_tri[i][j] - 1;
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]);
		}
		glEnd();
	}
	glXSwapBuffers(gpDisplay, gWindow);
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void LoadMeshData(void)
{
	void uninitialize(void);

	g_fp_meshfile = fopen("MonkeyHead.OBJ", "r");
	g_fp_logfile = fopen("MonkeyHead.LOG", "w");
	if (!g_fp_meshfile)
		uninitialize();

	char sep_space[2] = " ";
	char sep_fslash[2] = "/";

	char *first_token = NULL;
	char *token = NULL;

	char *face_tokens[NR_FACE_TOKENS];
	int nr_token;
	char *token_vertex_index = NULL;
	char *token_texture_index = NULL;
	char *token_normal_index = NULL;
	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		first_token = strtok(line, sep_space);

		if (strcmp(first_token, "v") == S_EQUAL)
		{
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for (int i = 0; i != NR_POINT_COORDS; i++)
			{
				vec_point_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_vertices.push_back(vec_point_coord);
		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_POINT_COORDS);

			for (int i = 0; i != NR_POINT_COORDS - 1; i++)
			{
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_texture.push_back(vec_texture_coord);
		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_POINT_COORDS);

			for (int i = 0; i != NR_POINT_COORDS; i++)
			{
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_normals.push_back(vec_normal_coord);
		}
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);
			nr_token = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_token] = token;
				nr_token++;
			}
			for (int i = 0; i != NR_FACE_TOKENS; i++)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}
			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		memset((void*)line, (int)'\0', BUFFER_SIZE);
	}
	fclose(g_fp_meshfile);

	fprintf(g_fp_logfile, "g_vertices:%llu g_texture:%llu g_normals:%llu g_face_tri:%llu", g_vertices.size(),
		g_texture.size(), g_normals.size(), g_face_tri.size());
}

void uninitialize() 
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();

	if (currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
	glXMakeCurrent(gpDisplay, 0, 0);
	}

	if (gGLXContext)
	{
	glXDestroyContext(gpDisplay, gGLXContext);
	}

	if( gWindow )
	{
		XDestroyWindow(gpDisplay,gWindow);
	
	}		

	if( gColormap ) 
	{
		XFreeColormap(gpDisplay, gColormap);
	}		

	if( gpXVisualInfo )
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}	

	fclose(g_fp_logfile);
	g_fp_logfile = NULL;

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}	
}
