package com.astromedicomp.animation2d;

public class GLESMacros
{
	public static final int VDG_ATTRIBUTE_POSITION = 0;
	public static final int VDG_ATTRIBUTE_COLOR = 1;
	public static final int VDG_ATTRIBUTE_NORMAL = 2;
	public static final int VDG_ATTRIBUTE_TEXTURE0 = 3;
}