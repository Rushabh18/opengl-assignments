package com.astromedicomp.windowMyView;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.Gravity;
import android.graphics.Color;
import android.widget.TextView;
import android.content.pm.ActivityInfo;
 
public class MainActivity extends Activity {

	private MyView myView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		// this is done to get rid of the ActionBar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//this is done to make fullscreen
		this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));
		
		myView = new MyView(this);
		TextView myTextView = new TextView(this);
		
		myTextView.setText("HeLlO GaLaXy ;)");
		myTextView.setTextSize(60);
		myTextView.setTextColor(Color.GREEN);
		myTextView.setGravity(Gravity.CENTER);
        
		setContentView(myView);
		// setContentView(R.layout.activity_main);

	}
}
