package com.astromedicomp.windowMyView;

import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.widget.TextView;
 
public class MyView extends TextView {
		
	MyView(Context context)
	{
		super(context);
		
		setText("HeLLo GalaXY :D");
		setGravity(Gravity.CENTER);
		setTextColor(Color.GREEN);
		setTextSize(60);		
	}
}
