package com.astromedicomp.twolightspyramid;

import android.content.Context;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.opengl.GLSurfaceView;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import java.lang.Math;

import android.opengl.Matrix; //for Matrix Math.

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
		
	private final Context context;
	
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	
	private int[] vao_pyramid = new int[1];
	private int[] vbo_pyramid_position = new int[1];
	private int[] vbo_pyramid_normal = new int [1];
	
	private int model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
	
	private int[] La_uniform = new int[2];
	private int[] Ld_uniform = new int[2];
	private int[] Ls_uniform  = new int[2] ;
	private int[] light_position_uniform  = new int[2];
	
	private int Ka_uniform;
	private int Kd_uniform;
	private int Ks_uniform;
	private int material_shininess_uniform;

	private int double_tap_uniform;
	
	private float lightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float light0Diffuse[] = {1.0f, 0.0f, 0.0f, 0.0f};
	private float light0Specular[] = {1.0f, 0.0f, 0.0f, 0.0f};
	private float light0Position[] = {100.0f, 0.0f, 100.0f, 1.0f};

	private float light1Diffuse[] = {0.0f, 0.0f, 1.0f, 0.0f};
	private float light1Specular[] = {0.0f, 0.0f, 1.0f, 0.0f};
	private float light1Position[] = {-100.0f, 0.0f, 100.0f, 1.0f};

	private float material_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float material_diffuse[] = {1.0f ,1.0f, 1.0f, 1.0f};
	private float material_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float material_shininess = 50.0f;
		
	private float perspectiveProjectionMatrix[] = new float[16];
	
	private float gAngle = 0.0f;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
		
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version =" + version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override 
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
			resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
			draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("VDG: " + "Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("VDG: " + "Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: " + "Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: " + "Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		System.out.println("VDG: " + "in initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+
		"in vec4 vPosition;"						+
		"in vec3 vNormal;"							+
		"uniform mat4 u_model_matrix;"				+
		"uniform mat4 u_view_matrix;"				+
		"uniform mat4 u_projection_matrix;"			+
		"uniform mediump int u_lighting_enabled;"	+
		"uniform vec3 u_La0;"						+
		"uniform vec3 u_Ld0;"						+
		"uniform vec3 u_Ls0;"						+
		"uniform vec4 u_light0_position;"			+
		"uniform vec3 u_La1;"						+
		"uniform vec3 u_Ld1;"						+
		"uniform vec3 u_Ls1;"						+
		"uniform vec4 u_light1_position;"			+
		"uniform vec3 u_Ka;"						+
		"uniform vec3 u_Kd;"						+
		"uniform vec3 u_Ks;"						+
		"uniform float u_material_shininess;"		+
		"out vec3 phong_ads_color;"					+
		"void main(void)"							+	
		"{"											+
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"					+
		"vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"	+
		"vec3 light0_direction = normalize(vec3(u_light0_position) - eye_coordinates.xyz);"		+
		"float tn_dot_ld0 = max(dot(transformed_normals, light0_direction), 0.0);"				+
		"vec3 ambient0 = u_La0 * u_Ka;"															+
		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;"											+
		"vec3 reflection0_vector = reflect(-light0_direction, transformed_normals);"			+
		"vec3 viewer_vector = normalize(-eye_coordinates.xyz);"									+
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection0_vector, viewer_vector), 0.0), u_material_shininess);"+
		"vec3 phong_ads_color0 = ambient0 + diffuse0 + specular0;"									+
		"vec3 light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);"		+
		"float tn_dot_ld1 = max(dot(transformed_normals, light1_direction), 0.0);"				+
		"vec3 ambient1 = u_La1 * u_Ka;"															+
		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"											+
		"vec3 reflection1_vector = reflect(-light1_direction, transformed_normals);"			+
		"viewer_vector = normalize(-eye_coordinates.xyz);"										+
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection1_vector, viewer_vector), 0.0), u_material_shininess);"+
		"vec3 phong_ads_color1 = ambient1 + diffuse1 + specular1;"									+
		"phong_ads_color = phong_ads_color0 + phong_ads_color1;"				+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"		+
		"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG:: Vertex Shader Compilation Log = "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("VDG: " + "vertex shader done");
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+
		"precision highp float;"					+
		"in vec3 phong_ads_color;"					+
		"out vec4 FragColor;"						+
		"void main(void)"							+
		"{"											+
		"FragColor = vec4(phong_ads_color, 1.0);"	+
		"}"											
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0]= 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG:: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}	
		System.out.println("VDG: " + "fragment shader done");
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject , GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG:: Shader Program Link Log:: "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("VDG: " + "program link done");
		

		model_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		view_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		projection_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

		La_uniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_La0");
		Ld_uniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld0");
		Ls_uniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls0");
		light_position_uniform[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_light0_position");

		La_uniform[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_La1");
		Ld_uniform[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld1");
		Ls_uniform[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls1");
		light_position_uniform[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_light1_position");

		Ka_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
		Kd_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
		Ks_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
		material_shininess_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
	

	float[] pyramidVertices =
	{
		0.0f, 1.0f, 0.0f, //right
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f, //front
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f, //left
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f, //back
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f
	};

	float[] pyramidNormals =
	{
		0.894427f, 0.447214f, 0.0f, //right
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, 0.894427f, //front
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		-0.894427f, 0.447214f, 0.0f, //left
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, -0.894427f, //back
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

 
	};
					
		GLES32.glGenVertexArrays(1, vao_pyramid, 0);
		GLES32.glBindVertexArray(vao_pyramid[0]);
		
			GLES32.glGenBuffers(1, vbo_pyramid_position, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_position[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
				verticesBuffer.put(pyramidVertices );
				verticesBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									pyramidVertices.length * 4, 
									verticesBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_POSITION,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_POSITION);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
			GLES32.glGenBuffers(1, vbo_pyramid_normal, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_pyramid_normal[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidNormals.length * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer normalBuffer = byteBuffer.asFloatBuffer();
				normalBuffer.put(pyramidNormals);
				normalBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									pyramidNormals.length * 4, 
									normalBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		System.out.println("VDG: " + "vao done");

		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
//		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	
		System.out.println("VDG: " + "initialize done");
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/(float)height, 0.1f, 100.0f);			
	}
	
	public void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		
			GLES32.glUniform1i(double_tap_uniform, 1);
					
			GLES32.glUniform3fv(La_uniform[0], 1, lightAmbient, 0);
			GLES32.glUniform3fv(Ld_uniform[0], 1, light0Diffuse, 0);
			GLES32.glUniform3fv(Ls_uniform[0], 1, light0Specular, 0);
			GLES32.glUniform4fv(light_position_uniform[0], 1, light0Position, 0);

			GLES32.glUniform3fv(La_uniform[1], 1, lightAmbient, 0);
			GLES32.glUniform3fv(Ld_uniform[1], 1, light1Diffuse, 0);
			GLES32.glUniform3fv(Ls_uniform[1], 1, light1Specular, 0);
			GLES32.glUniform4fv(light_position_uniform[1], 1, light1Position, 0);

			GLES32.glUniform3fv(Ka_uniform, 1, material_ambient, 0);
			GLES32.glUniform3fv(Kd_uniform, 1, material_diffuse, 0);
			GLES32.glUniform3fv(Ks_uniform, 1, material_specular, 0);
			GLES32.glUniform1f(material_shininess_uniform, material_shininess);

			float modelMatrix[] = new float[16];
			float viewMatrix[] = new float[16];
			float rotationMatrix[] = new float[16];
	
			Matrix.setIdentityM(modelMatrix, 0);
			Matrix.setIdentityM(viewMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -6.0f);
			Matrix.setRotateM(rotationMatrix, 0, gAngle, 0.0f, 1.0f, 0.0f);						
			Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
			
			GLES32.glUniformMatrix4fv(model_matrix_uniform, 1, false, modelMatrix, 0);
			GLES32.glUniformMatrix4fv(view_matrix_uniform, 1, false, viewMatrix, 0);
			GLES32.glUniformMatrix4fv(projection_matrix_uniform, 1, false, perspectiveProjectionMatrix, 0);
				
			GLES32.glBindVertexArray(vao_pyramid[0]);
			
				GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
			
			GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		gAngle += 0.3f;
		if (gAngle > 360)
		{
			gAngle = 0.0f;
		}
		
		requestRender();
	}
	
	public void uninitialize()
	{
		if(vao_pyramid[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_pyramid, 0);
			vao_pyramid[0] = 0;
		}
		
		if(vbo_pyramid_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_position, 0);
			vbo_pyramid_position[0] = 0;
		}
		
		if(vbo_pyramid_normal[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_pyramid_normal, 0);
			vbo_pyramid_normal[0] = 0;
		}
	
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}
		
		if(shaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
			
		
	}
}
