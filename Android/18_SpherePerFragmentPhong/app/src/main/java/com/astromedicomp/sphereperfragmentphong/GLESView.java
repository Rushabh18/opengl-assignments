package com.astromedicomp.sphereperfragmentphong;

import android.content.Context;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.opengl.GLSurfaceView;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix; //for Matrix Math.

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
		
	private final Context context;
	
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	
	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_normal = new int [1];
	private int[] vbo_sphere_element = new int [1];
	
	private int model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
	
	private int La_uniform;
	private int Ld_uniform;
	private int Ls_uniform;
	private int light_position_uniform;
	
	private int Ka_uniform;
	private int Kd_uniform;
	private int Ks_uniform;
	private int material_shininess_uniform;

	private int double_tap_uniform;
	
	private int numVertices;
	private int numElements;
	
	private float lightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float lightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float lightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float lightPosition[] = {100.0f, 100.0f, 100.0f, 1.0f};
	
	private float material_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float material_diffuse[] = {1.0f ,1.0f, 1.0f, 1.0f};
	private float material_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float material_shininess = 50.0f;
		
	private float perspectiveProjectionMatrix[] = new float[16];
	
	private float angleCube = 0.0f;
	
	private int singleTap  = 0;
	private int doubleTap = 0;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
		
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version =" + version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override 
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
			resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
			draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		System.out.println("VDG: " + "Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;
		if(singleTap > 1)
			singleTap = 0;
		System.out.println("VDG: " + "Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: " + "Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: " + "Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		System.out.println("VDG: " + "in initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+		
		"in vec4 vPosition;"						+
		"in vec3 vNormal;"							+
		"uniform mat4 u_model_matrix;"				+
		"uniform mat4 u_view_matrix;"				+
		"uniform mat4 u_projection_matrix;"			+
		"uniform mediump int u_lighting_enabled;"	+
		"uniform vec4 u_light_position;"			+
		"out vec3 transformed_normals;"				+
		"out vec3 light_direction;"					+
		"out vec3 viewer_vector;"					+
		"void main(void)"							+
		"{"											+
		"if(u_lighting_enabled == 1)"				+
		"{"											+
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"				+
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"				+
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;"					+
		"viewer_vector = -eye_coordinates.xyz;"												+
		"}"																					+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"	+
		"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG:: Vertex Shader Compilation Log = "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("VDG: " + "vertex shader done");
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+
		"precision highp float;"					+
		"in vec3 transformed_normals;"				+
		"in vec3 light_direction;"					+
		"in vec3 viewer_vector;"					+
		"out vec4 FragColor;"						+
		"uniform vec3 u_La;"						+
		"uniform vec3 u_Ld;"						+
		"uniform vec3 u_Ls;"						+
		"uniform vec3 u_Ka;"						+
		"uniform vec3 u_Kd;"						+
		"uniform vec3 u_Ks;"						+
		"uniform float u_material_shininess;"		+
		"uniform int u_lighting_enabled;"			+
		"void main(void)"							+
		"{"											+
		"vec3 phong_ads_color;"						+
		"if(u_lighting_enabled == 1)"				+
		"{"											+
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"					+
		"vec3 normalize_light_direction = normalize(light_direction);"							+
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"								+
		"vec3 ambient = u_La * u_Ka;"															+
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalize_light_direction), 0.0);"				+
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"												+
		"vec3 reflection_vector = reflect(-normalize_light_direction, normalized_transformed_normals);"				+
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
		"phong_ads_color = ambient + diffuse + specular;"										+
		"}"																						+
		"else"																					+
		"{"																						+
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"												+
		"}"																						+
		"FragColor = vec4(phong_ads_color, 1.0);"												+
		"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0]= 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG:: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}	
		System.out.println("VDG: " + "fragment shader done");
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject , GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG:: Shader Program Link Log:: "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("VDG: " + "program link done");
		

		model_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		view_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		projection_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

		double_tap_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");

		La_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
		Ld_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
		Ls_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
		light_position_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");

		Ka_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
		Kd_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
		Ks_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
		material_shininess_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
					
		Sphere sphere = new Sphere();
		float sphere_vertices[] = new float[1146];
		float sphere_normals[] = new float[1146];
		float sphere_textures[] = new float[764];
		short sphere_elements[] = new short[2280];

		System.out.println("VDG: " + "vertices and elements retrieval start");
		
		sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
		numVertices = sphere.getNumberOfSphereVertices();
		numElements = sphere.getNumberOfSphereElements();
		
		System.out.println("VDG: " + "vertices and elements retrieval done");
	
		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);
		
			GLES32.glGenBuffers(1, vbo_sphere_position, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
				verticesBuffer.put(sphere_vertices);
				verticesBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									sphere_vertices.length * 4, 
									verticesBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_POSITION,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_POSITION);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
			GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer normalBuffer = byteBuffer.asFloatBuffer();
				normalBuffer.put(sphere_normals);
				normalBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									sphere_normals.length * 4, 
									normalBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

			GLES32.glGenBuffers(1, vbo_sphere_element, 0);
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
				byteBuffer.order(ByteOrder.nativeOrder());
				ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
				elementsBuffer.put(sphere_elements);
				elementsBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
									sphere_elements.length * 2,
									elementsBuffer,
									GLES32.GL_STATIC_DRAW);
			}
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		System.out.println("VDG: " + "vao done");

		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
//		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		singleTap = 0;
		doubleTap = 0;
		
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	
		System.out.println("VDG: " + "initialize done");
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/(float)height, 0.1f, 100.0f);			
	}
	
	public void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		
			if(doubleTap == 1)
			{
				GLES32.glUniform1i(double_tap_uniform, 1);
				
				GLES32.glUniform3fv(La_uniform, 1, lightAmbient, 0);
				GLES32.glUniform3fv(Ld_uniform, 1, lightDiffuse, 0);
				GLES32.glUniform3fv(Ls_uniform, 1, lightSpecular, 0);
				GLES32.glUniform4fv(light_position_uniform, 1, lightPosition, 0);

				GLES32.glUniform3fv(Ka_uniform, 1, material_ambient, 0);
				GLES32.glUniform3fv(Kd_uniform, 1, material_diffuse, 0);
				GLES32.glUniform3fv(Ks_uniform, 1, material_specular, 0);
				GLES32.glUniform1f(material_shininess_uniform, material_shininess);
			}
			else
			{
				GLES32.glUniform1i(double_tap_uniform, 0);
			}
		
			float modelMatrix[] = new float[16];
			float viewMatrix[] = new float[16];
				
			Matrix.setIdentityM(modelMatrix, 0);
			Matrix.setIdentityM(viewMatrix, 0);
			
			Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -2.0f);
									
			GLES32.glUniformMatrix4fv(model_matrix_uniform, 1, false, modelMatrix, 0);
			GLES32.glUniformMatrix4fv(view_matrix_uniform, 1, false, viewMatrix, 0);
			GLES32.glUniformMatrix4fv(projection_matrix_uniform, 1, false, perspectiveProjectionMatrix, 0);
				
			GLES32.glBindVertexArray(vao_sphere[0]);
			
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
			
			GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		if(singleTap == 1)
		{
			angleCube = angleCube - 0.75f;
		}
		
		requestRender();
	}
	
	public void uninitialize()
	{
		if(vao_sphere[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
			vao_sphere[0] = 0;
		}
		
		if(vbo_sphere_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
			vbo_sphere_position[0] = 0;
		}
		
		if(vbo_sphere_normal[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
			vbo_sphere_normal[0] = 0;
		}

		if(vbo_sphere_element[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
			vbo_sphere_element[0] = 0;
		}
		
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}
		
		if(shaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
			
		
	}
}
