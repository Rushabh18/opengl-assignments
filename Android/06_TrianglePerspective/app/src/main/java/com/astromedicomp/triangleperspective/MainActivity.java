package com.astromedicomp.triangleperspective;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
 
public class MainActivity extends Activity {
	
	private GLESView glesView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		// this is done to get rid of the ActionBar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//this is done to make fullscreen
		this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				
		glesView = new GLESView(this);
        
		setContentView(glesView);
		// setContentView(R.layout.activity_main);

	}
}
