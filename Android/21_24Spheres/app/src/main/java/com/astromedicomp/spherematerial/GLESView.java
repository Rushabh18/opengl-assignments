package com.astromedicomp.spherematerial;

import android.content.Context;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.opengl.GLSurfaceView;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.lang.Math;

//for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix; //for Matrix Math.

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
		
	private final Context context;
	
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int giWindowHeight = 0;
	private int giWindowWidth = 0;
	
	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_normal = new int [1];
	private int[] vbo_sphere_element = new int [1];
	
	private int model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;
	
	private int La_uniform;
	private int Ld_uniform;
	private int Ls_uniform;
	private int light_position_uniform;
	
	private int Ka_uniform;
	private int Kd_uniform;
	private int Ks_uniform;
	private int material_shininess_uniform;

	private int double_tap_uniform;
	
	private int numVertices;
	private int numElements;
	
	private int giSingleTap = 0;
	
	private double gAngle = 0;
	
	private float lightAmbient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float lightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float lightSpecular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float lightPosition[] = {100.0f, 100.0f, 100.0f, 1.0f};
	
	private float[][] materialAmbient = {
		{ 0.0215f, 0.1745f, 0.0215f, 1.0f },
		{ 0.135f , 0.225f , 0.1575f, 1.0f },
		{ 0.05375f , 0.05f , 0.06625f, 1.0f },
		{ 0.25f , 0.20725f , 0.20725f, 1.0f },
		{ 0.1745f , 0.01175f , 0.01175f, 1.0f },
		{ 0.1f , 0.18725f , 0.1745f, 1.0f },

		{ 0.329412f , 0.223529f , 0.027451f, 1.0f },
		{ 0.2125f , 0.1275f , 0.054f, 1.0f },
		{ 0.25f , 0.25f , 0.25f, 1.0f },
		{ 0.19125f , 0.0735f , 0.0225f, 1.0f },
		{ 0.24725f , 0.1995f , 0.0745f, 1.0f },
		{ 0.19225f , 0.19225f , 0.19225f, 1.0f },

		{ 0.0f , 0.0f , 0.0f, 1.0f },
		{ 0.0f , 0.1f , 0.06f, 1.0f },
		{ 0.0f , 0.0f , 0.0f, 1.0f },
		{ 0.0f , 0.0f , 0.0f, 1.0f },
		{ 0.0f , 0.0f , 0.0f, 1.0f },
		{ 0.0f , 0.0f , 0.0f, 1.0f },

		{ 0.02f , 0.02f , 0.02f, 1.0f },
		{ 0.0f , 0.05f , 0.05f, 1.0f },
		{ 0.0f , 0.05f , 0.0f, 1.0f },
		{ 0.05f , 0.0f , 0.0f, 1.0f },
		{ 0.05f , 0.05f , 0.05f, 1.0f },
		{ 0.05f , 0.05f , 0.0f, 1.0f }
	};

	private float[][] materialDiffuse = {
		{ 0.07568f, 0.61424f, 0.07568f, 1.0f },
		{ 0.54f , 0.89f , 0.63f , 1.0f },
		{ 0.18275f , 0.17f , 0.22525f , 1.0f },
		{ 1.0f , 0.829f , 0.829f , 1.0f },
		{ 0.61424f , 0.04136f , 0.04136f , 1.0f },
		{ 0.396f , 0.74151f , 0.69102f , 1.0f },

		{ 0.780392f , 0.568627f , 0.113725f , 1.0f },
		{ 0.714f , 0.4284f , 0.18144f , 1.0f },
		{ 0.4f , 0.4f , 0.4f , 1.0f },
		{ 0.7038f , 0.27048f , 0.0828f , 1.0f },
		{ 0.75164f , 0.60648f , 0.22648f , 1.0f },
		{ 0.50754f , 0.50754f , 0.50754f , 1.0f },

		{ 0.01f , 0.01f , 0.01f , 1.0f },
		{ 0.0f , 0.0520980392f ,  0.50980392f , 1.0f },
		{ 0.1f , 0.35f , 0.2f , 1.0f },
		{ 0.5f , 0.0f , 0.0f , 1.0f },
		{ 0.55f , 0.55f , 0.55f , 1.0f },
		{ 0.5f , 0.5f , 0.0f , 1.0f },

		{ 0.01f , 0.01f , 0.01f , 1.0f },
		{ 0.4f , 0.5f , 0.5f , 1.0f },
		{ 0.4f , 0.5f , 0.4f , 1.0f },
		{ 0.5f , 0.4f , 0.4f , 1.0f },
		{ 0.5f , 0.5f , 0.5f , 1.0f },
		{ 0.5f , 0.5f , 0.4f , 1.0f }

	};

	private float[][] materialSpecular = {
		{ 0.633f , 0.727811f , 0.633f , 1.0f },
		{ 0.316228f , 0.316228f , 0.316228f , 1.0f },
		{ 0.332741f , 0.328634f , 0.346435f , 1.0f },
		{ 0.296648f , 0.296648f , 0.296648f , 1.0f },
		{ 0.727811f , 0.626959f , 0.626959f , 1.0f },
		{ 0.297254f , 0.30829f , 0.306678f , 1.0f },

		{ 0.992157f , 0.941176f , 0.807843f , 1.0f },
		{ 0.393548f , 0.271906f , 0.166721f , 1.0f },
		{ 0.774597f , 0.774597f , 0.774597f , 1.0f },
		{ 0.256777f , 0.137622f , 0.086014f , 1.0f },
		{ 0.628281f , 0.555802f , 0.366065f , 1.0f },
		{ 0.508273f , 0.508273f , 0.508273f , 1.0f },

		{ 0.5f , 0.5f , 0.5f , 1.0f },
		{ 0.50196078f , 0.50196078f , 0.50196078f , 1.0f },
		{ 0.45f , 0.5f , 0.45f , 1.0f },
		{ 0.7f , 0.6f , 0.6f , 1.0f },
		{ 0.7f , 0.7f , 0.7f , 1.0f },
		{ 0.6f , 0.6f , 0.5f , 1.0f },

		{ 0.4f , 0.4f , 0.4f , 1.0f },
		{ 0.04f , 0.7f , 0.7f , 1.0f },
		{ 0.04f , 0.7f , 0.04f , 1.0f },
		{ 0.7f , 0.04f , 0.04f , 1.0f },
		{ 0.7f , 0.7f , 0.7f , 1.0f },
		{ 0.7f , 0.7f , 0.04f , 1.0f }
	};

	private float[] materialShininess = {
		 0.6f * 128 ,
		 0.1f * 128 ,
		 0.3f * 128 ,
		 0.088f * 128 ,
		 0.6f * 128 ,
		 0.1f * 128 ,

		 0.21794872f * 128 ,
		 0.2f * 128 ,
		 0.6f * 128 ,
		 0.1f * 128 ,
		 0.4f * 128 ,
		 0.4f * 128 ,

		 0.25f * 128 ,
		 0.25f * 128 ,
		 0.25f * 128 ,
		 0.25f * 128 ,
		 0.25f * 128 ,
		 0.25f * 128 ,

		 0.078125f * 128 ,
		 0.078125f * 128 ,
		 0.078125f * 128 ,
		 0.078125f * 128 ,
		 0.078125f * 128 ,
		 0.078125f * 128 
	};
		
	private float perspectiveProjectionMatrix[] = new float[16];
	
	private float angleCube = 0.0f;
	
	private int doubleTap = 0;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
		
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version =" + version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override 
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
			resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
			draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		System.out.println("VDG: " + "Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		giSingleTap++;
		if(giSingleTap > 3)
			giSingleTap = 0;
		System.out.println("VDG: " + "Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: " + "Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: " + "Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		System.out.println("VDG: " + "in initialize");

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+		
		"in vec4 vPosition;"						+
		"in vec3 vNormal;"							+
		"uniform mat4 u_model_matrix;"				+
		"uniform mat4 u_view_matrix;"				+
		"uniform mat4 u_projection_matrix;"			+
		"uniform mediump int u_lighting_enabled;"	+
		"uniform vec4 u_light_position;"			+
		"out vec3 transformed_normals;"				+
		"out vec3 light_direction;"					+
		"out vec3 viewer_vector;"					+
		"void main(void)"							+
		"{"											+
		"if(u_lighting_enabled == 1)"				+
		"{"											+
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"				+
		"transformed_normals = mat3(u_view_matrix * u_model_matrix) * vNormal;"				+
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;"					+
		"viewer_vector = -eye_coordinates.xyz;"												+
		"}"																					+
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"	+
		"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG:: Vertex Shader Compilation Log = "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("VDG: " + "vertex shader done");
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+
		"precision highp float;"					+
		"in vec3 transformed_normals;"				+
		"in vec3 light_direction;"					+
		"in vec3 viewer_vector;"					+
		"out vec4 FragColor;"						+
		"uniform vec3 u_La;"						+
		"uniform vec3 u_Ld;"						+
		"uniform vec3 u_Ls;"						+
		"uniform vec3 u_Ka;"						+
		"uniform vec3 u_Kd;"						+
		"uniform vec3 u_Ks;"						+
		"uniform float u_material_shininess;"		+
		"uniform int u_lighting_enabled;"			+
		"void main(void)"							+
		"{"											+
		"vec3 phong_ads_color;"						+
		"if(u_lighting_enabled == 1)"				+
		"{"											+
		"vec3 normalized_transformed_normals = normalize(transformed_normals);"					+
		"vec3 normalize_light_direction = normalize(light_direction);"							+
		"vec3 normalized_viewer_vector = normalize(viewer_vector);"								+
		"vec3 ambient = u_La * u_Ka;"															+
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalize_light_direction), 0.0);"				+
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;"												+
		"vec3 reflection_vector = reflect(-normalize_light_direction, normalized_transformed_normals);"				+
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);"+
		"phong_ads_color = ambient + diffuse + specular;"										+
		"}"																						+
		"else"																					+
		"{"																						+
		"phong_ads_color = vec3(1.0, 1.0, 1.0);"												+
		"}"																						+
		"FragColor = vec4(phong_ads_color, 1.0);"												+
		"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0]= 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG:: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}	
		System.out.println("VDG: " + "fragment shader done");
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject , GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG:: Shader Program Link Log:: "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("VDG: " + "program link done");
		

		model_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
		view_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
		projection_matrix_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

		double_tap_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lighting_enabled");

		La_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La");
		Ld_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
		Ls_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls");
		light_position_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");

		Ka_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
		Kd_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
		Ks_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
		material_shininess_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");
					
		Sphere sphere = new Sphere();
		float sphere_vertices[] = new float[1146];
		float sphere_normals[] = new float[1146];
		float sphere_textures[] = new float[764];
		short sphere_elements[] = new short[2280];

		System.out.println("VDG: " + "vertices and elements retrieval start");
		
		sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
		numVertices = sphere.getNumberOfSphereVertices();
		numElements = sphere.getNumberOfSphereElements();
		
		System.out.println("VDG: " + "vertices and elements retrieval done");
	
		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);
		
			GLES32.glGenBuffers(1, vbo_sphere_position, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
				verticesBuffer.put(sphere_vertices);
				verticesBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									sphere_vertices.length * 4, 
									verticesBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_POSITION,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_POSITION);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
			GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer normalBuffer = byteBuffer.asFloatBuffer();
				normalBuffer.put(sphere_normals);
				normalBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									sphere_normals.length * 4, 
									normalBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

			GLES32.glGenBuffers(1, vbo_sphere_element, 0);
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
				byteBuffer.order(ByteOrder.nativeOrder());
				ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
				elementsBuffer.put(sphere_elements);
				elementsBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
									sphere_elements.length * 2,
									elementsBuffer,
									GLES32.GL_STATIC_DRAW);
			}
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);

		GLES32.glBindVertexArray(0);

		System.out.println("VDG: " + "vao done");

		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
//		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		
		giSingleTap = 0;
		doubleTap = 0;
		
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	
		System.out.println("VDG: " + "initialize done");
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		giWindowWidth = width;
		giWindowHeight = height;
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/(float)height, 0.1f, 100.0f);			
	}
	
	public void draw()
	{
		int i = 0;
		float length = 1.2f;
		
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);

		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float scaleMatrix[] = new float[16];
				
		int viewPortWidth = giWindowWidth/4;
		int viewPortHeight = giWindowHeight/6;
		int viewPortX = 0;
		int viewPortY =0;

		for (i = 1; i < 25; i++)
		{
			GLES32.glViewport(viewPortX, viewPortY, viewPortWidth, viewPortHeight);
			
			viewPortY += viewPortHeight;

			if (i % 6 == 0)
			{
				//x += length;
				//y = -length * 2.5;
				viewPortX += viewPortWidth;
				viewPortY = 0;
			}
			if(doubleTap == 1)
			{
				GLES32.glUniform1i(double_tap_uniform, 1);
				
				GLES32.glUniform3fv(La_uniform, 1, lightAmbient, 0);
				GLES32.glUniform3fv(Ld_uniform, 1, lightDiffuse, 0);
				GLES32.glUniform3fv(Ls_uniform, 1, lightSpecular, 0);
				GLES32.glUniform4fv(light_position_uniform, 1, lightPosition, 0);

				GLES32.glUniform3fv(Ka_uniform, 1, materialAmbient[i-1], 0);
				GLES32.glUniform3fv(Kd_uniform, 1, materialDiffuse[i-1], 0);
				GLES32.glUniform3fv(Ks_uniform, 1, materialSpecular[i-1], 0);
				GLES32.glUniform1f(material_shininess_uniform, materialShininess[i-1]);
			}
			else
			{
				GLES32.glUniform1i(double_tap_uniform, 0);
			}
		
				
			Matrix.setIdentityM(modelMatrix, 0);
			Matrix.setIdentityM(viewMatrix, 0);
			Matrix.setIdentityM(scaleMatrix, 0);
			
			Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -2.0f);
			Matrix.scaleM(scaleMatrix, 0, (float)viewPortHeight/(float)viewPortWidth * ((float)giWindowWidth/(float)giWindowHeight), 1.0f, 1.0f);
			Matrix.multiplyMM(modelMatrix, 0, scaleMatrix, 0, modelMatrix, 0);
									
			GLES32.glUniformMatrix4fv(model_matrix_uniform, 1, false, modelMatrix, 0);
			GLES32.glUniformMatrix4fv(view_matrix_uniform, 1, false, viewMatrix, 0);
			GLES32.glUniformMatrix4fv(projection_matrix_uniform, 1, false, perspectiveProjectionMatrix, 0);
				
			GLES32.glBindVertexArray(vao_sphere[0]);
			
				GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
				GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
			
			GLES32.glBindVertexArray(0);
		}
		GLES32.glUseProgram(0);

		if(doubleTap > 0)
		{
			gAngle += 0.01;
			if (gAngle >= 2*3.145)
			{
				gAngle = 0.0;
			}

			if (giSingleTap == 1)
			{
				lightPosition[1] = (float)(100.0 * Math.cos(gAngle));
				lightPosition[2] = (float)(100.0 * Math.sin(gAngle));
				lightPosition[0] = 0.0f;
			}
			else if (giSingleTap == 2)
			{
				lightPosition[0] = (float)(100.0 * Math.cos(gAngle));
				lightPosition[2] = (float)(100.0 * Math.sin(gAngle));
				lightPosition[1] = 0.0f;
			}
			else if (giSingleTap == 3)
			{
				lightPosition[0] = (float)(100.0 * Math.cos(gAngle));
				lightPosition[1] = (float)(100.0 * Math.sin(gAngle));
				lightPosition[2] = 0.0f;
			}
		}
	
		requestRender();
	}
	
	public void uninitialize()
	{
		if(vao_sphere[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
			vao_sphere[0] = 0;
		}
		
		if(vbo_sphere_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
			vbo_sphere_position[0] = 0;
		}
		
		if(vbo_sphere_normal[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
			vbo_sphere_normal[0] = 0;
		}

		if(vbo_sphere_element[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
			vbo_sphere_element[0] = 0;
		}
		
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}
		
		if(shaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
			
		
	}
}
