package com.astromedicomp.cubelights;

import android.content.Context;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.opengl.GLSurfaceView;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix; //for Matrix Math.

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
		
	private final Context context;
	
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	
	private int[] vao_cube = new int[1];
	private int[] vbo_cube_position = new int[1];
	private int[] vbo_cube_normal = new int [1];
		
	private int modelViewMatrixUniform, projectionMatrixUniform;
	private int ldUniform, kdUniform, lightPositionUniform;
	
	private int doubleTapUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16];
	
	private float angleCube = 0.0f;
	
	private int singleTap  = 0;
	private int doubleTap = 0;
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
		
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version =" + version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override 
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
			resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
			draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		System.out.println("VDG: " + "Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;
		if(singleTap > 1)
			singleTap = 0;
		System.out.println("VDG: " + "Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: " + "Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: " + "Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+
		"in vec4 vPosition;"						+
		"in vec3 vNormal;"							+
		"uniform mat4 u_model_view_matrix;"			+
		"uniform mat4 u_projection_matrix;"			+
		"uniform mediump int u_double_tap;"			+
		"uniform vec3 u_Ld;"						+
		"uniform vec3 u_Kd;"						+	
		"uniform vec4 u_light_position;"			+
		"out vec3 diffuse_light;"					+
		"void main(void)"							+	
		"{"											+
		"if(u_double_tap == 1)"						+
		"{"											+
		"vec4 eyeCoordinates = u_model_view_matrix * vPosition;"				+
		"vec3 tnorm = normalize(mat3(u_model_view_matrix) * vNormal);"			+
		"vec3 s = normalize(vec3(u_light_position - eyeCoordinates));"			+
		"diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);"				+
		"}"																		+
		"gl_Position = u_projection_matrix * u_model_view_matrix * vPosition;"	+
		"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG:: Vertex Shader Compilation Log = "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+
		"precision highp float;"					+
		"in vec3 diffuse_light;"					+
		"out vec4 FragColor;"						+
		"uniform int u_double_tap;"					+
		"void main(void)"							+
		"{"											+
		"vec4 color;"								+					
		"if(u_double_tap == 1)"						+
		"{"											+
		"color = vec4(diffuse_light, 1.0);"			+
		"}"											+
		"else"										+
		"{"											+
		"color = vec4(1.0, 1.0, 1.0, 1.0);"			+
		"}"											+
		"FragColor = color;"						+
		"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0]= 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG:: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}	
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_POSITION, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject , GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG:: Shader Program Link Log:: "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		modelViewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_view_matrix");
		projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
		
		doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");
		
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		
		
		final float cubeVertices[] = new float[]
		{
			1.0f, 1.0f, 1.0f, //front
			1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, 1.0f,
			-1.0f, 1.0f, 1.0f,

			1.0f, 1.0f, -1.0f, //back
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, 1.0f, -1.0f,

			1.0f, 1.0f, 1.0f, //right
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, 1.0f,

			-1.0f, 1.0f, 1.0f, //left
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,

			1.0f, 1.0f, 1.0f, //top
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f,

			1.0f, -1.0f, 1.0f, //bottom
			-1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, -1.0f
		};

		final float cubeNormals[] = new float[]
		{
			0.0f, 0.0f, 1.0f, 
			0.0f, 0.0f, 1.0f, 
			0.0f, 0.0f, 1.0f, 
			0.0f, 0.0f, 1.0f, 

			0.0f, 0.0f, -1.0f, 
			0.0f, 0.0f, -1.0f, 
			0.0f, 0.0f, -1.0f, 
			0.0f, 0.0f, -1.0f, 

			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,

			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,

			0.0f, 1.0f, 0.0f, 
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
		};
	
		GLES32.glGenVertexArrays(1, vao_cube, 0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
			GLES32.glGenBuffers(1, vbo_cube_position, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_position[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
				verticesBuffer.put(cubeVertices);
				verticesBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									cubeVertices.length * 4, 
									verticesBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_POSITION,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_POSITION);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
			GLES32.glGenBuffers(1, vbo_cube_normal, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_cube_normal[0]);
			{
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(cubeNormals.length * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer normalBuffer = byteBuffer.asFloatBuffer();
				normalBuffer.put(cubeNormals);
				normalBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									cubeNormals.length * 4, 
									normalBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
		GLES32.glBindVertexArray(0);
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
//		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		singleTap = 0;
		doubleTap = 0;
		
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/(float)height, 0.1f, 100.0f);			
	}
	
	public void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		
			if(doubleTap == 1)
			{
				GLES32.glUniform1i(doubleTapUniform, 1);
				
				GLES32.glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
				GLES32.glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f);
				float[] lightPosition = {0.0f, 0.0f, 2.0f, 1.0f};
				GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
			}
			else
			{
				GLES32.glUniform1i(doubleTapUniform, 0);
			}
		
			float modelMatrix[] = new float[16];
			float modelViewMatrix[] = new float[16];
			float rotationMatrix[] = new float[16];
				
			Matrix.setIdentityM(modelMatrix, 0);
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(rotationMatrix, 0);
			
			Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -6.0f);
						
			Matrix.setRotateM(rotationMatrix, 0, angleCube, 1.0f, 1.0f, 1.0f);
			
			Matrix.multiplyMM(modelViewMatrix, 0, modelMatrix, 0, rotationMatrix, 0);
			
			GLES32.glUniformMatrix4fv(modelViewMatrixUniform, 1, false, modelViewMatrix, 0);
			GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);
			
			GLES32.glBindVertexArray(vao_cube[0]);
				
				GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
				GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
				GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
				GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
				GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
				GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
			
			GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);

		if(singleTap == 1)
		{
			angleCube = angleCube - 0.75f;
		}
		
		requestRender();
	}
	
	public void uninitialize()
	{
		if(vao_cube[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_cube, 0);
			vao_cube[0] = 0;
		}
		
		if(vbo_cube_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_cube_position, 0);
			vbo_cube_position[0] = 0;
		}
		
		if(vbo_cube_normal[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_cube_normal, 0);
			vbo_cube_normal[0] = 0;
		}
		
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}
		
		if(shaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
			
		
	}
}
