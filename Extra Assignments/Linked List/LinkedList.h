#pragma once
#include<iostream>

#define SUCCESS true
#define FAILURE false
#define CHECK_ERROR(res) if(res!=SUCCESS) {return FAILURE;}
using namespace std;

typedef int data_t;
typedef bool result;

class LinkedList;

class IList
{
public:
	virtual result AddLast(data_t data) = 0;
	virtual result AddStart(data_t data) = 0;
	virtual result Del(int index) = 0;
	virtual result DelFirst() = 0;
	virtual result DelLast() = 0;
	virtual result Display() = 0;
	static IList* GetInstance();
};

class LinkedList: public IList
{
private:
	struct node
	{
		data_t data;
		node *next;
	};
	node *head;
	result CreateNode(data_t data, node **tempNode);
	int Length();
	result GoToLastNode(node **tempNode);
	result GoToIndex(data_t index, node **tempNode);

public:
	LinkedList();
	result AddLast(data_t data);
	result AddStart(data_t data);
	result Del(int index);
	result DelFirst();
	result DelLast();
	result Display();
};
