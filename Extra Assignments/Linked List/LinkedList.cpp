#include"LinkedList.h"


LinkedList::LinkedList()
{
	if (CreateNode(0, &head) != SUCCESS)
	{
		cout << "Node creation failed. Exiting code.";
		exit(1);
	}
}

result LinkedList::CreateNode(data_t data, node **tempNode)
{
	*tempNode = new node;
	if (*tempNode != NULL)
	{
		(*tempNode)->data = data;
		(*tempNode)->next = NULL;
		return SUCCESS;
	}
	return FAILURE;
}

result LinkedList::GoToLastNode(node **tempNode)
{
	*tempNode = head;

	while ((*tempNode)->next != NULL)
	{
		*tempNode = (*tempNode)->next;
	}
	return SUCCESS;
}

result LinkedList::GoToIndex(data_t index, node **tempNode)
{
	*tempNode = head;
	int i = 0;

	while ((*tempNode)->next != NULL && i < index)
	{
		*tempNode = (*tempNode)->next;
		i++;
	}
	if (i == index)
		return SUCCESS;
	else
		return FAILURE;
}

int LinkedList::Length()
{
	node *temp = head->next;
	int i = 0;

	while (temp != NULL)
	{
		temp = temp->next;
		i++;
	}

	return i;
}

result LinkedList::AddLast(data_t data)
{
	node *lastNode = NULL;
	node *newNode = NULL;
	GoToLastNode(&lastNode);
	CreateNode(data, &newNode);
	lastNode->next = newNode;
	return SUCCESS;
}

result LinkedList::AddStart(data_t data)
{
	node *newNode = NULL;
	
	CHECK_ERROR(CreateNode(data, &newNode));
	newNode->next = head->next;
	head->next = newNode;
	
	return SUCCESS;
}

result LinkedList::Del(int index)
{
	node *delNode = NULL;
	node *prevNode = NULL;
	result res = SUCCESS;

	CHECK_ERROR(GoToIndex(index - 1, &prevNode));
	CHECK_ERROR(GoToIndex(index, &delNode));

	prevNode->next = delNode->next;
	free(delNode);

	return SUCCESS;
}

result LinkedList::DelFirst()
{
	node *delNode = NULL;

	return Del(1);
}

result LinkedList::DelLast()
{
	return(Del(Length()));
}

result LinkedList::Display()
{
	node *temp = head->next;
	cout <<endl<< "Linked List is::";
	while (temp != NULL)
	{
		cout << "{" << temp->data << "} ";
		temp = temp->next;
	}
	return SUCCESS;
}

IList* IList::GetInstance()
{
	IList *l = new LinkedList;
	return l;
}

