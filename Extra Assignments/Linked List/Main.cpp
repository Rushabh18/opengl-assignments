#include<stdio.h>
#include"LinkedList.h"

#define PRINT_ERROR(res) if(res!=SUCCESS){cout<<"ERROR OCCURED";}
int main()
{

	class IList *list;
	list = IList::GetInstance();
	char input;
	do
	{
		cout << endl<< "*****LINKED LIST OPERATIONS*****"<<endl;
		cout << "1. Add Elements at End"<<endl;
		cout << "2. Add Single Element at Start" << endl;
		cout << "3. Add Single Element at End" << endl;
		cout << "4. Delete Last Element" << endl;
		cout << "5. Delete First Element" << endl;
		cout << "6. Delete Element at Index"<<endl;
		cout << "7. Display Elements"<<endl;
		cout << "8. Exit"<<endl;
		cout << "Enter the operation you want to perform::";
		cin >> input;

		switch (input)
		{
		case '1':
		{
			int count = 0, i = 0, data = 0;
			cout << endl << "How many elements you want to add::";
			cin >> count;
			for (i = 0; i < count; i++)
			{
				cout <<endl<< "List[" << i << "]::";
				cin >> data;
				PRINT_ERROR(list->AddLast(data));
			}
			list->Display();
		}
			break;

		case '2':
		{
			int data = 0;
			cout << "Enter Element to Add at start::";
			cin >> data;
			PRINT_ERROR(list->AddStart(data));
			PRINT_ERROR(list->Display());
		}
			break;

		case '3':
		{
			int data = 0;
			cout << "Enter Element to Add at last::";
			cin >> data;
			PRINT_ERROR(list->AddLast(data));
			PRINT_ERROR(list->Display());
		}
			break;

		case '4':
			PRINT_ERROR(list->DelLast());
			PRINT_ERROR(list->Display());
			break;

		case '5':
			PRINT_ERROR(list->DelFirst());
			PRINT_ERROR(list->Display());
			break;

		case '6':
			{
			int index = 0;
			cout << "Which index element to delete::";
			cin >> index;
			PRINT_ERROR(list->Del(index));
			PRINT_ERROR(list->Display());
			}
			break;

		case '7':
			PRINT_ERROR(list->Display());
			break;

		case '8':
			exit(0);
		default:
			cout << "You pressed an incorrect option.";
			break;
		}
	} while (1);
	return 0;
}