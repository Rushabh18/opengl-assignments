//common headers
#include <windows.h>
#include <stdio.h>
#include "Template.h"

//OpenGL headers
#include <gl/glew.h>
#include <gl/GL.h>

#include "vmath.h"

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 800
#define SPEED 0.1f

// import libraries
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glew32.lib")

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTRURE,
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

FILE *gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gTessControlShaderObject;
GLuint gTessEvaluationShaderObject;
GLuint gShaderProgramObject;

GLuint gVao;
GLuint gVbo_position;
GLuint gVbo_color;

GLuint gMVPUniform;
GLuint gModelViewMatrixUniform, gProjectionMatrixUniform;

GLuint gEnableFogUniform;
GLuint gDmapDepthUniform;

GLuint gTexture_color_uniform;
GLuint gTexture_displacement_uniform;
GLuint gTexture_Terrain_Map;
GLuint gTexture_Mountain;


mat4 gPerspectiveProjectionMatrix;

bool gbWireFrame = false;
bool gbEnableFog = false;
bool gbEnableDisplacement = false;
float gfDmapDepth = 2.0f;

float gX = 0.0f;
float gZ = 0.0f;
float gY = 0.0f;


//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File can not be Created\nExitting...."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	fprintf(gpFile, "Log File is successfully opened. \n");

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipleline:: Terrain Map"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}

			//display
			display();
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_UP:
			gZ += SPEED;
			break;
		case VK_DOWN:
			gZ -= SPEED;
			break;
		case VK_LEFT:
			gX += SPEED;
			break;
		case VK_RIGHT:
			gX -= SPEED;
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 'P':
		case 'p':
			gY += SPEED;
			break;

		case 'L':
		case 'l':
			gY -= SPEED;
			break;

		case 'F':
		case 'f'://for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 'A':
		case 'a':
			gfDmapDepth += 0.1f;
			break;
		case 'S':
		case 's':
			gfDmapDepth -= 0.1f;
			break;
		case 'z':
		case 'Z':
			gbEnableFog = !gbEnableFog;
			break;
		case 'w':
		case 'W':
			gbWireFrame = !gbWireFrame;
			break;
		}
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	//code 
	glGenTextures(1, texture); //1 image
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap) //if bitmap exists ( means hBitmap is not null )
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //pixel storage mode( word alignment/4 bytes ).
		glBindTexture(GL_TEXTURE_2D, *texture); //bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//generate mipmapped texture ( 3 bytes, width, height & data from bmp)
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR_EXT,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap); //delete unwanted bitmap handle
	}
	return(iStatus);
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();
	void computeIsohedronVertices(GLfloat[]);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	/*** VERTEX SHADER ***/
	//create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 440 core"							\
		"\n"										\
		"out VS_OUT"								\
		"{"											\
		"vec2 tc;"									\
		"}vs_out;"									\
		"void main(void)"							\
		"{"											\
		"const vec4 vertices[] = vec4[](vec4(-0.5, 0.0, -0.5, 1.0),"  \
		"								vec4( 0.5, 0.0, -0.5, 1.0),"  \
		"								vec4(-0.5, 0.0,  0.5, 1.0),"  \
		"								vec4( 0.5, 0.0,  0.5, 1.0));" \
		"int x = gl_InstanceID & 63;"				\
		"int y = gl_InstanceID >> 6;"				\
		"vec2 offs = vec2(x,y);"					\
		"vs_out.tc = (vertices[gl_VertexID].xz + offs + vec2(0.5))/64.0;"					\
/*		"gl_Position = vertices[gl_VertexID] + vec4(float(x-32), 0.0, float(y-32), 0.0);"	\*/
		"gl_Position = vertices[gl_VertexID] + vec4(float(x-32), 0.0, float(y-32), 0.0);"	\
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gTessControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);

	const GLchar *tessControlShaderSourceCode =
		"#version 440 core"							\
		"\n"										\
		"layout (vertices = 4) out;"				\
		"in VS_OUT"									\
		"{"											\
		"vec2 tc;"									\
		"}tcs_in[];"								\
		"out TCS_OUT"								\
		"{"											\
		"vec2 tc;"									\
		"}tcs_out[];"								\
		"uniform mat4 u_mvp_matrix;"				\

		"void main(void)"							\
		"{"											\
		"if(gl_InvocationID == 0)"					\
		"{"											\
		"vec4 p0 = u_mvp_matrix * gl_in[0].gl_Position;"		\
		"vec4 p1 = u_mvp_matrix * gl_in[1].gl_Position;"		\
		"vec4 p2 = u_mvp_matrix * gl_in[2].gl_Position;"		\
		"vec4 p3 = u_mvp_matrix * gl_in[3].gl_Position;"		\
		"p0 /= p0.w;"								\
		"p1 /= p1.w;"								\
		"p2 /= p2.w;"								\
		"p3 /= p3.w;"								\
		"if(p0.z <= 0.0 || p1.z <= 0.0 || p2.z <= 0.0 || p3.z <= 0.0)"	\
		"{"											\
		"gl_TessLevelOuter[0] = 0.0;"				\
		"gl_TessLevelOuter[1] = 0.0;"				\
		"gl_TessLevelOuter[2] = 0.0;"				\
		"gl_TessLevelOuter[3] = 0.0;"				\
		"}"											\
		"else"										\
		"{"											\
		"float l0 = length(p2.xy - p0.xy) * 16.0 + 1.0;"	\
		"float l1 = length(p3.xy - p2.xy) * 16.0 + 1.0;"	\
		"float l2 = length(p3.xy - p1.xy) * 16.0 + 1.0;"	\
		"float l3 = length(p1.xy - p0.xy) * 16.0 + 1.0;"	\
		"gl_TessLevelOuter[0] = l0;"						\
		"gl_TessLevelOuter[1] = l1;"						\
		"gl_TessLevelOuter[2] = l2;"						\
		"gl_TessLevelOuter[3] = l3;"						\
		"gl_TessLevelInner[0] = min(l1, l3);"				\
		"gl_TessLevelInner[1] = min(l0, l2);"				\
		"}"													\
		"}"													\
		"gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;"	\
		"tcs_out[gl_InvocationID].tc = tcs_in[gl_InvocationID].tc;"					\
		"}";
	glShaderSource(gTessControlShaderObject, 1, (const GLchar **)&tessControlShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gTessControlShaderObject);
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;

	glGetShaderiv(gTessControlShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gTessControlShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gTessControlShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Tessellation Control shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gTessEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);

	//Without FOG
	//const GLchar *tessEvaluationShaderSourceCode =
	//	"#version 440 core"							\
	//	"\n"										\
	//	"layout (quads, fractional_odd_spacing) in;"\
	//	"uniform sampler2D u_tex_displacement;"		\
	//	"uniform mat4 u_mvp_matrix;"					\
	//	"uniform float u_dmap_depth;"					\
	//	"in TCS_OUT"								\
	//	"{"											\
	//	"vec2 tc;"									\
	//	"} tes_in[];"								\
	//	"out TES_OUT"								\
	//	"{"											\
	//	"vec2 tc;"									\
	//	"} tes_out;"								\
	//	"void main(void)"							\
	//	"{"											\
	//	"vec2 tc1 = mix(tes_in[0].tc, tes_in[1].tc, gl_TessCoord.x);"\
	//	"vec2 tc2 = mix(tes_in[2].tc, tes_in[3].tc, gl_TessCoord.x);"\
	//	"vec2 tc = mix(tc2, tc1, gl_TessCoord.y);"	\
	//	"vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);"	\
	//	"vec4 p2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);"	\
	//	"vec4 p = mix(p2, p1, gl_TessCoord.y);"		\
	//	"p.y += texture(u_tex_displacement, tc).r * u_dmap_depth;"\
	//	"gl_Position = u_mvp_matrix * p;"			\
	//	"tes_out.tc = tc;"							\
	//	"}";

	const GLchar *tessEvaluationShaderSourceCode =
		"#version 440 core"							\
		"\n"										\
		"layout (quads, fractional_odd_spacing) in;"\
		"uniform sampler2D u_tex_displacement;"		\
		"uniform mat4 u_mv_matrix;"				\
		"uniform mat4 u_p_matrix;"				\
		"uniform float u_dmap_depth;"				\
		"in TCS_OUT"								\
		"{"											\
		"vec2 tc;"									\
		"} tes_in[];"								\
		"out TES_OUT"								\
		"{"											\
		"vec2 tc;"									\
		"vec3 world_coord;"							\
		"vec3 eye_coord;"							\
		"} tes_out;"								\
		"void main(void)"							\
		"{"											\
		"vec2 tc1 = mix(tes_in[0].tc, tes_in[1].tc, gl_TessCoord.x);"\
		"vec2 tc2 = mix(tes_in[2].tc, tes_in[3].tc, gl_TessCoord.x);"\
		"vec2 tc = mix(tc2, tc1, gl_TessCoord.y);"	\
		"vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);"	\
		"vec4 p2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);"	\
		"vec4 p = mix(p2, p1, gl_TessCoord.y);"		\
		"p.y += texture(u_tex_displacement, tc).r * u_dmap_depth;"\
		"vec4 P_eye = u_mv_matrix * p;"				\
		"tes_out.tc = tc;"							\
		"tes_out.world_coord = p.xyz;"				\
		"tes_out.eye_coord = P_eye.xyz;"			\
		"gl_Position = u_p_matrix * P_eye;"			\
		"}";

	glShaderSource(gTessEvaluationShaderObject, 1, (const GLchar **)&tessEvaluationShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gTessEvaluationShaderObject);
	iInfoLogLength = 0;
	iShaderCompiledStatus = 0;
	szInfoLog = NULL;
	
	glGetShaderiv(gTessEvaluationShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gTessEvaluationShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gTessEvaluationShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Tessellation Evaluation shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/*** FRAGMENT SHADER***/
	//create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);


	//Without FOG
	//const GLchar *fragmentShaderSourceCode =
	//	"#version 440 core"										\
	//	"\n"													\
	//	"out vec4 FragColor;"									\
	//	"layout (binding = 1) uniform sampler2D u_tex_color;"	\
	//	"in TES_OUT"											\
	//	"{"														\
	//	"vec2 tc;"												\
	//	"} fs_in;"												\
	//	"void main(void)"										\
	//	"{"														\
	//	"FragColor = texture(u_tex_color, fs_in.tc);"			\
	//	"}";

	const GLchar *fragmentShaderSourceCode =
		"#version 440 core"										\
		"\n"													\
		"out vec4 FragColor;"									\
		"layout (binding = 1) uniform sampler2D u_tex_color;"	\
		"uniform bool u_enable_fog;"							\
		"uniform vec4 fog_color = vec4(0.6, 0.7, 0.8, 0.0);"	\
		"in TES_OUT"											\
		"{"														\
		"vec2 tc;"												\
		"vec3 world_coord;"										\
		"vec3 eye_coord;"										\
		"} fs_in;"												\
		"vec4 fog(vec4 c)"															\
		"{"																			\
		"float z = length(fs_in.eye_coord);"										\
		"float de = 0.025 * smoothstep(0.0, 6.0, 10.0 - fs_in.world_coord.y);"		\
		"float di = 0.045 * (smoothstep(0.0, 40.0, 20.0 - fs_in.world_coord.y));"	\
		"float extinction = exp(-z * de);"											\
		"float inscattering = exp(-z * di);"										\
		"return c * extinction + fog_color * (1.0 - inscattering);"					\
		"}"																			\
		"void main(void)"										\
		"{"														\
		"vec4 landscape = texture(u_tex_color, fs_in.tc); "		\
		"if(u_enable_fog)"										\
		"{"														\
		"FragColor = fog(landscape);"							\
		"}"														\
		"else"													\
		"{"														\
		"FragColor = landscape;"								\
		"}"														\
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/**** SHADER PROGRAM ****/
	//create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gTessControlShaderObject);
	glAttachShader(gShaderProgramObject, gTessEvaluationShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	gModelViewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_mv_matrix");
	gProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");

	gEnableFogUniform = glGetUniformLocation(gShaderProgramObject, "u_enable_fog");
	gDmapDepthUniform = glGetUniformLocation(gShaderProgramObject, "u_dmap_depth");
	gTexture_displacement_uniform = glGetUniformLocation(gShaderProgramObject, "u_tex_displacement");
	gTexture_color_uniform = glGetUniformLocation(gShaderProgramObject, "u_tex_color");

	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

	//clear color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//shade model
	glShadeModel(GL_SMOOTH);
	//depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	LoadGLTextures(&gTexture_Terrain_Map, MAKEINTRESOURCE(ID_BITMAP_TERRAIN_MAP));
	LoadGLTextures(&gTexture_Mountain, MAKEINTRESOURCE(ID_BITMAP_MOUNTAIN));

	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	gPerspectiveProjectionMatrix = perspective(60.0f, ((GLfloat)width/(GLfloat)height), 0.1f, 1000.0f);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//start using opengl program object
	glUseProgram(gShaderProgramObject);
	
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(gX, -4.0f + gY, -40.0f+gZ);
	
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniformMatrix4fv(gModelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);
	glUniformMatrix4fv(gProjectionMatrixUniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	glUniform1f(gDmapDepthUniform, gfDmapDepth);
	glUniform1i(gEnableFogUniform, gbEnableFog);

	glPatchParameteri(GL_PATCH_VERTICES, 4);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gTexture_Terrain_Map);
	glUniform1i(gTexture_displacement_uniform, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gTexture_Mountain);
	glUniform1i(gTexture_color_uniform, 1);

	if(gbWireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glDrawArraysInstanced(GL_PATCHES, 0, 4, 64 * 64);

	//double buffering
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gTessControlShaderObject);
	glDetachShader(gShaderProgramObject, gTessEvaluationShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gTessControlShaderObject);
	gTessControlShaderObject = 0;

	glDeleteShader(gTessEvaluationShaderObject);
	gTessEvaluationShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File is Successfully closed.");
		fclose(gpFile);
		gpFile = NULL;
	}
}
