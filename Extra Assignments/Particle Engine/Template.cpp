//common headers
#include <windows.h>
#include <stdio.h>

//OpenGL headers
#include <gl/glew.h>
#include <gl/GL.h>

#include "vmath.h"

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 800
#define PARTICLE_WIDTH 100
#define PARTICLE_HEIGHT 100
#define SPEED 0.0001f

// import libraries
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib, "glew32.lib")

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_POSITION = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTRURE,
	VDG_ATTRIBUTE_VELOCITY_ARRAY,
	VDG_ATTRIBUTE_START_TIME_ARRAY
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;


FILE *gpFile = NULL;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao;
GLuint gVbo_position;
GLuint gVbo_color;
GLuint gVbo_velocity;
GLuint gVbo_start_time;

GLuint gMVPUniform;
GLuint gLocationUniform;
GLuint gBackgroundUniform;

mat4 gPerspectiveProjectionMatrix;

GLfloat backgroundColor[4] = { 0.0f, 1.0f, 0.0f, 0.0f };

/********PARTICLE VARIABLES START**********/

GLint arrayWidth, arrayHeight;
GLfloat *pVertex = NULL;
GLfloat *pColor = NULL;
GLfloat *pVelocity = NULL;
GLfloat *pStartTime = NULL;

GLfloat gParticleTime = 0.0f;

/********PARTICLE VARAIBLE ENDS************/
//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File can not be Created\nExitting...."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	fprintf(gpFile, "Log File is successfully opened. \n");

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipleline:: Orthographic Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}

			//display
			update();
			display();
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();
	void createPoints(GLint, GLint);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	/*** VERTEX SHADER ***/
	//create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 440 core"									\
		"\n"												\
		"uniform float u_time;"								\
		"uniform vec4 u_background;"						\
		"uniform mat4 u_mvp_matrix;"						\
		"in vec4 vPosition;"								\
		"in vec4 vColor;"									\
		"in vec3 vVelocity;"								\
		"in float fStartTime;"								\
		"out vec4 out_color;"								\
		"void main(void)"									\
		"{"													\
		"vec4 vertice = vPosition;"							\
		"float t = u_time - fStartTime;"					\
		"if(t >= 0)"										\
		"{"													\
		"vertice = vPosition + vec4(vVelocity * t, 0.0);"	\
		"vertice.y -= 4.9 * t * t;"							\
		"out_color = vColor;"								\
		"}"													\
		"else"												\
		"{"													\
		"vertice = vPosition;"								\
		"out_color = vColor;"								\
		"}"													\
		"gl_Position = u_mvp_matrix * vertice;"				\
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/*** FRAGMENT SHADER***/
	//create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 440 core"		\
		"\n"					\
		"in vec4 out_color;"	\
		"out vec4 FragColor;"	\
		"void main(void)"		\
		"{"						\
		"FragColor = out_color;"\
		"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	/**** SHADER PROGRAM ****/
	//create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VELOCITY_ARRAY, "vVelocity");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_START_TIME_ARRAY, "fStartTime");

	//link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log: %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	gLocationUniform = glGetUniformLocation(gShaderProgramObject, "u_time");
	gBackgroundUniform = glGetUniformLocation(gShaderProgramObject, "u_background");

	createPoints(PARTICLE_WIDTH, PARTICLE_HEIGHT);

	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

		glGenBuffers(1, &gVbo_position);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_position);
			glBufferData(GL_ARRAY_BUFFER, arrayWidth * arrayHeight * 3 * sizeof(GLfloat), pVertex, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_POSITION);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &gVbo_color);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_color);
			glBufferData(GL_ARRAY_BUFFER, arrayWidth * arrayHeight * 3 * sizeof(GLfloat), pColor, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &gVbo_velocity);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_velocity);
			glBufferData(GL_ARRAY_BUFFER, arrayWidth * arrayHeight * 3 * sizeof(GLfloat), pVelocity, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_VELOCITY_ARRAY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_VELOCITY_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &gVbo_start_time);
		glBindBuffer(GL_ARRAY_BUFFER, gVbo_start_time);
			glBufferData(GL_ARRAY_BUFFER, arrayWidth * arrayHeight * sizeof(GLfloat), pStartTime, GL_STATIC_DRAW);

			glVertexAttribPointer(VDG_ATTRIBUTE_START_TIME_ARRAY, 1, GL_FLOAT, GL_FALSE, 0, NULL);
			glEnableVertexAttribArray(VDG_ATTRIBUTE_START_TIME_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//clear color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//shade model
	glShadeModel(GL_SMOOTH);
	//depth
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	gPerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width/(GLfloat)height), 0.1f, 100.0f);
}

//Comment for Sankya Fankya
//Radius which should be less than R = R*(float)rand() / RAND_MAX
//Theta which should be less than 2 * 3.145 = 2 * 3.145*(float)rand() / RAND_MAX
//x = rcos(theta)
//z = rsin(theta)
//y = h
//

void createPoints(GLint w, GLint h)
{
	GLfloat *vptr = NULL, *cptr = NULL, *velptr = NULL, *stptr = NULL;

	GLfloat i = 0.0f, j = 0.0f;
	GLfloat x = 0.0f;
	GLfloat y = 0.0f;
	GLfloat r = 0.8f;
	GLfloat theta = 0.0f;

	pVertex = (float *)malloc(w * h * 3 * sizeof(float));
	pColor = (float *)malloc(w * h * 3 * sizeof(float));
	pVelocity = (float *)malloc(w * h * 3 * sizeof(float));
	pStartTime = (float *)malloc(w * h * sizeof(float));

	vptr = pVertex;
	cptr = pColor;
	velptr = pVelocity;
	stptr = pStartTime;

	for (i = 0; i < PARTICLE_WIDTH * PARTICLE_HEIGHT; i++)
	{
		*vptr = x;
		*(vptr + 1) = y;
		*(vptr + 2) = 0.0f;
		vptr += 3;

		x = r * cos(theta);
		y = r * sin(theta);

		theta += SPEED*50;
		r -= SPEED* 0.8;

		*cptr = ((float)rand() / RAND_MAX) *  0.9f + 0.1f;
		*(cptr + 1) = ((float)rand() / RAND_MAX) *  0.9f + 0.1f;
		*(cptr + 2) = ((float)rand() / RAND_MAX) *  0.9f + 0.1f;
		cptr += 3;

		*velptr = (((float)rand() / RAND_MAX) + 5.0f) * (rand() % 2 ? 1 : -1);
		*(velptr + 1) = ((float)rand() / RAND_MAX) *  5.0f  *(rand() % 2 ? 1 : -1);
		*(velptr + 2) = ((float)rand() / RAND_MAX) + 3.0f;
		velptr += 3;

		*stptr = i * SPEED;
		stptr++;
	}

	arrayWidth = w;
	arrayHeight = h;
}

void update()
{
	gParticleTime += SPEED;
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//start using opengl program object
	glUseProgram(gShaderProgramObject);

	glUniform1f(gLocationUniform, gParticleTime);
	glUniform4fv(gBackgroundUniform, 1, backgroundColor);
	glPointSize(1.0f);


	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -2.0f);

	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glBindVertexArray(gVao);

		glDrawArrays(GL_POINTS, 0, PARTICLE_WIDTH * PARTICLE_HEIGHT); 

	glBindVertexArray(0);
	//stop using opengl program object
	glUseProgram(0);

	//double buffering
	SwapBuffers(ghdc);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}

	if (gVbo_position)
	{
		glDeleteBuffers(1, &gVbo_position);
		gVbo_position = 0;
	}

	if (gVbo_color)
	{
		glDeleteBuffers(1, &gVbo_color);
		gVbo_color = 0;
	}

	if (gVbo_velocity)
	{
		glDeleteBuffers(1, &gVbo_velocity);
		gVbo_velocity = 0;
	}

	if (gVbo_start_time)
	{
		glDeleteBuffers(1, &gVbo_start_time);
		gVbo_start_time = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File is Successfully closed.");
		fclose(gpFile);
		gpFile = NULL;
	}
}
