
/*
* @author: Sanket Deshmukh
* @mailto: 1sanketdeshmukh@gmail.com
* please dont remove @author or modify it.
* any changes or bug fixes are done please annotate the locations where bug fixed.
*/

#include "Starfield.h"

Sanket::Starfield::Starfield()
{
	_particleTime = 0.0f;
}

Sanket::Starfield::~Starfield()
{
	UnInitialize();
}

float Sanket::Starfield::random_float(void)
{
	float res;
	unsigned int tmp;

	_seed *= 16807;

	tmp = _seed ^ (_seed >> 4) ^ (_seed << 15);

	*((unsigned int *)&res) = (tmp >> 9) | 0x3F800000;

	return (res - 1.0f);
}

BOOL Sanket::Starfield::SceneHandler(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	return 0;
}

void Sanket::Starfield::CreatePoints()
{
	GLfloat *vptr = NULL, *velptr = NULL, *stptr = NULL;

	int i = 0;
	GLfloat x = 0.0f;
	GLfloat y = 0.0f;
	GLfloat z = 0.0f;
	GLfloat r = 0.0f;
	GLfloat theta = 0.0f;
	GLfloat zInc = 0.0001f / 5;
	_pVertex = (float *)malloc(PARTICLE_WIDTH * PARTICLE_HEIGHT * 3 * sizeof(float));
	_pVelocity = (float *)malloc(PARTICLE_WIDTH * PARTICLE_HEIGHT * 3 * sizeof(float));
	_pStartTime = (float *)malloc(PARTICLE_WIDTH * PARTICLE_HEIGHT * sizeof(float));

	vptr = _pVertex;
	velptr = _pVelocity;
	stptr = _pStartTime;

	for (i = 0; i < PARTICLE_WIDTH * PARTICLE_HEIGHT; i++)
	{
		x = (((float)rand() / RAND_MAX)*1.0f) * (rand() % 2 ? 1 : -1);
		y = (((float)rand() / RAND_MAX)* 1.0f) * (rand() % 2 ? 1 : -1);
		z = -10.0f;

		*vptr = x;
		*(vptr + 1) = y;
		*(vptr + 2) = z;
		vptr += 3;

		*velptr = (((float)rand() / RAND_MAX)*0.5f);
		if (x < 0)
			*velptr *= -1;

		*(velptr + 1) = (((float)rand() / RAND_MAX)*0.5f);
		if (y < 0)
			*(velptr+1) *= -1;

		*(velptr + 2) = 3.0f;

		velptr += 3;

		*stptr = i * 0.01f;

		stptr++;
	}
}
void Sanket::Starfield::Initialize()
{
	
	SAFE_LOG("Starfield shading language supported version is : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION))

		//CREAT VERTEXT SHADER OBJECT
		_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//CREATE SOURCE CODE FOR VERTEX SHADER OBJECT
	const GLchar *vertexShaderSourcecode =

		//"#version 400 core" \
		//"\n" \
		//"in vec4 vPosition;" \
		//"in vec4 color;" \
		//"out vec4 starColor ;" \
		//"uniform mat4 u_mvp_matrix;" \
		//"uniform float time;" \
		//"void main(void)" \
		//"{" \
		//"vec4 newVertex = vPosition;" \
		//" newVertex.z += time ;" \
		//" newVertex.z = fract(newVertex.z);" \
		//" float size = (7.0 * newVertex.z * newVertex.z);" \
		//" starColor = smoothstep(1.0,7.0,size) * color;"\
		//" newVertex.z = (99.9 * newVertex.z) - 100.0 ;" \
		//"gl_Position = u_mvp_matrix * newVertex;" \
		//"gl_PointSize = size ; " \
		//"}";

		"#version 440 core"									\
		"\n"												\
		"uniform float u_time;"								\
		"uniform mat4 u_mvp_matrix;"						\
		"in vec4 vPosition;"								\
		"in vec3 vVelocity;"								\
		"in float fStartTime;"								\
		"out vec4 out_color;"								\
		"void main(void)"									\
		"{"													\
		"float size = 3.0;"									\
		"vec4 vertice = vPosition;"							\
		"float t = u_time - fStartTime;"					\
		"if(t >= 0)"										\
		"{"													\
		"vertice = vPosition + vec4(vVelocity * t, 0.0);"	\
		"out_color = vec4(0.0, 1.0, 0.0, 1.0);"				\
		"}"													\
		"else"												\
		"{"													\
		"vertice = vPosition;"								\
		"out_color = vec4(0.0, 0.0, 0.0, 1.0);"				\
		"}"													\
		"if(vertice.x > size || vertice.x < -size || vertice.y > size || vertice.y < -size || vertice.z > 0.0)"	\
		"{"													\
		"out_color = vec4(1.0, 0.0, 0.0, 1.0); "			\
		"}"													\
		"gl_Position = u_mvp_matrix * vertice;"				\
		"}";

	//BIND vertexShaderSourcecode CODE TO gVertexShaderObject
	glShaderSource(_vertexShaderObject, 1, (const GLchar **)&vertexShaderSourcecode, NULL);

	//COMPILE VERTEX SHADER
	glCompileShader(_vertexShaderObject);

	/****************/


	GLint infoLogLength = 0;
	GLint iShaderCompilationStatus = 0;
	char *szInfoLog = NULL;
	GLsizei written = 0;

	glGetShaderiv(_vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompilationStatus);

	if (iShaderCompilationStatus == GL_FALSE)
	{
		glGetShaderiv(_vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szInfoLog = (char*)malloc(infoLogLength);

			if (szInfoLog != NULL)
			{

				glGetShaderInfoLog(_vertexShaderObject, infoLogLength, &written, szInfoLog);
				SAFE_LOG("Starfield Vertex Shader Compilation Log : %s\n", szInfoLog)
					SAFE_FREE(szInfoLog);
				UnInitialize();
				exit(0);
			}
		}
	}


	//Create FRAGMENT SHADER OBJECT
	_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//CREATE SOURCE CODE FOR FRAGMENT SHADER
	const GLchar *fragmentShaderSourceCode =

		//"#version 400 core" \
		//"\n" \
		//"in vec4 starColor;" \
		//"out vec4 FragColor;" \
		//"void main(void)" \
		//"{" \
		//"FragColor = starColor;"\
		//"}";

		"#version 440 core"		\
		"\n"					\
		"in vec4 out_color;"	\
		"out vec4 FragColor;"	\
		"void main(void)"		\
		"{"						\
		"FragColor = out_color;"\
		"}";

	//BIND fragmentShaderSourceCode to gFragmentShaderObject
	glShaderSource(_fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//COMPILE FRAGMENT SHADER
	glCompileShader(_fragmentShaderObject);

	infoLogLength = 0;
	iShaderCompilationStatus = 0;
	szInfoLog = NULL;
	written = 0;

	glGetShaderiv(_fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompilationStatus);
	if (iShaderCompilationStatus == GL_FALSE)
	{
		glGetShaderiv(_fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(infoLogLength));

			if (szInfoLog != NULL)
			{
				glGetShaderInfoLog(_fragmentShaderObject, infoLogLength, &written, szInfoLog);

				SAFE_LOG("Starfield Fragment Shader Compilation Log : %s\n", szInfoLog)
					SAFE_FREE(szInfoLog);
				UnInitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ****//
	//CREATE SHADER PROGRAM OBJECT
	_shaderProgramObject = glCreateProgram();

	//ATTACH VERTEX SHADER PROGRAM TO SHADER PROGRAM
	glAttachShader(_shaderProgramObject, _vertexShaderObject);

	//ATTACH FRAGMENT SHADER PROGRAM TO SHADER PROGRAM
	glAttachShader(_shaderProgramObject, _fragmentShaderObject);

	//PRE-LINK BINDING OF SHADER PROGRAM OBJECT WITH VERTEX SHADER POSITION ATTRIBUTE
	glBindAttribLocation(_shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(_shaderProgramObject, AMC_ATTRIBUTE_VELOCITY_ARRAY, "vVelocity");
	glBindAttribLocation(_shaderProgramObject, AMC_ATTRIBUTE_START_TIME_ARRAY, "fStartTime");

	//LINK THE SHADER
	glLinkProgram(_shaderProgramObject);

	infoLogLength = 0;
	iShaderCompilationStatus = 0;
	szInfoLog = NULL;
	written = 0;

	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(_shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(_shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			szInfoLog = (char*)malloc(sizeof(infoLogLength));
			if (szInfoLog != NULL)
			{

				glGetProgramInfoLog(_shaderProgramObject, infoLogLength, &written, szInfoLog);

				SAFE_LOG("Starfield Shader Program Link Log : %s \n", szInfoLog)
					SAFE_FREE(szInfoLog);
				UnInitialize();
				exit(0);
			}

		}
	}


	_MVPUniform = glGetUniformLocation(_shaderProgramObject, "u_mvp_matrix");

//	_TimeUniform = glGetUniformLocation(_shaderProgramObject, "time");


	_TimeUniform = glGetUniformLocation(_shaderProgramObject, "u_time");
	/// *** vertices , colors , shader attribs , vbo ,vao initializations *** ///


	// For particle system

	float starPositions[6000];

	for (int i = 0; i < 6000; i++)
	{
		starPositions[i] = (random_float() * 2.0f - 1.0f) * 10.0f;
		starPositions[i + 1] = (random_float() * 2.0f - 1.0f) * 10.0f;
		starPositions[i + 2] = random_float();
		i = i + 3;
	}


	float starColors[6000];

	for (int i = 0; i < 6000; i++)
	{

		starColors[i] = 0.0f;
		starColors[i + 1] = 1.0f;
		starColors[i + 2] = 0.0f;

		i = i + 3;

	}


	//glGenVertexArrays(1, &vao);
	//glBindVertexArray(vao);

	//glGenBuffers(1, &vbo_Position);
	//glBindBuffer(GL_ARRAY_BUFFER, vbo_Position);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(starPositions), starPositions, GL_STATIC_DRAW);

	//glVertexAttribPointer(AMC_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_VERTEX);

	//glBindBuffer(GL_ARRAY_BUFFER, 0);

	////// vbo for texture 

	//glGenBuffers(1, &vbo_Color);
	//glBindBuffer(GL_ARRAY_BUFFER, vbo_Color);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(starColors), starColors, GL_STATIC_DRAW);

	//glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//glBindBuffer(GL_ARRAY_BUFFER, 0);

	//glBindVertexArray(0);

	CreatePoints();

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);
	glBufferData(GL_ARRAY_BUFFER, PARTICLE_HEIGHT * PARTICLE_WIDTH * 3 * sizeof(GLfloat), _pVertex, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_velocity);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_velocity);
	glBufferData(GL_ARRAY_BUFFER, PARTICLE_HEIGHT * PARTICLE_WIDTH * 3 * sizeof(GLfloat), _pVelocity, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_VELOCITY_ARRAY, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_VELOCITY_ARRAY);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_start_time);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_start_time);
	glBufferData(GL_ARRAY_BUFFER, PARTICLE_HEIGHT * PARTICLE_WIDTH * sizeof(GLfloat), _pStartTime, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_START_TIME_ARRAY, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_START_TIME_ARRAY);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_CULL_FACE); /*Turning oFF cull , as we are doing animation , and so need to paint the back of object when rotating*/

	_perspectiveProjectionMatrix = mat4::identity();

	return;

}

void Sanket::Starfield::Update()
{

}

void Sanket::Starfield::ReSize(int width, int height, struct ResizeAttributes attributes)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	_perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, attributes.nearValue, attributes.farValue);

}

void Sanket::Starfield::Render(HDC hdc, struct Attributes attributes)
{
	GetSystemTime(&_systemTime);


	float t = (float)_systemTime.wMilliseconds;


	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	modelViewMatrix = mat4::identity();

	modelViewMatrix = translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = _perspectiveProjectionMatrix * modelViewMatrix;

	//START USING SHADER OBJECT	
	glUseProgram(_shaderProgramObject);
	glPointSize(4.0f);

	glUniformMatrix4fv(_MVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glUniform1f(_TimeUniform, _particleTime);
	//t *= 0.0001f;
	//t -= floor(t);

	//glUniform1f(_TimeUniform, t);

	glBindVertexArray(vao);

	//glEnable(GL_PROGRAM_POINT_SIZE);

	glDrawArrays(GL_POINTS, 0, PARTICLE_WIDTH * PARTICLE_HEIGHT);

	glBindVertexArray(0);

	
	//STOP USING SHADER
	glUseProgram(0);
	_particleTime = _particleTime + 0.01f;
	return;
}

void Sanket::Starfield::SceneTransition()
{
}

void Sanket::Starfield::UnInitialize()
{
	
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	if (vbo_Position)
	{
		glDeleteBuffers(1, &vbo_Position);
		vbo_Position = 0;
	}

	if (vbo_Color)
	{
		glDeleteBuffers(1, &vbo_Color);
		vbo_Color = 0;
	}


	//DETACH THE FRAGMENT SHADER OBJECT
	glDetachShader(_shaderProgramObject, _fragmentShaderObject);

	//DETACH THE VERTEX SHADER OBJECT
	glDetachShader(_shaderProgramObject, _vertexShaderObject);


	//DELETE VERTEX SHADER OBJECT
	glDeleteShader(_vertexShaderObject);
	_vertexShaderObject = 0;

	//DELETE FRAGMENT SHADER OBJECT
	glDeleteShader(_fragmentShaderObject);
	_fragmentShaderObject = 0;

	//DELETE SHADER PROGRAM OBJECT
	glDeleteProgram(_shaderProgramObject);
	_shaderProgramObject = 0;

	//UNLINK THE PROGRAM
	glUseProgram(0);
}




