package com.astromedicomp.particleengineandroid;

public class GLESMacros
{
	public static final int VDG_ATTRIBUTE_VERTEX = 0;
	public static final int VDG_ATTRIBUTE_COLOR = 1;
	public static final int VDG_ATTRIBUTE_NORMAL = 2;
	public static final int VDG_ATTRIBUTE_TEXTURE0 = 3;
	public static final int VDG_ATTRIBUTE_VELOCITY_ARRAY= 4;
	public static final int VDG_ATTRIBUTE_START_TIME_ARRAY= 5;
}