package com.astromedicomp.particleengineandroid;

import android.content.Context;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;
import android.opengl.GLSurfaceView;

import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

//for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.Random;

import android.opengl.Matrix; //for Matrix Math.


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener{
		
	private final Context context;
	
	private GestureDetector gestureDetector;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao = new int[1];
	private int[] vbo_position = new int[1];
	private int[] vbo_color = new int[1];
	private int[] vbo_velocity = new int[1];
	private int[] vbo_start_time = new int[1];
	
	private int mvpUniform;
	private int locationUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16];
	
	private int widthArray = 100;
	private int heightArray = 100;
	
	private float speed = 0.001f;
	
	private float[] particleVertices = new float[100 * 100 * 3];
	private float[] particleColors = new float[100 * 100 * 3];
	private float[] particleVelocity = new float[100 * 100 * 3];
	private float[] particleStartTime = new float[100 * 100];
	
	private float particleTime = 0.0f;
	
	Random rand = new Random();
	
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		setEGLContextClientVersion(3);
		
		setRenderer(this);
		
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
		
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG: OpenGL-ES Version =" + version);
		
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version = " + glslVersion);
		
		initialize(gl);
	}
	
	@Override 
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
			resize(width, height);
	}
	
	@Override
	public void onDrawFrame(GL10 unused)
	{
			draw();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("VDG: " + "Double Tap");
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("VDG: " + "Single Tap");
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: " + "Long Press");
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: " + "Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void createPoints()
	{
		int i  = 0;
		float x = 0.0f; 
		float y = 0.0f;
		float r = 0.0f;
		float theta = 0.0f;
		
		for(i=0;i<widthArray * heightArray * 3;i+=3)
		{
			particleVertices[i] = x;
			particleVertices[i+1] = y;
			particleVertices[i+2] = (float)0.0;
			
			x= r* (float)Math.cos(theta);
			y = r* (float)Math.sin(theta);
			
			theta += speed * 200;
			r += speed*0.5;
			
			particleColors[i] = rand.nextFloat() * 0.5f + 0.5f ;
			particleColors[i+1] = rand.nextFloat() * 0.5f + 0.5f ;
			particleColors[i+2] = rand.nextFloat() * 0.5f + 0.5f ;
	
			if(particleColors[i] <= 0.0f || particleColors[i+1] <= 0.0f || particleColors[i+2] <= 0.0f)
				System.out.println("VDG::COLOR ISSUE");
		
			particleVelocity[i] = (rand.nextFloat()  + 0.5f) * (rand.nextInt(2) == 1 ? 1 : -1 ) ;
			particleVelocity[i+1] = (rand.nextFloat()  + 0.5f) * (rand.nextInt(2) == 1 ? 1 : -1 ) ;
			particleVelocity[i+2] = 0.0f;

//			System.out.println("VDG::PARTICLE: " + particleVertices[i] + "," + particleVertices[i + 1] + "," + particleVertices[i + 2] );

		}
		System.out.println("VDG::PARTICLE: " + particleVertices[i -3] + "," + particleVertices[i - 2] + "," + particleVertices[i - 1] );
		System.out.println("VDG::COLOR: " + particleColors[i -3 ] + "," + particleColors[i -2] + "," + particleColors[i -1] );
		System.out.println("VDG::VELOCITY: " + particleVelocity[i -3] + "," + particleVelocity[i -2] + "," + particleVelocity[i -1] );
		System.out.println("VDG::r: " + r );
		
		for(i=0;i<widthArray * heightArray ;i++)
		{
			particleStartTime[i] = i * speed;
		}
	}
	
	private void initialize(GL10 gl)
	{
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es"									+
		"\n"												+
		"in vec4 vPosition;"								+
		"in vec4 vColor;"									+
		"in vec3 vVelocity;"								+
		"in float fStartTime;"								+
		"uniform mat4 u_mvp_matrix;"						+
		"uniform float u_time;"								+
		"out vec4 out_color;"								+
		"void main(void)"									+
		"{"													+
		"vec4 vertice = vPosition;"							+
		"float t = u_time - fStartTime;"					+
		// "if(t >= 0.0)"										+
		// "{"													+
		// "vertice = vPosition + vec4(vVelocity * t, 0.0);" 	+
		// "vertice.y -= 4.9 * t * t; "						+
		// "}"													+
		"gl_Position = u_mvp_matrix  * vertice;"			+
		"out_color = vColor;"								+
		"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG:: Vertex Shader Compilation Log = "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es"							+
		"\n"										+
		"precision highp float;"					+
		"in vec4 out_color;"						+
		"out vec4 FragColor;"						+
		"void main(void)"							+
		"{"											+
		"FragColor = out_color;"					+
		"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0]= 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG:: Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}	
		
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);
		
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_COLOR, "vColor");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VELOCITY_ARRAY, "vVelocity");
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_START_TIME_ARRAY, "fStartTime");
		
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject , GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG:: Shader Program Link Log:: "+ szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		locationUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_time");
		
		createPoints();
		
		GLES32.glGenVertexArrays(1, vao, 0);
		GLES32.glBindVertexArray(vao[0]);
		
			GLES32.glGenBuffers(1, vbo_position, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);
			{	
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(widthArray * heightArray * 3 * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
				verticesBuffer.put(particleVertices);
				verticesBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									widthArray * heightArray * 3 * 4, 
									verticesBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

			GLES32.glGenBuffers(1, vbo_color, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color[0]);
			{	
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(widthArray * heightArray * 3 * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer colorBuffer = byteBuffer.asFloatBuffer();
				colorBuffer.put(particleColors);
				colorBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									widthArray * heightArray * 3 * 4, 
									colorBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_COLOR,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_COLOR);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

			GLES32.glGenBuffers(1, vbo_velocity, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_velocity[0]);
			{	
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(widthArray * heightArray * 3 * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer velocityBuffer = byteBuffer.asFloatBuffer();
				velocityBuffer.put(particleVelocity);
				velocityBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									widthArray * heightArray * 3 * 4, 
									velocityBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VELOCITY_ARRAY,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VELOCITY_ARRAY);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

			GLES32.glGenBuffers(1, vbo_start_time, 0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_start_time[0]);
			{	
				ByteBuffer byteBuffer = ByteBuffer.allocateDirect(widthArray * heightArray * 4);
				byteBuffer.order(ByteOrder.nativeOrder());
				FloatBuffer startTimeBuffer = byteBuffer.asFloatBuffer();
				startTimeBuffer.put(particleStartTime);
				startTimeBuffer.position(0);
				
				GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
									widthArray * heightArray * 4, 
									startTimeBuffer,
									GLES32.GL_STATIC_DRAW);
									
				GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_START_TIME_ARRAY,
											 3,
											 GLES32.GL_FLOAT,
											 false, 0, 0);
													
				GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_START_TIME_ARRAY);
			}
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
			
		GLES32.glBindVertexArray(0);
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		GLES32.glEnable(GLES32.GL_CULL_FACE);
			
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}
	
	private void resize(int width, int height)
	{
		GLES32.glViewport(0, 0, width, height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width/(float)height, 0.1f, 100.0f);			
	}
	
	public void draw()
	{
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		GLES32.glUseProgram(shaderProgramObject);
		
			// GLES32.glPointSize(5.0f);
			GLES32.glUniform1f(locationUniform, particleTime);
				
			float modelViewMatrix[] = new float[16];
			float modelViewProjectionMatrix[] = new float[16];
			
			Matrix.setIdentityM(modelViewMatrix, 0);
			Matrix.setIdentityM(modelViewProjectionMatrix, 0);
			
			Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -2.0f);
			
			Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0, modelViewMatrix, 0);
			GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);
			
			GLES32.glBindVertexArray(vao[0]);
				GLES32.glDrawArrays(GLES32.GL_POINTS, 0, widthArray * heightArray);
			GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);
		
		requestRender();
		
		particleTime += speed;
	}
	
	public void uninitialize()
	{
		if(vao[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao, 0);
			vao[0] = 0;
		}

		if(vbo_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_position, 0);
			vbo_position[0] = 0;
		}
		
		if(vbo_color[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_color, 0);
			vbo_color[0] = 0;
		}

		if(vbo_velocity[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_velocity, 0);
			vbo_velocity[0] = 0;
		}

		if(vbo_start_time[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_start_time, 0);
			vbo_start_time[0] = 0;
		}
		
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}
			
			if(fragmentShaderObject != 0)
			{
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}
		
		if(shaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
			
		
	}
}
