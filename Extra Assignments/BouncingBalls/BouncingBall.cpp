#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <math.h>
#include "BouncingBall.h"

#define WIN_WIDTH 600
#define WIN_HEIGHT 600
#define MAX_BALLS 24
#define BALL_RADIUS 0.15f

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

GLuint Texture_Ball = 0;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool gbLightsEnable = false;
bool gbStart = false;

float angleBall = 0.0f;
float gfWallMaxX = 2.0f;
float gfWallMaxY = 2.0f;
float gfSpeed = 0.1f;
float gfRed = 1.0f;
float gfGreen = 0.0f;
float gfBlue = 0.0f;

struct BallInfo 
{
	float x = 1.0f;
	float y = 0.0f;
	int dirX = 1;
	int dirY = 1;
	float dX = 0.03f;
	float dY = 0.03f;
	float red = 0.0f;
	float green = 0.0f;
	float blue = 0.0f;
	bool active = false;
};

int giNumOfBalls = 2;
int giNumOfBallsActive = 0;

BallInfo balls[MAX_BALLS];

GLfloat lightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat lightModelAmbiet[] = { 0.2f, 0.2f, 0.2f, 0.2f };
GLfloat lightModelLocalViewer = 0.0f;

GLfloat material_Ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat material_Diffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_Specular[] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat material_Shininess[] = { 50.0f };

GLfloat materialAmbient[24][4] = {
	{ 0.0215f, 0.1745f, 0.0215f, 1.0f },
	{ 0.135f , 0.225f , 0.1575f, 1.0f },
	{ 0.05375f , 0.05f , 0.06625f, 1.0f },
	{ 0.25f , 0.20725f , 0.20725f, 1.0f },
	{ 0.1745f , 0.01175f , 0.01175f, 1.0f },
	{ 0.1f , 0.18725f , 0.1745f, 1.0f },

	{ 0.329412f , 0.223529f , 0.027451f, 1.0f },
	{ 0.2125f , 0.1275f , 0.054f, 1.0f },
	{ 0.25f , 0.25f , 0.25f, 1.0f },
	{ 0.19125f , 0.0735f , 0.0225f, 1.0f },
	{ 0.24725f , 0.1995f , 0.0745f, 1.0f },
	{ 0.19225f , 0.19225f , 0.19225f, 1.0f },

	{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.1f , 0.06f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },
	{ 0.0f , 0.0f , 0.0f, 1.0f },

	{ 0.02f , 0.02f , 0.02f, 1.0f },
	{ 0.0f , 0.05f , 0.05f, 1.0f },
	{ 0.0f , 0.05f , 0.0f, 1.0f },
	{ 0.05f , 0.0f , 0.0f, 1.0f },
	{ 0.05f , 0.05f , 0.05f, 1.0f },
	{ 0.05f , 0.05f , 0.0f, 1.0f }
};

GLfloat materialDiffuse[24][4] = {
	{ 0.07568f, 0.61424f, 0.07568f, 1.0f },
	{ 0.54f , 0.89f , 0.63f , 1.0f },
	{ 0.18275f , 0.17f , 0.22525f , 1.0f },
	{ 1.0f , 0.829f , 0.829f , 1.0f },
	{ 0.61424f , 0.04136f , 0.04136f , 1.0f },
	{ 0.396f , 0.74151f , 0.69102f , 1.0f },

	{ 0.780392f , 0.568627f , 0.113725f , 1.0f },
	{ 0.714f , 0.4284f , 0.18144f , 1.0f },
	{ 0.4f , 0.4f , 0.4f , 1.0f },
	{ 0.7038f , 0.27048f , 0.0828f , 1.0f },
	{ 0.75164f , 0.60648f , 0.22648f , 1.0f },
	{ 0.50754f , 0.50754f , 0.50754f , 1.0f },

	{ 0.01f , 0.01f , 0.01f , 1.0f },
	{ 0.0f , 0.0520980392f ,  0.50980392f , 1.0f },
	{ 0.1f , 0.35f , 0.2f , 1.0f },
	{ 0.5f , 0.0f , 0.0f , 1.0f },
	{ 0.55f , 0.55f , 0.55f , 1.0f },
	{ 0.5f , 0.5f , 0.0f , 1.0f },

	{ 0.01f , 0.01f , 0.01f , 1.0f },
	{ 0.4f , 0.5f , 0.5f , 1.0f },
	{ 0.4f , 0.5f , 0.4f , 1.0f },
	{ 0.5f , 0.4f , 0.4f , 1.0f },
	{ 0.5f , 0.5f , 0.5f , 1.0f },
	{ 0.5f , 0.5f , 0.4f , 1.0f }

};

GLfloat materialSpecular[24][4] = {
	{ 0.633f , 0.727811f , 0.633f , 1.0f },
	{ 0.316228f , 0.316228f , 0.316228f , 1.0f },
	{ 0.332741f , 0.328634f , 0.346435f , 1.0f },
	{ 0.296648f , 0.296648f , 0.296648f , 1.0f },
	{ 0.727811f , 0.626959f , 0.626959f , 1.0f },
	{ 0.297254f , 0.30829f , 0.306678f , 1.0f },

	{ 0.992157f , 0.941176f , 0.807843f , 1.0f },
	{ 0.393548f , 0.271906f , 0.166721f , 1.0f },
	{ 0.774597f , 0.774597f , 0.774597f , 1.0f },
	{ 0.256777f , 0.137622f , 0.086014f , 1.0f },
	{ 0.628281f , 0.555802f , 0.366065f , 1.0f },
	{ 0.508273f , 0.508273f , 0.508273f , 1.0f },

	{ 0.5f , 0.5f , 0.5f , 1.0f },
	{ 0.50196078f , 0.50196078f , 0.50196078f , 1.0f },
	{ 0.45f , 0.5f , 0.45f , 1.0f },
	{ 0.7f , 0.6f , 0.6f , 1.0f },
	{ 0.7f , 0.7f , 0.7f , 1.0f },
	{ 0.6f , 0.6f , 0.5f , 1.0f },

	{ 0.4f , 0.4f , 0.4f , 1.0f },
	{ 0.04f , 0.7f , 0.7f , 1.0f },
	{ 0.04f , 0.7f , 0.04f , 1.0f },
	{ 0.7f , 0.04f , 0.04f , 1.0f },
	{ 0.7f , 0.7f , 0.7f , 1.0f },
	{ 0.7f , 0.7f , 0.04f , 1.0f }
};

GLfloat materialShininess[24] = {
	{ 0.6f * 128 },
	{ 0.1f * 128 },
	{ 0.3f * 128 },
	{ 0.088f * 128 },
	{ 0.6f * 128 },
	{ 0.1f * 128 },

	{ 0.21794872f * 128 },
	{ 0.2f * 128 },
	{ 0.6f * 128 },
	{ 0.1f * 128 },
	{ 0.4f * 128 },
	{ 0.4f * 128 },

	{ 0.25f * 128 },
	{ 0.25f * 128 },
	{ 0.25f * 128 },
	{ 0.25f * 128 },
	{ 0.25f * 128 },
	{ 0.25f * 128 },

	{ 0.078125f * 128 },
	{ 0.078125f * 128 },
	{ 0.078125f * 128 },
	{ 0.078125f * 128 },
	{ 0.078125f * 128 },
	{ 0.078125f * 128 }
};

GLUquadric *quadric = NULL;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	int width = 0, height = 0;
	width = GetSystemMetrics(SM_CXSCREEN);
	height = GetSystemMetrics(SM_CYSCREEN);

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("BouncingBall"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(width - WIN_WIDTH) / 2,
		(height - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				if (gbStart == true)
				{
					display();
				}
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 'a':
		case 'A':
			gbStart = true;
			break;
		case 'F'://for 'f' or 'F'
		case 'f':
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 'l':
		case 'L':
			if (gbLightsEnable)
			{
				gbLightsEnable = false;
				glDisable(GL_LIGHTING);
			}
			else
			{
				gbLightsEnable = true;
				glEnable(GL_LIGHTING);
			}
			break;
		case 'h':
			gfWallMaxX += gfSpeed;
			break;
		case 'H':
			gfWallMaxX -= gfSpeed;
			break;
		case 'v':
			gfWallMaxY += gfSpeed;
			break;
		case 'V':
			gfWallMaxY -= gfSpeed;
			break;
		case 's':
			gfSpeed += 0.001f;
			break;
		case 'S':
			gfSpeed -= 0.001f;
			break;
		case 'z':
			giNumOfBalls++;
			break;
		case 'Z':
			giNumOfBalls--;
			break;

		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[]);
	void initializeBalls();

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	quadric = gluNewQuadric();

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_TEXTURE_2D); //enable texture mapping

	LoadGLTextures(&Texture_Ball, MAKEINTRESOURCE(ID_BITMAP_BALL));

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmbiet);
	glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, lightModelLocalViewer);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

	glEnable(GL_LIGHT0);

	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

	initializeBalls();
}

void initializeBalls()
{
	int i = 0;
	for (i = 0; i < MAX_BALLS; i++)
	{
		balls[i].x = i%2 ? -3.0f : 3.0f;
		balls[i].y = -3.0f;
		balls[i].red = rand() % 100 / 100.0f;
		balls[i].green = rand() % 100 / 100.0f;
		balls[i].blue = rand() % 100 / 100.0f;
		balls[i].dirX = i % 2 ? balls[i].dirX : -balls[i].dirX;
	}
}

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	//code 
	glGenTextures(1, texture); //1 image
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap) //if bitmap exists ( means hBitmap is not null )
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //pixel storage mode( word alignment/4 bytes ).
		glBindTexture(GL_TEXTURE_2D, *texture); //bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//generate mipmapped texture ( 3 bytes, width, height & data from bmp)
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

		DeleteObject(hBitmap); //delete unwanted bitmap handle
	}
	return(iStatus);
}

void DrawBall()
{
	int i = 0;
	for (i = 0; i < giNumOfBalls; i++)
	{
		glPushMatrix();
		glTranslatef(balls[i].x, balls[i].y, 0.0f);
		glRotatef(angleBall, 1.0f, 1.0f, 1.0f);
		//glBindTexture(GL_TEXTURE_2D, Texture_Ball);
		//gluQuadricTexture(quadric, GLU_TRUE);
		glColor3f(balls[i].red, balls[i].green, balls[i].blue);
		glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient[i]);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse[i]);
		glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular[i]);
		glMaterialf(GL_FRONT, GL_SHININESS, materialShininess[i]);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		gluSphere(quadric, BALL_RADIUS, 50, 50);
		glPopMatrix();
	}
}

void SpinBall()
{
	if (angleBall > 360)
	{
		angleBall = 0.0f;
	}
	angleBall += gfSpeed;
}

void GenerateRandomColor(float *color)
{
	*color = rand() % 700;
	*color /= 700;
}

void CheckWallCollision(float *cur, float increment, int *dir, float max)
{	
	if (*cur > (max - BALL_RADIUS) || *cur < -(max - BALL_RADIUS))
	{
		(*dir) = (*dir)*(-1);
		if (*cur > (max - BALL_RADIUS))
		{
			*cur = max - BALL_RADIUS + (*dir)*increment*gfSpeed;
		}
		else
		{
			*cur = -(max - BALL_RADIUS) + (*dir)*increment*gfSpeed;
		}
		GenerateRandomColor(&gfRed);
		GenerateRandomColor(&gfGreen);
		GenerateRandomColor(&gfBlue);
	}
}

bool IsCollided(float x1, float y1, float x2, float y2)
{
	float distance = 0.0f;
	distance = sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
	if (distance <= 2* (BALL_RADIUS))
	{
		return true;
	}
	return false;
}

void InvertDirection(BallInfo *ball1, BallInfo *ball2)
{
	if (ball1->dirX * ball2->dirX == -1)
	{
		ball1->dirX *= -1;
		ball2->dirX *= -1;
	}
	if (ball1->dirY * ball2->dirY == -1)
	{
		ball1->dirY *= -1;
		ball2->dirY *= -1;
	}
}

void CheckCollision()
{
	int i = 0, j=0;
	for (i = 0; i < giNumOfBalls; i++)
	{
		for (j = i + 1; j < giNumOfBalls; j++)
		{ 
			if (IsCollided(balls[i].x, balls[i].y, balls[j].x, balls[j].y))
			{
				InvertDirection(&balls[i], &balls[j]);
				balls[i].x = balls[i].x + (balls[i].dirX)*balls[i].dX*gfSpeed;
				balls[i].y = balls[i].y + (balls[i].dirY)*balls[i].dY*gfSpeed;
			}
		}
	}
}

void MoveBall()
{
	int i = 0;
	for (i = 0; i < giNumOfBalls; i++)
	{
		balls[i].x = balls[i].x + (balls[i].dirX)*balls[i].dX*gfSpeed;
		balls[i].y = balls[i].y + (balls[i].dirY)*balls[i].dY*gfSpeed;
		if (i < giNumOfBallsActive)
		{
			CheckWallCollision(&balls[i].x, balls[i].dX, &balls[i].dirX, gfWallMaxX);
			CheckWallCollision(&balls[i].y, balls[i].dY, &balls[i].dirY, gfWallMaxY);
		}
	}
}

void DrawWall()
{
	glLineWidth(9.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(gfRed + 0.3f, gfGreen + 0.3f, gfBlue + 0.3f);
	glVertex3f(gfWallMaxX, gfWallMaxY, 0.0f);
	glVertex3f(-gfWallMaxX, gfWallMaxY, 0.0f);
	glVertex3f(-gfWallMaxX, -gfWallMaxY, 0.0f);
	glVertex3f(gfWallMaxX, -gfWallMaxY, 0.0f);
	glEnd();
}

void CheckActive()
{
	int i = 0;
	for (i = giNumOfBallsActive; i < giNumOfBalls; i++)
	{
		if (i % 2 == 0)
		{
			if (balls[i].y > -gfWallMaxY && balls[i].x > -gfWallMaxX)
			{
				giNumOfBallsActive++;
			}
		}
		else
		{
			if (balls[i].y > -gfWallMaxY && balls[i].x < gfWallMaxX)
			{
				giNumOfBallsActive++;
			}
		}
	}
}

void DrawAndUpdate()
{
	DrawBall();
	DrawWall();
	SpinBall();
	CheckCollision();
	MoveBall();
	CheckActive();
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -5.0f);
	DrawAndUpdate();
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	if (width == 0)
		width = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
		gluPerspective(45.0f, 1.0f*(GLfloat)height / (GLfloat)width, 0.1f, 100.0f);
	else
		gluPerspective(45.0f, 1.0f*(GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (Texture_Ball)
	{
		glDeleteTextures(1, &Texture_Ball);
		Texture_Ball = 0;
	}

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}
